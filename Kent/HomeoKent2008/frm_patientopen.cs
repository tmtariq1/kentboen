using ADODB;
using AxMSFlexGridLib;
using HomeoKent2008.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoKent2008
{
	[DesignerGenerated]
	public class frm_patientopen : Form
	{
		private IContainer components;

		[AccessedThroughProperty("FlexGrid1")]
		private AxMSFlexGrid _FlexGrid1;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("Button3")]
		private Button _Button3;

		[AccessedThroughProperty("Button4")]
		private Button _Button4;

		[AccessedThroughProperty("lst_doc")]
		private ListBox _lst_doc;

		[AccessedThroughProperty("OpenFileDialog1")]
		private OpenFileDialog _OpenFileDialog1;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("Combo1")]
		private ComboBox _Combo1;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Button5")]
		private Button _Button5;

		[AccessedThroughProperty("SaveDialog1")]
		private SaveFileDialog _SaveDialog1;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Combo2")]
		private ComboBox _Combo2;

		private Connection con;

		private Recordset rst;

		private Recordset rst1;

		internal virtual AxMSFlexGrid FlexGrid1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._FlexGrid1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._FlexGrid1 != null)
				{
					this._FlexGrid1.Enter -= new EventHandler(this.FlexGrid1_Enter);
					this._FlexGrid1.ClickEvent -= new EventHandler(this.FlexGrid1_ClickEvent);
				}
				this._FlexGrid1 = value;
				if (this._FlexGrid1 != null)
				{
					this._FlexGrid1.Enter += new EventHandler(this.FlexGrid1_Enter);
					this._FlexGrid1.ClickEvent += new EventHandler(this.FlexGrid1_ClickEvent);
				}
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.delete_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.delete_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.open_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.open_Click);
				}
			}
		}

		internal virtual Button Button3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button3 != null)
				{
					this._Button3.Click -= new EventHandler(this.cancel_Click);
				}
				this._Button3 = value;
				if (this._Button3 != null)
				{
					this._Button3.Click += new EventHandler(this.cancel_Click);
				}
			}
		}

		internal virtual Button Button4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button4 != null)
				{
					this._Button4.Click -= new EventHandler(this.Button4_Click);
				}
				this._Button4 = value;
				if (this._Button4 != null)
				{
					this._Button4.Click += new EventHandler(this.Button4_Click);
				}
			}
		}

		internal virtual ListBox lst_doc
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_doc;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_doc != null)
				{
					this._lst_doc.SelectedIndexChanged -= new EventHandler(this.lst_doc_SelectedIndexChanged);
				}
				this._lst_doc = value;
				if (this._lst_doc != null)
				{
					this._lst_doc.SelectedIndexChanged += new EventHandler(this.lst_doc_SelectedIndexChanged);
				}
			}
		}

		internal virtual OpenFileDialog OpenFileDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._OpenFileDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._OpenFileDialog1 = value;
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual ComboBox Combo1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Combo1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Combo1 = value;
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Button Button5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button5 != null)
				{
					this._Button5.Click -= new EventHandler(this.Button5_Click);
				}
				this._Button5 = value;
				if (this._Button5 != null)
				{
					this._Button5.Click += new EventHandler(this.Button5_Click);
				}
			}
		}

		internal virtual SaveFileDialog SaveDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._SaveDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._SaveDialog1 = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual ComboBox Combo2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Combo2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Combo2 = value;
			}
		}

		public frm_patientopen()
		{
			base.Load += new EventHandler(this.frm_patientopen_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.rst1 = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_patientopen));
			this.FlexGrid1 = new AxMSFlexGrid();
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.Button3 = new Button();
			this.Button4 = new Button();
			this.lst_doc = new ListBox();
			this.OpenFileDialog1 = new OpenFileDialog();
			this.Label1 = new Label();
			this.Combo1 = new ComboBox();
			this.Label2 = new Label();
			this.Button5 = new Button();
			this.SaveDialog1 = new SaveFileDialog();
			this.Label3 = new Label();
			this.Combo2 = new ComboBox();
			((ISupportInitialize)this.FlexGrid1).BeginInit();
			this.SuspendLayout();
			Control arg_CC_0 = this.FlexGrid1;
			Point location = new Point(12, 8);
			arg_CC_0.Location = location;
			this.FlexGrid1.Name = "FlexGrid1";
			this.FlexGrid1.OcxState = (AxHost.State)componentResourceManager.GetObject("FlexGrid1.OcxState");
			Control arg_114_0 = this.FlexGrid1;
			Size size = new Size(623, 368);
			arg_114_0.Size = size;
			this.FlexGrid1.TabIndex = 0;
			this.Button1.Enabled = false;
			Control arg_149_0 = this.Button1;
			location = new Point(490, 152);
			arg_149_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_170_0 = this.Button1;
			size = new Size(75, 60);
			arg_170_0.Size = size;
			this.Button1.TabIndex = 2;
			this.Button1.Text = "Delete";
			this.Button1.UseVisualStyleBackColor = true;
			this.Button1.Visible = false;
			this.Button2.Enabled = false;
			Control arg_1CD_0 = this.Button2;
			location = new Point(490, 212);
			arg_1CD_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_1F4_0 = this.Button2;
			size = new Size(75, 60);
			arg_1F4_0.Size = size;
			this.Button2.TabIndex = 3;
			this.Button2.Text = "Open";
			this.Button2.UseVisualStyleBackColor = true;
			this.Button2.Visible = false;
			this.Button3.BackColor = Color.FromArgb(255, 192, 128);
			this.Button3.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button3.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_295_0 = this.Button3;
			location = new Point(672, 34);
			arg_295_0.Location = location;
			this.Button3.Name = "Button3";
			Control arg_2BF_0 = this.Button3;
			size = new Size(159, 28);
			arg_2BF_0.Size = size;
			this.Button3.TabIndex = 4;
			this.Button3.Text = "Cancel / Update";
			this.Button3.UseVisualStyleBackColor = false;
			this.Button4.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_31E_0 = this.Button4;
			location = new Point(675, 77);
			arg_31E_0.Location = location;
			this.Button4.Name = "Button4";
			Control arg_348_0 = this.Button4;
			size = new Size(157, 29);
			arg_348_0.Size = size;
			this.Button4.TabIndex = 1;
			this.Button4.Text = "Edit Patient Detail";
			this.Button4.UseVisualStyleBackColor = true;
			this.lst_doc.FormattingEnabled = true;
			Control arg_399_0 = this.lst_doc;
			location = new Point(641, 216);
			arg_399_0.Location = location;
			this.lst_doc.Name = "lst_doc";
			Control arg_3C6_0 = this.lst_doc;
			size = new Size(192, 160);
			arg_3C6_0.Size = size;
			this.lst_doc.TabIndex = 5;
			this.lst_doc.Visible = false;
			this.OpenFileDialog1.FileName = "OpenFileDialog1";
			this.Label1.AutoSize = true;
			Control arg_417_0 = this.Label1;
			location = new Point(641, 200);
			arg_417_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_43E_0 = this.Label1;
			size = new Size(101, 13);
			arg_43E_0.Size = size;
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Selected Document";
			this.Label1.Visible = false;
			this.Combo1.FormattingEnabled = true;
			Control arg_48F_0 = this.Combo1;
			location = new Point(708, 140);
			arg_48F_0.Location = location;
			this.Combo1.Name = "Combo1";
			Control arg_4B6_0 = this.Combo1;
			size = new Size(121, 21);
			arg_4B6_0.Size = size;
			this.Combo1.TabIndex = 7;
			this.Combo1.Visible = false;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_514_0 = this.Label2;
			location = new Point(661, 145);
			arg_514_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_53B_0 = this.Label2;
			size = new Size(38, 13);
			arg_53B_0.Size = size;
			this.Label2.TabIndex = 8;
			this.Label2.Text = "Date:";
			this.Label2.Visible = false;
			this.Button5.Enabled = false;
			this.Button5.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_5A5_0 = this.Button5;
			location = new Point(672, 6);
			arg_5A5_0.Location = location;
			this.Button5.Name = "Button5";
			Control arg_5CF_0 = this.Button5;
			size = new Size(157, 29);
			arg_5CF_0.Size = size;
			this.Button5.TabIndex = 0;
			this.Button5.Text = "Patient Report";
			this.Button5.UseVisualStyleBackColor = true;
			this.Button5.Visible = false;
			this.Label3.AutoSize = true;
			this.Label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_649_0 = this.Label3;
			location = new Point(659, 174);
			arg_649_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_670_0 = this.Label3;
			size = new Size(38, 13);
			arg_670_0.Size = size;
			this.Label3.TabIndex = 9;
			this.Label3.Text = "Time:";
			this.Label3.Visible = false;
			this.Combo2.FormattingEnabled = true;
			Control arg_6C2_0 = this.Combo2;
			location = new Point(708, 173);
			arg_6C2_0.Location = location;
			this.Combo2.Name = "Combo2";
			Control arg_6E9_0 = this.Combo2;
			size = new Size(121, 21);
			arg_6E9_0.Size = size;
			this.Combo2.TabIndex = 10;
			this.Combo2.Visible = false;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(850, 382);
			this.ClientSize = size;
			this.Controls.Add(this.Combo2);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Combo1);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.lst_doc);
			this.Controls.Add(this.Button3);
			this.Controls.Add(this.FlexGrid1);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.Button4);
			this.Controls.Add(this.Button5);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_patientopen";
			this.Text = " P H R  ::: Pak Homeopathic Repertory 2008 Ver 1.0  ::: O L D  P A T I E N T S   :::";
			((ISupportInitialize)this.FlexGrid1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void frm_patientopen_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
			this.FlexGrid1.set_ColWidth(0, 1800);
			this.FlexGrid1.set_ColWidth(1, 1000);
			this.FlexGrid1.set_ColWidth(2, 900);
			this.FlexGrid1.Row = 0;
			this.FlexGrid1.Col = 0;
			this.FlexGrid1.Text = "Patient Name";
			this.FlexGrid1.Col = 1;
			this.FlexGrid1.Text = "Address";
			this.FlexGrid1.Col = 2;
			this.FlexGrid1.Text = "Mobile";
			this.grid_load();
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrk.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void grid_load()
		{
			int num = 1;
			byte b = 0;
			_Connection arg_19_0 = this.con;
			string arg_19_1 = "select * from Person";
			object value = Missing.Value;
			this.rst = arg_19_0.Execute(arg_19_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					this.FlexGrid1.Rows = num + 1;
					this.FlexGrid1.Row = num;
					if (b == 0)
					{
						this.FlexGrid1.Col = 0;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["name1"].Value);
						this.FlexGrid1.CellBackColor = Color.Silver;
						this.FlexGrid1.Col = 1;
						this.FlexGrid1.CellBackColor = Color.Silver;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["city"].Value);
						this.FlexGrid1.Col = 2;
						this.FlexGrid1.CellBackColor = Color.Silver;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["Mobile"].Value);
						this.FlexGrid1.Col = 3;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["Date1"].Value);
						this.FlexGrid1.Col = 4;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["persid"].Value);
						b = 1;
						num++;
						this.rst.MoveNext();
					}
					else
					{
						this.FlexGrid1.Col = 0;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["name1"].Value);
						this.FlexGrid1.CellBackColor = Color.LightSkyBlue;
						this.FlexGrid1.Col = 1;
						this.FlexGrid1.CellBackColor = Color.LightSkyBlue;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["city"].Value);
						this.FlexGrid1.Col = 2;
						this.FlexGrid1.CellBackColor = Color.LightSkyBlue;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["Mobile"].Value);
						this.FlexGrid1.Col = 3;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["Date1"].Value);
						this.FlexGrid1.Col = 4;
						this.FlexGrid1.Text = Conversions.ToString(this.rst.Fields["persid"].Value);
						num++;
						this.rst.MoveNext();
						b = 0;
					}
				}
			}
		}

		private void delete_Click(object sender, EventArgs e)
		{
			this.FlexGrid1.Col = 0;
			string text = this.FlexGrid1.Text;
			this.FlexGrid1.Col = 4;
			int num = Conversions.ToInteger(this.FlexGrid1.Text);
			if (Interaction.MsgBox("Do you want to Delete" + text, MsgBoxStyle.YesNo, null) == MsgBoxResult.Yes)
			{
				if (Operators.CompareString(text, "", false) != 0)
				{
					this.FlexGrid1.Col = 4;
					_Connection arg_9B_0 = this.con;
					string arg_9B_1 = "delete from Person where persid=" + Conversions.ToString(Conversion.Val(this.FlexGrid1.Text)) + "";
					object value = Missing.Value;
					arg_9B_0.Execute(arg_9B_1, out value, -1);
					_Connection arg_D4_0 = this.con;
					string arg_D4_1 = "delete from Patient_Symptoms where Patient_id= " + Conversions.ToString(Conversion.Val(this.FlexGrid1.Text)) + " ";
					value = Missing.Value;
					arg_D4_0.Execute(arg_D4_1, out value, -1);
				}
				Interaction.MsgBox("Record has been deleted", MsgBoxStyle.OkOnly, null);
				this.Close();
			}
			this.grid_load();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void open_Click(object sender, EventArgs e)
		{
			frm_Main frm_Main = new frm_Main();
			if (Operators.CompareString(this.Combo1.Text, "", false) != 0 & Operators.CompareString(this.Combo2.Text, "", false) != 0)
			{
				this.FlexGrid1.Col = 0;
				string text = this.FlexGrid1.Text;
				this.FlexGrid1.Col = 4;
				_Connection arg_CE_0 = this.con;
				string arg_CE_1 = string.Concat(new string[]
				{
					"update doctor set d_name='",
					this.FlexGrid1.Text,
					"',r_address='",
					this.Combo2.Text,
					"',phone='",
					this.Combo1.Text,
					"' where id =4"
				});
				object value = Missing.Value;
				arg_CE_0.Execute(arg_CE_1, out value, -1);
				frm_Main.Label6.Text = this.FlexGrid1.Text;
				frm_Main.Label7.Text = text;
				MyProject.Forms.frm_Main.open_patient();
				this.Close();
			}
			else
			{
				Interaction.MsgBox("Please first select Patient visit Date and Time", MsgBoxStyle.Information, null);
			}
		}

		private void Button4_Click(object sender, EventArgs e)
		{
			this.FlexGrid1.Col = 4;
			MyProject.Forms.frm_Patient.Label8.Text = this.FlexGrid1.Text;
			MyProject.Forms.frm_Patient.Show();
		}

		private void lst_doc_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Operators.ConditionalCompareObjectNotEqual(this.lst_doc.SelectedItem, "", false) && Interaction.MsgBox(Operators.ConcatenateObject("Do you want to delete ", this.lst_doc.SelectedItem), MsgBoxStyle.YesNo, null) == MsgBoxResult.Yes)
			{
				this.lst_doc.Items.Remove(RuntimeHelpers.GetObjectValue(this.lst_doc.SelectedItem));
			}
		}

		private void FlexGrid1_ClickEvent(object sender, EventArgs e)
		{
			this.Combo1.Items.Clear();
			this.Combo2.Items.Clear();
			this.FlexGrid1.Col = 4;
			_Connection arg_60_0 = this.con;
			string arg_60_1 = "select distinct date1 from Patient_Symptoms where Patient_id=" + Conversions.ToString(Conversion.Val(this.FlexGrid1.Text)) + " ";
			object value = Missing.Value;
			this.rst = arg_60_0.Execute(arg_60_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				this.Combo1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["date1"].Value));
				this.rst.MoveNext();
			}
			_Connection arg_FA_0 = this.con;
			string arg_FA_1 = "select distinct time1 from Patient_Symptoms where Patient_id=" + Conversions.ToString(Conversion.Val(this.FlexGrid1.Text)) + " ";
			value = Missing.Value;
			this.rst = arg_FA_0.Execute(arg_FA_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				this.Combo2.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["time1"].Value));
				this.rst.MoveNext();
			}
		}

		private void FlexGrid1_Enter(object sender, EventArgs e)
		{
		}

		private void Button5_Click(object sender, EventArgs e)
		{
		}
	}
}
