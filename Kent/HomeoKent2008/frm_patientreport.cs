using ADODB;
using AxMSFlexGridLib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoKent2008
{
	[DesignerGenerated]
	public class frm_patientreport : Form
	{
		private IContainer components;

		[AccessedThroughProperty("result_grid")]
		private AxMSFlexGrid _result_grid;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("ListBox1")]
		private ListBox _ListBox1;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("SaveDialog1")]
		private SaveFileDialog _SaveDialog1;

		[AccessedThroughProperty("ListBox2")]
		private ListBox _ListBox2;

		private Connection con;

		private Recordset rst;

		private Recordset rst1;

		private frm_Main frm;

		internal virtual AxMSFlexGrid result_grid
		{
			[DebuggerNonUserCode]
			get
			{
				return this._result_grid;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._result_grid != null)
				{
					this._result_grid.ClickEvent -= new EventHandler(this.result_grid_ClickEvent);
				}
				this._result_grid = value;
				if (this._result_grid != null)
				{
					this._result_grid.ClickEvent += new EventHandler(this.result_grid_ClickEvent);
				}
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual ListBox ListBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ListBox1 != null)
				{
					this._ListBox1.SelectedIndexChanged -= new EventHandler(this.ListBox1_SelectedIndexChanged);
				}
				this._ListBox1 = value;
				if (this._ListBox1 != null)
				{
					this._ListBox1.SelectedIndexChanged += new EventHandler(this.ListBox1_SelectedIndexChanged);
				}
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual SaveFileDialog SaveDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._SaveDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._SaveDialog1 = value;
			}
		}

		internal virtual ListBox ListBox2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ListBox2 = value;
			}
		}

		public frm_patientreport()
		{
			base.Load += new EventHandler(this.frm_patientreport_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.rst1 = new RecordsetClass();
			this.frm = new frm_Main();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_patientreport));
			this.result_grid = new AxMSFlexGrid();
			this.Label2 = new Label();
			this.ListBox1 = new ListBox();
			this.Button1 = new Button();
			this.Label5 = new Label();
			this.Button2 = new Button();
			this.SaveDialog1 = new SaveFileDialog();
			this.ListBox2 = new ListBox();
			((ISupportInitialize)this.result_grid).BeginInit();
			this.SuspendLayout();
			Control arg_8B_0 = this.result_grid;
			Point location = new Point(12, 30);
			arg_8B_0.Location = location;
			this.result_grid.Name = "result_grid";
			this.result_grid.OcxState = (AxHost.State)componentResourceManager.GetObject("result_grid.OcxState");
			Control arg_D3_0 = this.result_grid;
			Size size = new Size(161, 292);
			arg_D3_0.Size = size;
			this.result_grid.TabIndex = 0;
			this.Label2.AutoSize = true;
			Control arg_102_0 = this.Label2;
			location = new Point(12, 9);
			arg_102_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_129_0 = this.Label2;
			size = new Size(111, 13);
			arg_129_0.Size = size;
			this.Label2.TabIndex = 2;
			this.Label2.Text = "Suggested Remedies:";
			this.ListBox1.FormattingEnabled = true;
			Control arg_16B_0 = this.ListBox1;
			location = new Point(188, 30);
			arg_16B_0.Location = location;
			this.ListBox1.Name = "ListBox1";
			Control arg_198_0 = this.ListBox1;
			size = new Size(162, 160);
			arg_198_0.Size = size;
			this.ListBox1.TabIndex = 3;
			Control arg_1BE_0 = this.Button1;
			location = new Point(362, 85);
			arg_1BE_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_1E5_0 = this.Button1;
			size = new Size(75, 60);
			arg_1E5_0.Size = size;
			this.Button1.TabIndex = 6;
			this.Button1.Text = "Report";
			this.Button1.UseVisualStyleBackColor = true;
			this.Label5.AutoSize = true;
			Control arg_233_0 = this.Label5;
			location = new Point(144, 9);
			arg_233_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_25A_0 = this.Label5;
			size = new Size(39, 13);
			arg_25A_0.Size = size;
			this.Label5.TabIndex = 7;
			this.Label5.Text = "Label5";
			Control arg_293_0 = this.Button2;
			location = new Point(362, 145);
			arg_293_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_2BA_0 = this.Button2;
			size = new Size(75, 62);
			arg_2BA_0.Size = size;
			this.Button2.TabIndex = 8;
			this.Button2.Text = "Cancel";
			this.Button2.UseVisualStyleBackColor = true;
			this.ListBox2.FormattingEnabled = true;
			Control arg_30B_0 = this.ListBox2;
			location = new Point(188, 196);
			arg_30B_0.Location = location;
			this.ListBox2.Name = "ListBox2";
			Control arg_335_0 = this.ListBox2;
			size = new Size(147, 69);
			arg_335_0.Size = size;
			this.ListBox2.TabIndex = 9;
			this.ListBox2.Visible = false;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(449, 334);
			this.ClientSize = size;
			this.Controls.Add(this.ListBox2);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.ListBox1);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.result_grid);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.Name = "frm_patientreport";
			this.Text = "Report";
			((ISupportInitialize)this.result_grid).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void frm_patientreport_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
			this.result_grid.Clear();
			Recordset recordset = new RecordsetClass();
			double num = 2.0;
			Cursor.Current = Cursors.WaitCursor;
			this.result_grid.Row = 0;
			this.result_grid.Col = 0;
			this.result_grid.Text = "Remedey";
			this.result_grid.Col = 1;
			this.result_grid.Text = "Grade";
			_Connection arg_83_0 = this.con;
			string arg_83_1 = "update cal_result set parity=0";
			object value = Missing.Value;
			arg_83_0.Execute(arg_83_1, out value, -1);
			_Connection arg_9E_0 = this.con;
			string arg_9E_1 = "select rem,remid from cal_result where parity =0";
			value = Missing.Value;
			this.rst = arg_9E_0.Execute(arg_9E_1, out value, -1);
			int num2;
			while (!this.rst.EOF & !this.rst.BOF)
			{
				checked
				{
					this.result_grid.Rows = (int)Math.Round(num);
					this.result_grid.Row = (int)Math.Round(unchecked(num - 1.0));
					this.result_grid.Col = 0;
					this.result_grid.Text = Conversions.ToString(this.rst.Fields["rem"].Value);
					num2 = 0;
					_Connection arg_155_0 = this.con;
					string arg_155_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select weight from cal_result where remid=", this.rst.Fields["remid"].Value), ""));
					value = Missing.Value;
					recordset = arg_155_0.Execute(arg_155_1, out value, -1);
					while (!recordset.EOF & !recordset.BOF)
					{
						num2 = Conversions.ToInteger(Operators.AddObject(num2, recordset.Fields["weight"].Value));
						_Connection arg_1C5_0 = this.con;
						string arg_1C5_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("update cal_result set parity=1 where remid=", this.rst.Fields["remid"].Value), ""));
						value = Missing.Value;
						arg_1C5_0.Execute(arg_1C5_1, out value, -1);
						recordset.MoveNext();
					}
					this.result_grid.Row = (int)Math.Round(unchecked(num - 1.0));
					this.result_grid.Col = 1;
					this.result_grid.Text = Conversions.ToString(num2);
					this.rst.MoveNext();
				}
				num += 1.0;
			}
			this.result_grid.Col = 1;
			this.result_grid.Sort = 2;
			this.result_grid.Row = 1;
			this.result_grid.Col = 0;
			this.ListBox1.Items.Add(this.result_grid.Text);
			this.result_grid.Col = 1;
			num2 = Conversions.ToInteger(this.result_grid.Text);
			this.ListBox2.Items.Add(this.result_grid.Text);
			double arg_2F9_0 = 2.0;
			double num3 = (double)(checked(this.result_grid.Rows - 1));
			for (num = arg_2F9_0; num <= num3; num += 1.0)
			{
				this.result_grid.Row = checked((int)Math.Round(num));
				this.result_grid.Col = 1;
				if (Conversions.ToDouble(this.result_grid.Text) != (double)num2)
				{
					break;
				}
				this.ListBox2.Items.Add(this.result_grid.Text);
				this.result_grid.Col = 0;
				this.ListBox1.Items.Add(this.result_grid.Text);
			}
			Cursor.Current = Cursors.Arrow;
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrk.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ListBox1.Items.Remove(RuntimeHelpers.GetObjectValue(this.ListBox1.SelectedItem));
		}

		private void result_grid_ClickEvent(object sender, EventArgs e)
		{
			this.result_grid.Col = 0;
			this.ListBox1.Items.Add(this.result_grid.Text);
			this.result_grid.Col = 1;
			this.ListBox2.Items.Add(this.result_grid.Text);
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			_Connection arg_16_0 = this.con;
			string arg_16_1 = "select * from doctor where id=5";
			object value = Missing.Value;
			this.rst = arg_16_0.Execute(arg_16_1, out value, -1);
			int value2 = Conversions.ToInteger(this.rst.Fields["d_name"].Value);
			value2 = 8;
			SaveFileDialog saveDialog = this.SaveDialog1;
			saveDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			checked
			{
				if (saveDialog.ShowDialog() == DialogResult.OK)
				{
					FileStream stream = new FileStream(saveDialog.FileName, FileMode.Create, FileAccess.Write);
					StreamWriter streamWriter = new StreamWriter(stream);
					_Connection arg_90_0 = this.con;
					string arg_90_1 = "select * from doctor where id=5";
					value = Missing.Value;
					this.rst = arg_90_0.Execute(arg_90_1, out value, -1);
					string text = Conversions.ToString(this.rst.Fields["d_name"].Value);
					streamWriter.WriteLine(text.ToUpper());
					text = Conversions.ToString(this.rst.Fields["c_address"].Value);
					streamWriter.WriteLine("Clinic: " + text.ToUpper());
					streamWriter.WriteLine(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("Phone:  ", this.rst.Fields["phone"].Value), Strings.Space(10)), "Cell:  "), this.rst.Fields["cell"].Value));
					streamWriter.WriteLine(Operators.ConcatenateObject("Email:  ", this.rst.Fields["email"].Value));
					streamWriter.WriteLine("--------------------------------------------------------------------");
					_Connection arg_1BB_0 = this.con;
					string arg_1BB_1 = "select * from person where persid=" + Conversions.ToString(value2) + "";
					value = Missing.Value;
					this.rst = arg_1BB_0.Execute(arg_1BB_1, out value, -1);
					text = Conversions.ToString(this.rst.Fields["name1"].Value);
					streamWriter.WriteLine(Operators.ConcatenateObject("Patient Name: " + text.ToUpper() + Strings.Space(10) + "Age: ", this.rst.Fields["DateBirth"].Value));
					streamWriter.WriteLine(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("Sex:  ", this.rst.Fields["sex"].Value), Strings.Space(10)), "ID:"), this.rst.Fields["persid"].Value));
					streamWriter.WriteLine("----------------------------------");
					streamWriter.WriteLine("");
					streamWriter.WriteLine("SYMPTOMS");
					streamWriter.WriteLine("");
					int arg_2DB_0 = 0;
					int num = this.frm.ListBox1.Items.Count - 1;
					for (int i = arg_2DB_0; i <= num; i++)
					{
						streamWriter.WriteLine(Operators.ConcatenateObject(Conversions.ToString(i + 1) + ":", this.frm.ListBox1.Items[i]));
					}
					streamWriter.WriteLine("----------------------------------");
					streamWriter.WriteLine("");
					streamWriter.WriteLine("SELECTED REMEDIES");
					streamWriter.WriteLine("");
					int arg_360_0 = 0;
					int num2 = this.ListBox1.Items.Count - 1;
					for (int i = arg_360_0; i <= num2; i++)
					{
						streamWriter.WriteLine(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(this.ListBox1.Items[i], "["), this.ListBox2.Items[i]), "]"));
					}
					streamWriter.Close();
				}
			}
		}
	}
}
