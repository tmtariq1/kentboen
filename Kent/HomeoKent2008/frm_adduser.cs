using ADODB;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoKent2008
{
	[DesignerGenerated]
	public class frm_adduser : Form
	{
		private IContainer components;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("u_name")]
		private TextBox _u_name;

		[AccessedThroughProperty("u_pass")]
		private TextBox _u_pass;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("u_pass1")]
		private TextBox _u_pass1;

		private Connection con;

		private Recordset rst;

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual TextBox u_name
		{
			[DebuggerNonUserCode]
			get
			{
				return this._u_name;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._u_name = value;
			}
		}

		internal virtual TextBox u_pass
		{
			[DebuggerNonUserCode]
			get
			{
				return this._u_pass;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._u_pass = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual TextBox u_pass1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._u_pass1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._u_pass1 = value;
			}
		}

		public frm_adduser()
		{
			base.Load += new EventHandler(this.frm_adduser_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_adduser));
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.Label1 = new Label();
			this.Label2 = new Label();
			this.u_name = new TextBox();
			this.u_pass = new TextBox();
			this.Label3 = new Label();
			this.u_pass1 = new TextBox();
			this.SuspendLayout();
			this.Button1.BackgroundImage = (Image)componentResourceManager.GetObject("Button1.BackgroundImage");
			this.Button1.BackgroundImageLayout = ImageLayout.Center;
			this.Button1.Image = (Image)componentResourceManager.GetObject("Button1.Image");
			Control arg_C8_0 = this.Button1;
			Point location = new Point(147, 139);
			arg_C8_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_EF_0 = this.Button1;
			Size size = new Size(75, 60);
			arg_EF_0.Size = size;
			this.Button1.TabIndex = 3;
			this.Button1.UseVisualStyleBackColor = true;
			this.Button1.UseWaitCursor = true;
			this.Button2.BackgroundImage = (Image)componentResourceManager.GetObject("Button2.BackgroundImage");
			this.Button2.Image = (Image)componentResourceManager.GetObject("Button2.Image");
			Control arg_166_0 = this.Button2;
			location = new Point(222, 139);
			arg_166_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_18D_0 = this.Button2;
			size = new Size(75, 60);
			arg_18D_0.Size = size;
			this.Button2.TabIndex = 4;
			this.Button2.UseVisualStyleBackColor = true;
			this.Button2.UseWaitCursor = true;
			this.Label1.AutoSize = true;
			Control arg_1D3_0 = this.Label1;
			location = new Point(7, 39);
			arg_1D3_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_1FA_0 = this.Label1;
			size = new Size(63, 13);
			arg_1FA_0.Size = size;
			this.Label1.TabIndex = 2;
			this.Label1.Text = "User Name:";
			this.Label1.UseWaitCursor = true;
			this.Label2.AutoSize = true;
			Control arg_244_0 = this.Label2;
			location = new Point(6, 71);
			arg_244_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_26B_0 = this.Label2;
			size = new Size(81, 13);
			arg_26B_0.Size = size;
			this.Label2.TabIndex = 3;
			this.Label2.Text = "New Password:";
			this.Label2.UseWaitCursor = true;
			Control arg_2AD_0 = this.u_name;
			location = new Point(147, 35);
			arg_2AD_0.Location = location;
			this.u_name.Name = "u_name";
			Control arg_2D7_0 = this.u_name;
			size = new Size(150, 20);
			arg_2D7_0.Size = size;
			this.u_name.TabIndex = 0;
			this.u_name.UseWaitCursor = true;
			Control arg_309_0 = this.u_pass;
			location = new Point(147, 67);
			arg_309_0.Location = location;
			this.u_pass.Name = "u_pass";
			this.u_pass.PasswordChar = '|';
			Control arg_340_0 = this.u_pass;
			size = new Size(150, 20);
			arg_340_0.Size = size;
			this.u_pass.TabIndex = 1;
			this.u_pass.UseWaitCursor = true;
			this.Label3.AutoSize = true;
			Control arg_37A_0 = this.Label3;
			location = new Point(7, 103);
			arg_37A_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_3A1_0 = this.Label3;
			size = new Size(113, 13);
			arg_3A1_0.Size = size;
			this.Label3.TabIndex = 6;
			this.Label3.Text = "Confirm new password";
			this.Label3.UseWaitCursor = true;
			Control arg_3E3_0 = this.u_pass1;
			location = new Point(147, 99);
			arg_3E3_0.Location = location;
			this.u_pass1.Name = "u_pass1";
			this.u_pass1.PasswordChar = '|';
			Control arg_41A_0 = this.u_pass1;
			size = new Size(150, 20);
			arg_41A_0.Size = size;
			this.u_pass1.TabIndex = 2;
			this.u_pass1.UseWaitCursor = true;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(513, 209);
			this.ClientSize = size;
			this.Controls.Add(this.u_pass1);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.u_pass);
			this.Controls.Add(this.u_name);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_adduser";
			this.Text = "Change Login :::  PHR :: Pak Homeopathic Repertory 2008 Ver 1.0";
			this.UseWaitCursor = true;
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			if (Operators.CompareString(this.u_name.Text, "", false) != 0)
			{
				if (Operators.CompareString(this.u_pass.Text, this.u_pass1.Text, false) == 0)
				{
					_Connection arg_8E_0 = this.con;
					string arg_8E_1 = string.Concat(new string[]
					{
						"update  pass set u_name='",
						this.u_name.Text,
						"'  ,u_pass='",
						this.u_pass.Text,
						"' where id =1  "
					});
					object value = Missing.Value;
					arg_8E_0.Execute(arg_8E_1, out value, -1);
					this.u_name.Text = "";
					this.u_pass.Text = "";
					Interaction.MsgBox("Password Changed", MsgBoxStyle.OkOnly, null);
					this.Close();
				}
				else
				{
					Interaction.MsgBox("Confirm password is Wrong", MsgBoxStyle.Information, null);
					this.u_pass.Text = "";
					this.u_pass1.Text = "";
				}
			}
			else
			{
				Interaction.MsgBox("Plz Give User Name", MsgBoxStyle.Information, null);
			}
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrk.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void frm_adduser_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
