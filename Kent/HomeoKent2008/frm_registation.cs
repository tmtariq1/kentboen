using HomeoKent2008.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoKent2008
{
	[DesignerGenerated]
	public class frm_registation : Form
	{
		private IContainer components;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("TextBox1")]
		private TextBox _TextBox1;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("TextBox2")]
		private TextBox _TextBox2;

		[AccessedThroughProperty("TextBox3")]
		private TextBox _TextBox3;

		[AccessedThroughProperty("TextBox4")]
		private TextBox _TextBox4;

		[AccessedThroughProperty("TextBox5")]
		private TextBox _TextBox5;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Label4")]
		private Label _Label4;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("number1")]
		private TextBox _number1;

		[AccessedThroughProperty("Label6")]
		private Label _Label6;

		[AccessedThroughProperty("number2")]
		private TextBox _number2;

		[AccessedThroughProperty("number3")]
		private TextBox _number3;

		[AccessedThroughProperty("number4")]
		private TextBox _number4;

		[AccessedThroughProperty("number5")]
		private TextBox _number5;

		[AccessedThroughProperty("Label7")]
		private Label _Label7;

		[AccessedThroughProperty("Label8")]
		private Label _Label8;

		[AccessedThroughProperty("Label9")]
		private Label _Label9;

		[AccessedThroughProperty("Label10")]
		private Label _Label10;

		[AccessedThroughProperty("PictureBox1")]
		private PictureBox _PictureBox1;

		[AccessedThroughProperty("PictureBox2")]
		private PictureBox _PictureBox2;

		[AccessedThroughProperty("Label11")]
		private Label _Label11;

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual TextBox TextBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TextBox1 != null)
				{
					this._TextBox1.TextChanged -= new EventHandler(this.TextBox1_TextChanged);
				}
				this._TextBox1 = value;
				if (this._TextBox1 != null)
				{
					this._TextBox1.TextChanged += new EventHandler(this.TextBox1_TextChanged);
				}
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual TextBox TextBox2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TextBox2 != null)
				{
					this._TextBox2.TextChanged -= new EventHandler(this.TextBox2_TextChanged);
				}
				this._TextBox2 = value;
				if (this._TextBox2 != null)
				{
					this._TextBox2.TextChanged += new EventHandler(this.TextBox2_TextChanged);
				}
			}
		}

		internal virtual TextBox TextBox3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TextBox3 != null)
				{
					this._TextBox3.TextChanged -= new EventHandler(this.TextBox3_TextChanged);
				}
				this._TextBox3 = value;
				if (this._TextBox3 != null)
				{
					this._TextBox3.TextChanged += new EventHandler(this.TextBox3_TextChanged);
				}
			}
		}

		internal virtual TextBox TextBox4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TextBox4 != null)
				{
					this._TextBox4.TextChanged -= new EventHandler(this.TextBox4_TextChanged);
				}
				this._TextBox4 = value;
				if (this._TextBox4 != null)
				{
					this._TextBox4.TextChanged += new EventHandler(this.TextBox4_TextChanged);
				}
			}
		}

		internal virtual TextBox TextBox5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TextBox5 != null)
				{
					this._TextBox5.LostFocus -= new EventHandler(this.TextBox5_LostFocus);
				}
				this._TextBox5 = value;
				if (this._TextBox5 != null)
				{
					this._TextBox5.LostFocus += new EventHandler(this.TextBox5_LostFocus);
				}
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual Label Label4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label4 = value;
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual TextBox number1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._number1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._number1 = value;
			}
		}

		internal virtual Label Label6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label6 = value;
			}
		}

		internal virtual TextBox number2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._number2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._number2 = value;
			}
		}

		internal virtual TextBox number3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._number3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._number3 = value;
			}
		}

		internal virtual TextBox number4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._number4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._number4 = value;
			}
		}

		internal virtual TextBox number5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._number5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._number5 = value;
			}
		}

		internal virtual Label Label7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label7 = value;
			}
		}

		internal virtual Label Label8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label8 = value;
			}
		}

		internal virtual Label Label9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label9 = value;
			}
		}

		internal virtual Label Label10
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label10;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label10 = value;
			}
		}

		internal virtual PictureBox PictureBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._PictureBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._PictureBox1 = value;
			}
		}

		internal virtual PictureBox PictureBox2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._PictureBox2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._PictureBox2 != null)
				{
					this._PictureBox2.Click -= new EventHandler(this.PictureBox2_Click);
				}
				this._PictureBox2 = value;
				if (this._PictureBox2 != null)
				{
					this._PictureBox2.Click += new EventHandler(this.PictureBox2_Click);
				}
			}
		}

		internal virtual Label Label11
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label11;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label11 = value;
			}
		}

		[DebuggerNonUserCode]
		public frm_registation()
		{
			base.Load += new EventHandler(this.frm_registation_Load);
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_registation));
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.TextBox1 = new TextBox();
			this.Label1 = new Label();
			this.TextBox2 = new TextBox();
			this.TextBox3 = new TextBox();
			this.TextBox4 = new TextBox();
			this.TextBox5 = new TextBox();
			this.Label2 = new Label();
			this.Label3 = new Label();
			this.Label4 = new Label();
			this.Label5 = new Label();
			this.number1 = new TextBox();
			this.Label6 = new Label();
			this.number2 = new TextBox();
			this.number3 = new TextBox();
			this.number4 = new TextBox();
			this.number5 = new TextBox();
			this.Label7 = new Label();
			this.Label8 = new Label();
			this.Label9 = new Label();
			this.Label10 = new Label();
			this.PictureBox1 = new PictureBox();
			this.PictureBox2 = new PictureBox();
			this.Label11 = new Label();
			((ISupportInitialize)this.PictureBox1).BeginInit();
			((ISupportInitialize)this.PictureBox2).BeginInit();
			this.SuspendLayout();
			this.Button1.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_171_0 = this.Button1;
			Point location = new Point(66, 160);
			arg_171_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_198_0 = this.Button1;
			Size size = new Size(67, 50);
			arg_198_0.Size = size;
			this.Button1.TabIndex = 0;
			this.Button1.Text = "OK";
			this.Button1.UseVisualStyleBackColor = true;
			this.Button2.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_1FA_0 = this.Button2;
			location = new Point(378, 160);
			arg_1FA_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_221_0 = this.Button2;
			size = new Size(69, 50);
			arg_221_0.Size = size;
			this.Button2.TabIndex = 1;
			this.Button2.Text = "Cancel";
			this.Button2.UseVisualStyleBackColor = true;
			this.TextBox1.ForeColor = Color.Red;
			Control arg_273_0 = this.TextBox1;
			location = new Point(139, 89);
			arg_273_0.Location = location;
			this.TextBox1.Name = "TextBox1";
			Control arg_29A_0 = this.TextBox1;
			size = new Size(62, 20);
			arg_29A_0.Size = size;
			this.TextBox1.TabIndex = 2;
			this.TextBox1.TextAlign = HorizontalAlignment.Center;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_2F2_0 = this.Label1;
			location = new Point(12, 92);
			arg_2F2_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_319_0 = this.Label1;
			size = new Size(121, 16);
			arg_319_0.Size = size;
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Activation Code:";
			this.TextBox2.ForeColor = Color.Red;
			Control arg_35F_0 = this.TextBox2;
			location = new Point(214, 89);
			arg_35F_0.Location = location;
			this.TextBox2.Name = "TextBox2";
			Control arg_386_0 = this.TextBox2;
			size = new Size(62, 20);
			arg_386_0.Size = size;
			this.TextBox2.TabIndex = 4;
			this.TextBox2.TextAlign = HorizontalAlignment.Center;
			this.TextBox3.ForeColor = Color.Red;
			Control arg_3C8_0 = this.TextBox3;
			location = new Point(289, 89);
			arg_3C8_0.Location = location;
			this.TextBox3.Name = "TextBox3";
			Control arg_3EF_0 = this.TextBox3;
			size = new Size(62, 20);
			arg_3EF_0.Size = size;
			this.TextBox3.TabIndex = 5;
			this.TextBox3.TextAlign = HorizontalAlignment.Center;
			this.TextBox4.ForeColor = Color.Red;
			Control arg_431_0 = this.TextBox4;
			location = new Point(364, 89);
			arg_431_0.Location = location;
			this.TextBox4.Name = "TextBox4";
			Control arg_458_0 = this.TextBox4;
			size = new Size(62, 20);
			arg_458_0.Size = size;
			this.TextBox4.TabIndex = 6;
			this.TextBox4.TextAlign = HorizontalAlignment.Center;
			this.TextBox5.ForeColor = Color.Red;
			Control arg_49A_0 = this.TextBox5;
			location = new Point(439, 89);
			arg_49A_0.Location = location;
			this.TextBox5.Name = "TextBox5";
			Control arg_4C1_0 = this.TextBox5;
			size = new Size(62, 20);
			arg_4C1_0.Size = size;
			this.TextBox5.TabIndex = 7;
			this.TextBox5.TextAlign = HorizontalAlignment.Center;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_51C_0 = this.Label2;
			location = new Point(201, 91);
			arg_51C_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_543_0 = this.Label2;
			size = new Size(13, 16);
			arg_543_0.Size = size;
			this.Label2.TabIndex = 8;
			this.Label2.Text = "-";
			this.Label3.AutoSize = true;
			this.Label3.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_5A2_0 = this.Label3;
			location = new Point(276, 91);
			arg_5A2_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_5C9_0 = this.Label3;
			size = new Size(13, 16);
			arg_5C9_0.Size = size;
			this.Label3.TabIndex = 9;
			this.Label3.Text = "-";
			this.Label4.AutoSize = true;
			this.Label4.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_629_0 = this.Label4;
			location = new Point(351, 91);
			arg_629_0.Location = location;
			this.Label4.Name = "Label4";
			Control arg_650_0 = this.Label4;
			size = new Size(13, 16);
			arg_650_0.Size = size;
			this.Label4.TabIndex = 10;
			this.Label4.Text = "-";
			this.Label5.AutoSize = true;
			this.Label5.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_6B0_0 = this.Label5;
			location = new Point(426, 91);
			arg_6B0_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_6D7_0 = this.Label5;
			size = new Size(13, 16);
			arg_6D7_0.Size = size;
			this.Label5.TabIndex = 11;
			this.Label5.Text = "-";
			Control arg_70E_0 = this.number1;
			location = new Point(139, 41);
			arg_70E_0.Location = location;
			this.number1.Name = "number1";
			Control arg_735_0 = this.number1;
			size = new Size(62, 20);
			arg_735_0.Size = size;
			this.number1.TabIndex = 12;
			this.number1.TextAlign = HorizontalAlignment.Center;
			this.Label6.AutoSize = true;
			this.Label6.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_78E_0 = this.Label6;
			location = new Point(12, 45);
			arg_78E_0.Location = location;
			this.Label6.Name = "Label6";
			Control arg_7B5_0 = this.Label6;
			size = new Size(70, 16);
			arg_7B5_0.Size = size;
			this.Label6.TabIndex = 13;
			this.Label6.Text = "Client ID:";
			this.Label6.TextAlign = ContentAlignment.TopCenter;
			Control arg_7F8_0 = this.number2;
			location = new Point(214, 41);
			arg_7F8_0.Location = location;
			this.number2.Name = "number2";
			Control arg_81F_0 = this.number2;
			size = new Size(62, 20);
			arg_81F_0.Size = size;
			this.number2.TabIndex = 14;
			this.number2.TextAlign = HorizontalAlignment.Center;
			Control arg_852_0 = this.number3;
			location = new Point(289, 41);
			arg_852_0.Location = location;
			this.number3.Name = "number3";
			Control arg_879_0 = this.number3;
			size = new Size(62, 20);
			arg_879_0.Size = size;
			this.number3.TabIndex = 15;
			this.number3.TextAlign = HorizontalAlignment.Center;
			Control arg_8AC_0 = this.number4;
			location = new Point(367, 41);
			arg_8AC_0.Location = location;
			this.number4.Name = "number4";
			Control arg_8D3_0 = this.number4;
			size = new Size(62, 20);
			arg_8D3_0.Size = size;
			this.number4.TabIndex = 16;
			this.number4.TextAlign = HorizontalAlignment.Center;
			Control arg_906_0 = this.number5;
			location = new Point(439, 41);
			arg_906_0.Location = location;
			this.number5.Name = "number5";
			Control arg_92D_0 = this.number5;
			size = new Size(62, 20);
			arg_92D_0.Size = size;
			this.number5.TabIndex = 17;
			this.number5.TextAlign = HorizontalAlignment.Center;
			this.Label7.AutoSize = true;
			this.Label7.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_989_0 = this.Label7;
			location = new Point(426, 43);
			arg_989_0.Location = location;
			this.Label7.Name = "Label7";
			Control arg_9B0_0 = this.Label7;
			size = new Size(13, 16);
			arg_9B0_0.Size = size;
			this.Label7.TabIndex = 21;
			this.Label7.Text = "-";
			this.Label8.AutoSize = true;
			this.Label8.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_A10_0 = this.Label8;
			location = new Point(351, 43);
			arg_A10_0.Location = location;
			this.Label8.Name = "Label8";
			Control arg_A37_0 = this.Label8;
			size = new Size(13, 16);
			arg_A37_0.Size = size;
			this.Label8.TabIndex = 20;
			this.Label8.Text = "-";
			this.Label9.AutoSize = true;
			this.Label9.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_A97_0 = this.Label9;
			location = new Point(276, 43);
			arg_A97_0.Location = location;
			this.Label9.Name = "Label9";
			Control arg_ABE_0 = this.Label9;
			size = new Size(13, 16);
			arg_ABE_0.Size = size;
			this.Label9.TabIndex = 19;
			this.Label9.Text = "-";
			this.Label10.AutoSize = true;
			this.Label10.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_B1E_0 = this.Label10;
			location = new Point(201, 43);
			arg_B1E_0.Location = location;
			this.Label10.Name = "Label10";
			Control arg_B45_0 = this.Label10;
			size = new Size(13, 16);
			arg_B45_0.Size = size;
			this.Label10.TabIndex = 18;
			this.Label10.Text = "-";
			this.PictureBox1.Image = (Image)componentResourceManager.GetObject("PictureBox1.Image");
			Control arg_B9A_0 = this.PictureBox1;
			location = new Point(450, 162);
			arg_B9A_0.Location = location;
			this.PictureBox1.Name = "PictureBox1";
			Control arg_BC1_0 = this.PictureBox1;
			size = new Size(52, 48);
			arg_BC1_0.Size = size;
			this.PictureBox1.TabIndex = 22;
			this.PictureBox1.TabStop = false;
			this.PictureBox2.Image = (Image)componentResourceManager.GetObject("PictureBox2.Image");
			Control arg_C0F_0 = this.PictureBox2;
			location = new Point(12, 160);
			arg_C0F_0.Location = location;
			this.PictureBox2.Name = "PictureBox2";
			Control arg_C36_0 = this.PictureBox2;
			size = new Size(52, 50);
			arg_C36_0.Size = size;
			this.PictureBox2.TabIndex = 23;
			this.PictureBox2.TabStop = false;
			this.Label11.AutoSize = true;
			this.Label11.BackColor = Color.GhostWhite;
			this.Label11.BorderStyle = BorderStyle.FixedSingle;
			Control arg_C8E_0 = this.Label11;
			location = new Point(19, 9);
			arg_C8E_0.Location = location;
			this.Label11.Name = "Label11";
			Control arg_CB5_0 = this.Label11;
			size = new Size(99, 15);
			arg_CB5_0.Size = size;
			this.Label11.TabIndex = 24;
			this.Label11.Text = "Build : Apr-2008-01";
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			this.BackColor = Color.MediumAquamarine;
			size = new Size(529, 222);
			this.ClientSize = size;
			this.Controls.Add(this.Label11);
			this.Controls.Add(this.PictureBox2);
			this.Controls.Add(this.PictureBox1);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Label9);
			this.Controls.Add(this.Label10);
			this.Controls.Add(this.number5);
			this.Controls.Add(this.number4);
			this.Controls.Add(this.number3);
			this.Controls.Add(this.number2);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.number1);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.TextBox5);
			this.Controls.Add(this.TextBox4);
			this.Controls.Add(this.TextBox3);
			this.Controls.Add(this.TextBox2);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.TextBox1);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_registation";
			this.Text = "Registration PHR ::: Pak Homeopathic Repertory 2008 Ver 1.0";
			((ISupportInitialize)this.PictureBox1).EndInit();
			((ISupportInitialize)this.PictureBox2).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			HDiskInfo.DiskInfo diskInfo = default(HDiskInfo.DiskInfo);
			if (Operators.CompareString(this.TextBox1.Text, "", false) != 0 & Operators.CompareString(this.TextBox2.Text, "", false) != 0 & Operators.CompareString(this.TextBox3.Text, "", false) != 0 & Operators.CompareString(this.TextBox4.Text, "", false) != 0 & Operators.CompareString(this.TextBox5.Text, "", false) != 0)
			{
				double num = Conversions.ToDouble(Conversion.Int(this.TextBox1.Text));
				num = num - 45838.0 + 35789.0;
				double num2 = Conversions.ToDouble(Conversion.Int(this.TextBox2.Text));
				num2 = num2 - 54354.0 + 56879.0;
				double num3 = Conversions.ToDouble(Conversion.Int(this.TextBox3.Text));
				num3 = num3 - 78549.0 + 85468.0;
				double num4 = Conversions.ToDouble(Conversion.Int(this.TextBox4.Text));
				num4 = num4 - 67894.0 + 85758.0;
				double num5 = Conversions.ToDouble(Conversion.Int(this.TextBox5.Text));
				num5 = num5 - 59875.0 + 65421.0;
				if (Conversions.ToBoolean(Operators.AndObject(Operators.AndObject(Operators.AndObject(Operators.AndObject(Operators.CompareObjectEqual(Conversion.Int(this.number1.Text), num, false), Operators.CompareObjectEqual(Conversion.Int(this.number2.Text), num2, false)), Operators.CompareObjectEqual(Conversion.Int(this.number3.Text), num3, false)), Operators.CompareObjectEqual(Conversion.Int(this.number4.Text), num4, false)), Operators.CompareObjectEqual(Conversion.Int(this.number5.Text), num5, false))))
				{
					short num6 = 0;
					short arg_23C_0 = num6;
					string text = "7MU98-E4EHR-UETYF-QNJNP-YU3RQ";
					HDiskInfo.GetIdeDiskInfo(arg_23C_0, ref diskInfo, ref text);
					string serialNumber = diskInfo.SerialNumber;
					MyProject.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM", "Setup2", serialNumber);
					frm_Login frm_Login = new frm_Login();
					frm_Login.Show();
					this.Close();
				}
				else
				{
					Interaction.MsgBox("Wrong Serial Number Plz try later", MsgBoxStyle.Information, null);
				}
			}
			else
			{
				Interaction.MsgBox("Please Give Serial Number", MsgBoxStyle.Information, null);
			}
		}

		private void TextBox1_TextChanged(object sender, EventArgs e)
		{
			if (this.TextBox1.TextLength == 5)
			{
				this.TextBox2.Focus();
			}
		}

		private void TextBox2_TextChanged(object sender, EventArgs e)
		{
			if (this.TextBox2.TextLength == 5)
			{
				this.TextBox3.Focus();
			}
		}

		private void TextBox3_TextChanged(object sender, EventArgs e)
		{
			if (this.TextBox3.TextLength == 5)
			{
				this.TextBox4.Focus();
			}
		}

		private void TextBox4_TextChanged(object sender, EventArgs e)
		{
			if (this.TextBox4.TextLength == 5)
			{
				this.TextBox5.Focus();
			}
		}

		private void frm_registation_Load(object sender, EventArgs e)
		{
			/*try
			{
				FileStream fileStream = new FileStream("" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\boen.txt", FileMode.Open, FileAccess.Read);
				fileStream.Close();
			}
			catch (Exception arg_17_0)
			{
				ProjectData.SetProjectError(arg_17_0);
				Interaction.MsgBox("Error Code: 0003-Pak-2008: Repertory will close now,Kindly obtain valid file from Homeopathic Dr. Mumtaz Ali Riaz  drmumtaz@urduhomeopath.com  Mobile Phone: 0321-7701904", MsgBoxStyle.OkOnly, null);
				this.Close();
				ProjectData.ClearProjectError();
			}
			try
			{
				FileStream fileStream2 = new FileStream("" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\def.txt", FileMode.Open, FileAccess.Read);
				fileStream2.Close();
			}
			catch (Exception arg_4D_0)
			{
				ProjectData.SetProjectError(arg_4D_0);
				Interaction.MsgBox("Error Code: 0004-Pak-2008: Repertory will close now,Kindly obtain valid file from Homeopathic Dr. Mumtaz Ali Riaz  drmumtaz@urduhomeopath.com  Mobile Phone: 0321-7701904", MsgBoxStyle.OkOnly, null);
				this.Close();
				ProjectData.ClearProjectError();
			}*/
			frm_Login frm_Login = new frm_Login();
            frm_Login.Show();
            this.Close();
			/*HDiskInfo.DiskInfo diskInfo = default(HDiskInfo.DiskInfo);
			short num = 0;
			this.PictureBox1.Visible = false;
			this.PictureBox2.Visible = false;
			string text = Conversions.ToString(MyProject.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM", "Setup2", null));
			short arg_C1_0 = num;
			string text2 = "7MU98-E4EHR-UETYF-QNJNP-YU3RQ";
			HDiskInfo.GetIdeDiskInfo(arg_C1_0, ref diskInfo, ref text2);
			string serialNumber = diskInfo.SerialNumber;
			checked
			{
				if (Operators.CompareString(text, "", false) != 0)
				{
					if (Operators.CompareString(serialNumber, text, false) == 0)
					{
						frm_Login.Show();
						this.Close();
					}
				}
				else
				{
					short arg_10B_0 = num;
					text2 = "7MU98-E4EHR-UETYF-QNJNP-YU3RQ";
					HDiskInfo.GetIdeDiskInfo(arg_10B_0, ref diskInfo, ref text2);
					serialNumber = diskInfo.SerialNumber;
					char[] array = Conversions.ToCharArrayRankOne(serialNumber);
					string text3 = new string(array);
					this.number1.Text = Conversions.ToString(Strings.Asc(array[0]) * 428);
					this.number2.Text = Conversions.ToString(Strings.Asc(array[1]) * 428);
					this.number3.Text = Conversions.ToString(Strings.Asc(array[2]) * 428);
					this.number4.Text = Conversions.ToString(Strings.Asc(array[3]) * 428);
					this.number5.Text = Conversions.ToString(Strings.Asc(array[4]) * 428);
				}
			}*/
		}

		private void TextBox5_LostFocus(object sender, EventArgs e)
		{
			double num = Conversions.ToDouble(Conversion.Int(this.TextBox1.Text));
			num = num - 45838.0 + 35789.0;
			double num2 = Conversions.ToDouble(Conversion.Int(this.TextBox2.Text));
			num2 = num2 - 54354.0 + 56879.0;
			double num3 = Conversions.ToDouble(Conversion.Int(this.TextBox3.Text));
			num3 = num3 - 78549.0 + 85468.0;
			double num4 = Conversions.ToDouble(Conversion.Int(this.TextBox4.Text));
			num4 = num4 - 67894.0 + 85758.0;
			double num5 = Conversions.ToDouble(Conversion.Int(this.TextBox5.Text));
			num5 = num5 - 59875.0 + 65421.0;
			if (Conversions.ToBoolean(Operators.AndObject(Operators.AndObject(Operators.AndObject(Operators.AndObject(Operators.CompareObjectEqual(Conversion.Int(this.number1.Text), num, false), Operators.CompareObjectEqual(Conversion.Int(this.number2.Text), num2, false)), Operators.CompareObjectEqual(Conversion.Int(this.number3.Text), num3, false)), Operators.CompareObjectEqual(Conversion.Int(this.number4.Text), num4, false)), Operators.CompareObjectEqual(Conversion.Int(this.number5.Text), num5, false))))
			{
				this.PictureBox2.Visible = true;
			}
			else
			{
				this.PictureBox1.Visible = true;
			}
		}

		private void PictureBox2_Click(object sender, EventArgs e)
		{
		}
	}
}
