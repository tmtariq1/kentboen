using ADODB;
using AxMSFlexGridLib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class frm_report1 : Form
	{
		private IContainer components;

		[AccessedThroughProperty("flexGrid1")]
		private AxMSFlexGrid _flexGrid1;

		[AccessedThroughProperty("print")]
		private Button _print;

		[AccessedThroughProperty("saveas")]
		private Button _saveas;

		[AccessedThroughProperty("cancel")]
		private Button _cancel;

		[AccessedThroughProperty("SaveFileDialog1")]
		private SaveFileDialog _SaveFileDialog1;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		private Connection con;

		private Recordset rst;

		internal virtual AxMSFlexGrid flexGrid1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._flexGrid1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._flexGrid1 = value;
			}
		}

		internal virtual Button print
		{
			[DebuggerNonUserCode]
			get
			{
				return this._print;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._print != null)
				{
					this._print.Click -= new EventHandler(this.print_Click);
				}
				this._print = value;
				if (this._print != null)
				{
					this._print.Click += new EventHandler(this.print_Click);
				}
			}
		}

		internal virtual Button saveas
		{
			[DebuggerNonUserCode]
			get
			{
				return this._saveas;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._saveas != null)
				{
					this._saveas.Click -= new EventHandler(this.saveas_Click);
				}
				this._saveas = value;
				if (this._saveas != null)
				{
					this._saveas.Click += new EventHandler(this.saveas_Click);
				}
			}
		}

		internal virtual Button cancel
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cancel;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cancel != null)
				{
					this._cancel.Click -= new EventHandler(this.cancel_Click);
				}
				this._cancel = value;
				if (this._cancel != null)
				{
					this._cancel.Click += new EventHandler(this.cancel_Click);
				}
			}
		}

		internal virtual SaveFileDialog SaveFileDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._SaveFileDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._SaveFileDialog1 = value;
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		public frm_report1()
		{
			base.Load += new EventHandler(this.frm_report1_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_report1));
			this.flexGrid1 = new AxMSFlexGrid();
			this.print = new Button();
			this.saveas = new Button();
			this.cancel = new Button();
			this.SaveFileDialog1 = new SaveFileDialog();
			this.Button1 = new Button();
			((ISupportInitialize)this.flexGrid1).BeginInit();
			this.SuspendLayout();
			Control arg_74_0 = this.flexGrid1;
			Point location = new Point(3, 54);
			arg_74_0.Location = location;
			this.flexGrid1.Name = "flexGrid1";
			this.flexGrid1.OcxState = (AxHost.State)componentResourceManager.GetObject("flexGrid1.OcxState");
			Control arg_BC_0 = this.flexGrid1;
			Size size = new Size(1013, 321);
			arg_BC_0.Size = size;
			this.flexGrid1.TabIndex = 0;
			this.print.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.print.ForeColor = Color.Red;
			Control arg_10A_0 = this.print;
			location = new Point(5, 0);
			arg_10A_0.Location = location;
			this.print.Name = "print";
			Control arg_131_0 = this.print;
			size = new Size(75, 48);
			arg_131_0.Size = size;
			this.print.TabIndex = 1;
			this.print.Text = "Print";
			this.print.UseVisualStyleBackColor = true;
			this.saveas.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.saveas.ForeColor = Color.Blue;
			Control arg_19C_0 = this.saveas;
			location = new Point(79, 0);
			arg_19C_0.Location = location;
			this.saveas.Name = "saveas";
			Control arg_1C3_0 = this.saveas;
			size = new Size(75, 48);
			arg_1C3_0.Size = size;
			this.saveas.TabIndex = 2;
			this.saveas.Text = "SaveAs";
			this.saveas.UseVisualStyleBackColor = true;
			this.cancel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cancel.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_238_0 = this.cancel;
			location = new Point(227, 0);
			arg_238_0.Location = location;
			this.cancel.Name = "cancel";
			Control arg_25F_0 = this.cancel;
			size = new Size(75, 48);
			arg_25F_0.Size = size;
			this.cancel.TabIndex = 3;
			this.cancel.Text = "Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			this.Button1.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button1.ForeColor = Color.FromArgb(255, 192, 128);
			Control arg_2DC_0 = this.Button1;
			location = new Point(153, 0);
			arg_2DC_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_303_0 = this.Button1;
			size = new Size(75, 48);
			arg_303_0.Size = size;
			this.Button1.TabIndex = 4;
			this.Button1.Text = "Excel Report";
			this.Button1.UseVisualStyleBackColor = true;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			this.BackColor = Color.Navy;
			size = new Size(1028, 387);
			this.ClientSize = size;
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.saveas);
			this.Controls.Add(this.print);
			this.Controls.Add(this.flexGrid1);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_report1";
			this.Text = "P H R ::: Pak Homeopathic Repertory 2008 Ver 1.0   ::: SYMPTOMS ANALYSIS  ::: ";
			((ISupportInitialize)this.flexGrid1).EndInit();
			this.ResumeLayout(false);
		}

		private void frm_report1_Load(object sender, EventArgs e)
		{
			checked
			{
				int num;
				int num12;
				try
				{
					ProjectData.ClearProjectError();
					num = 2;
					this.Connect_Database();
					this.flexGrid1.set_ColWidth(0, 5000);
					this.flexGrid1.set_ColWidth(1, 0);
					this.flexGrid1.set_RowHeight(0, 400);
					this.flexGrid1.set_RowHeight(1, 0);
					this.flexGrid1.Col = 0;
					this.flexGrid1.Row = 0;
					this.flexGrid1.Text = "Symptoms/Remedies";
					int num2 = 2;
					_Connection arg_8A_0 = this.con;
					string arg_8A_1 = "select distinct symname,symid from cal_result";
					object value = Missing.Value;
					this.rst = arg_8A_0.Execute(arg_8A_1, out value, -1);
					while (!this.rst.BOF & !this.rst.EOF)
					{
						this.flexGrid1.Rows = num2 + 2;
						this.flexGrid1.Row = num2;
						this.flexGrid1.Col = 0;
						this.flexGrid1.Text = Conversions.ToString(this.rst.Fields["symname"].Value);
						this.flexGrid1.Col = 1;
						this.flexGrid1.Text = Conversions.ToString(this.rst.Fields["symid"].Value);
						num2++;
						this.rst.MoveNext();
					}
					int num3 = 2;
					_Connection arg_168_0 = this.con;
					string arg_168_1 = "select distinct remid,rem from cal_result";
					value = Missing.Value;
					this.rst = arg_168_0.Execute(arg_168_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						this.flexGrid1.Cols = num3 + 1;
						this.flexGrid1.Col = num3;
						this.flexGrid1.Row = 0;
						this.flexGrid1.Text = Conversions.ToString(this.rst.Fields["rem"].Value);
						this.flexGrid1.Row = 1;
						this.flexGrid1.Text = Conversions.ToString(this.rst.Fields["remid"].Value);
						num3++;
						this.rst.MoveNext();
					}
					int arg_234_0 = 1;
					int num4 = num2 - 2;
					int j = 0;
					for (int i = arg_234_0; i <= num4; i++)
					{
						this.flexGrid1.Row = i + 1;
						this.flexGrid1.Col = 1;
						int value2 = (int)Math.Round(Conversion.Val(this.flexGrid1.Text));
						int arg_272_0 = 1;
						int num5 = num3 - 2;
						for (j = arg_272_0; j <= num5; j++)
						{
							this.flexGrid1.Row = 1;
							this.flexGrid1.Col = j + 1;
							int num6 = (int)Math.Round(Conversion.Val(this.flexGrid1.Text));
							this.flexGrid1.Row = i + 1;
							_Connection arg_30B_0 = this.con;
							string arg_30B_1 = string.Concat(new string[]
							{
								"select * from cal_result where symid=",
								Conversions.ToString(value2),
								" and remid=",
								Conversions.ToString(num6),
								" "
							});
							value = Missing.Value;
							this.rst = arg_30B_0.Execute(arg_30B_1, out value, -1);
							if (!this.rst.EOF & !this.rst.BOF)
							{
								this.flexGrid1.Col = j + 1;
								this.flexGrid1.Text = Conversions.ToString(this.rst.Fields["weight"].Value);
								object left = Conversion.Int(RuntimeHelpers.GetObjectValue(this.rst.Fields["weight"].Value));
								if (Operators.ConditionalCompareObjectEqual(left, 1, false))
								{
									this.flexGrid1.CellBackColor = Color.LightGreen;
								}
								else if (Operators.ConditionalCompareObjectEqual(left, 2, false))
								{
									this.flexGrid1.CellBackColor = Color.LightGray;
								}
								else if (Operators.ConditionalCompareObjectEqual(left, 3, false))
								{
									this.flexGrid1.CellBackColor = Color.GreenYellow;
								}
								else if (Operators.ConditionalCompareObjectEqual(left, 4, false))
								{
									this.flexGrid1.CellBackColor = Color.LawnGreen;
								}
							}
							else
							{
								this.flexGrid1.Text = Conversions.ToString(0);
							}
						}
					}
					this.flexGrid1.Row = this.flexGrid1.Rows - 1;
					this.flexGrid1.Col = 0;
					this.flexGrid1.Text = "Total";
					int arg_48E_0 = 2;
					int num7 = this.flexGrid1.Cols - 1;
					for (int i = arg_48E_0; i <= num7; i++)
					{
						int num6 = 0;
						this.flexGrid1.Col = i;
						int arg_4B3_0 = 2;
						int num8 = this.flexGrid1.Rows - 1;
						for (j = arg_4B3_0; j <= num8; j++)
						{
							this.flexGrid1.Row = j;
							num6 = (int)Math.Round(unchecked((double)num6 + Conversion.Val(this.flexGrid1.Text)));
						}
						this.flexGrid1.Row = j - 1;
						this.flexGrid1.Text = Conversions.ToString(num6);
						this.flexGrid1.CellBackColor = Color.LightBlue;
					}
					this.flexGrid1.Col = 2;
					this.flexGrid1.Row = j - 1;
					int num9 = Conversions.ToInteger(this.flexGrid1.Text);
					int arg_565_0 = 3;
					int num10 = this.flexGrid1.Cols - 1;
					for (int i = arg_565_0; i <= num10; i++)
					{
						this.flexGrid1.Col = i;
						if ((double)num9 < Conversion.Val(this.flexGrid1.Text))
						{
							num9 = (int)Math.Round(Conversion.Val(this.flexGrid1.Text));
						}
					}
					this.flexGrid1.Rows = this.flexGrid1.Rows + 1;
					this.flexGrid1.Col = 0;
					this.flexGrid1.Row = this.flexGrid1.Rows - 1;
					this.flexGrid1.Text = "Percentage";
					j = 0;
					int arg_607_0 = 2;
					int num11 = this.flexGrid1.Cols - 1;
					for (int i = arg_607_0; i <= num11; i++)
					{
						this.flexGrid1.Col = i;
						this.flexGrid1.Row = this.flexGrid1.Rows - 2;
						j = (int)Math.Round(Conversion.Val(this.flexGrid1.Text));
						this.flexGrid1.Row = this.flexGrid1.Rows - 1;
						this.flexGrid1.Text = Conversions.ToString(Conversion.Int((double)(j * 100) / (double)num9)) + "%";
						this.flexGrid1.CellBackColor = Color.LightSkyBlue;
					}
					IL_6A5:
					goto IL_6EC;
					num12 = -1;
					//@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num);
					IL_6BD:
					goto IL_6E1;
				}
                catch (Exception ex) { }
				object arg_6BF_0;
				//endfilter(arg_6BF_0 is Exception & num != 0 & num12 == 0);
				IL_6E1:
				//throw ProjectData.CreateProjectError(-2146828237);
				IL_6EC:
				//if (num12 != 0)
				{
					ProjectData.ClearProjectError();
				}
			}
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void saveas_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			checked
			{
				if (saveFileDialog.ShowDialog() == DialogResult.OK)
				{
					FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
					StreamWriter streamWriter = new StreamWriter(stream);
					Cursor.Current = Cursors.WaitCursor;
					int arg_52_0 = 2;
					int num = this.flexGrid1.Rows - 3;
					for (int i = arg_52_0; i <= num; i++)
					{
						this.flexGrid1.Col = 0;
						this.flexGrid1.Row = i;
						streamWriter.WriteLine(" ");
						streamWriter.WriteLine(" ");
						string text = this.flexGrid1.Text;
						streamWriter.WriteLine("SYMPTOM: " + text.ToUpper());
						streamWriter.WriteLine(" ");
						streamWriter.WriteLine("-----------------------------------------");
						streamWriter.WriteLine(" ");
						int arg_DF_0 = 2;
						int num2 = this.flexGrid1.Cols - 3;
						for (int j = arg_DF_0; j <= num2; j++)
						{
							this.flexGrid1.Row = 0;
							this.flexGrid1.Col = j;
							text = this.flexGrid1.Text;
							this.flexGrid1.Row = i;
							this.flexGrid1.Col = j;
							if (Operators.ConditionalCompareObjectGreater(Conversion.Int(this.flexGrid1.Text), 0, false))
							{
								streamWriter.WriteLine(text + Strings.Space(55 - text.Length) + this.flexGrid1.Text);
							}
						}
					}
					streamWriter.Close();
					Cursor.Current = Cursors.Arrow;
				}
			}
		}

		private void print_Click(object sender, EventArgs e)
		{
			FileStream stream = new FileStream("c:\\materia.doc", FileMode.Create, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream);
			Cursor.Current = Cursors.WaitCursor;
			int arg_2F_0 = 2;
			checked
			{
				int num = this.flexGrid1.Rows - 3;
				for (int i = arg_2F_0; i <= num; i++)
				{
					this.flexGrid1.Col = 0;
					this.flexGrid1.Row = i;
					streamWriter.WriteLine(" ");
					streamWriter.WriteLine(" ");
					string text = this.flexGrid1.Text;
					streamWriter.WriteLine("SYMPTOM: " + text.ToUpper());
					streamWriter.WriteLine(" ");
					streamWriter.WriteLine("-----------------------------------------");
					streamWriter.WriteLine(" ");
					int arg_BE_0 = 2;
					int num2 = this.flexGrid1.Cols - 3;
					for (int j = arg_BE_0; j <= num2; j++)
					{
						this.flexGrid1.Row = 0;
						this.flexGrid1.Col = j;
						text = this.flexGrid1.Text;
						this.flexGrid1.Row = i;
						this.flexGrid1.Col = j;
						if (Operators.ConditionalCompareObjectGreater(Conversion.Int(this.flexGrid1.Text), 0, false))
						{
							streamWriter.WriteLine(text + Strings.Space(55 - text.Length) + this.flexGrid1.Text);
						}
					}
				}
				streamWriter.Close();
				Process.Start(new ProcessStartInfo
				{
					UseShellExecute = true,
					Verb = "print",
					WindowStyle = ProcessWindowStyle.Hidden,
					FileName = "c:\\materia.doc"
				});
				Cursor.Current = Cursors.Arrow;
			}
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Excel Files (*.xls)|*.xls|All files|*.*";
			checked
			{
				if (saveFileDialog.ShowDialog() == DialogResult.OK)
				{
					object objectValue = RuntimeHelpers.GetObjectValue(Interaction.CreateObject("Excel.Application", ""));
					object objectValue2 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(NewLateBinding.LateGet(objectValue, null, "Workbooks", new object[0], null, null, null), null, "Add", new object[0], null, null, null));
					object objectValue3 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(objectValue2, null, "Worksheets", new object[]
					{
						1
					}, null, null, null));
					Cursor.Current = Cursors.WaitCursor;
					int arg_A8_0 = 0;
					int num = this.flexGrid1.Cols - 1;
					for (int i = arg_A8_0; i <= num; i++)
					{
						int num2 = 65;
						int arg_C3_0 = 0;
						int num3 = this.flexGrid1.Rows - 1;
						for (int j = arg_C3_0; j <= num3; j++)
						{
							this.flexGrid1.Col = i;
							this.flexGrid1.Row = j;
							if (i != 1 & j != 1)
							{
								NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
								{
									Conversions.ToString(Strings.Chr(num2)) + Conversions.ToString(i + 1)
								}, null, null, null), null, "Value", new object[]
								{
									this.flexGrid1.Text
								}, null, null, false, true);
							}
							num2++;
						}
					}
					object arg_1AA_0 = objectValue2;
					Type arg_1AA_1 = null;
					string arg_1AA_2 = "SaveAs";
					object[] array = new object[1];
					object[] arg_191_0 = array;
					int arg_191_1 = 0;
					SaveFileDialog saveFileDialog2 = saveFileDialog;
					arg_191_0[arg_191_1] = saveFileDialog2.FileName;
					object[] array2 = array;
					object[] arg_1AA_3 = array2;
					string[] arg_1AA_4 = null;
					Type[] arg_1AA_5 = null;
					bool[] array3 = new bool[]
					{
						true
					};
					NewLateBinding.LateCall(arg_1AA_0, arg_1AA_1, arg_1AA_2, arg_1AA_3, arg_1AA_4, arg_1AA_5, array3, true);
					if (array3[0])
					{
						saveFileDialog2.FileName = (string)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(array2[0]), typeof(string));
					}
					NewLateBinding.LateCall(objectValue, null, "Quit", new object[0], null, null, null, true);
				}
			}
		}
	}
}
