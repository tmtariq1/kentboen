using ADODB;
using AxMSFlexGridLib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	public class frm_Main : Form
	{
		private Connection con;

		private Recordset rst;

		private Recordset rst1;

		private Recordset rst2;

		private string DBPath;

		private byte open;

		[AccessedThroughProperty("ToolStrip1")]
		private ToolStrip _ToolStrip1;

		[AccessedThroughProperty("Tool_Button1")]
		private ToolStripButton _Tool_Button1;

		[AccessedThroughProperty("ToolStripButton2")]
		private ToolStripButton _ToolStripButton2;

		[AccessedThroughProperty("ToolStripButton3")]
		private ToolStripButton _ToolStripButton3;

		[AccessedThroughProperty("Button3")]
		private Button _Button3;

		[AccessedThroughProperty("ToolStripButton1")]
		private ToolStripButton _ToolStripButton1;

		[AccessedThroughProperty("FontDialog1")]
		private FontDialog _FontDialog1;

		[AccessedThroughProperty("Button4")]
		private Button _Button4;

		[AccessedThroughProperty("ToolStripButton4")]
		private ToolStripButton _ToolStripButton4;

		[AccessedThroughProperty("ToolStripButton5")]
		private ToolStripButton _ToolStripButton5;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("Label7")]
		private Label _Label7;

		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		[AccessedThroughProperty("ToolStripDropDownButton1")]
		private ToolStripDropDownButton _ToolStripDropDownButton1;

		[AccessedThroughProperty("load_book1")]
		private ToolStripMenuItem _load_book1;

		[AccessedThroughProperty("load_book2")]
		private ToolStripMenuItem _load_book2;

		[AccessedThroughProperty("Button5")]
		private Button _Button5;

		[AccessedThroughProperty("ImageList1")]
		private ImageList _ImageList1;

		[AccessedThroughProperty("ListBox1")]
		private ListBox _ListBox1;

		[AccessedThroughProperty("Label8")]
		private Label _Label8;

		[AccessedThroughProperty("Button6")]
		private Button _Button6;

		[AccessedThroughProperty("Label9")]
		private Label _Label9;

		[AccessedThroughProperty("Label10")]
		private Label _Label10;

		[AccessedThroughProperty("repertory")]
		private GroupBox _repertory;

		[AccessedThroughProperty("chapter")]
		private GroupBox _chapter;

		[AccessedThroughProperty("lst_chapter")]
		private ListBox _lst_chapter;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("lst_search_symptoms")]
		private ListBox _lst_search_symptoms;

		[AccessedThroughProperty("search3")]
		private TextBox _search3;

		[AccessedThroughProperty("search2")]
		private TextBox _search2;

		[AccessedThroughProperty("search11")]
		private TextBox _search11;

		[AccessedThroughProperty("Label11")]
		private Label _Label11;

		[AccessedThroughProperty("Label12")]
		private Label _Label12;

		[AccessedThroughProperty("Timer1")]
		private Timer _Timer1;

		[AccessedThroughProperty("BoggarToolStripMenuItem")]
		private ToolStripMenuItem _BoggarToolStripMenuItem;

		[AccessedThroughProperty("ToolStripMenuItem1")]
		private ToolStripMenuItem _ToolStripMenuItem1;

		[AccessedThroughProperty("Label6")]
		private Label _Label6;

		[AccessedThroughProperty("lst_symptoms")]
		private ListBox _lst_symptoms;

		[AccessedThroughProperty("Label13")]
		private Label _Label13;

		[AccessedThroughProperty("R2")]
		private RadioButton _R2;

		[AccessedThroughProperty("R1")]
		private RadioButton _R1;

		[AccessedThroughProperty("lst_symptoms1")]
		private ListBox _lst_symptoms1;

		[AccessedThroughProperty("lst_search_symptoms1")]
		private ListBox _lst_search_symptoms1;

		[AccessedThroughProperty("search3urdu")]
		private TextBox _search3urdu;

		[AccessedThroughProperty("search2urdu")]
		private TextBox _search2urdu;

		[AccessedThroughProperty("search11urdu")]
		private TextBox _search11urdu;

		[AccessedThroughProperty("ListBox2")]
		private ListBox _ListBox2;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Picture1")]
		private PictureBox _Picture1;

		[AccessedThroughProperty("Button9")]
		private Button _Button9;

		[AccessedThroughProperty("Button8")]
		private Button _Button8;

		[AccessedThroughProperty("CheckBox1")]
		private CheckBox _CheckBox1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		private IContainer components;

		[AccessedThroughProperty("cmd_rep")]
		private Button _cmd_rep;

		[AccessedThroughProperty("cmd_chapter")]
		private Button _cmd_chapter;

		[AccessedThroughProperty("cmd_exit")]
		private Button _cmd_exit;

		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[AccessedThroughProperty("cmd_result")]
		private Button _cmd_result;

		[AccessedThroughProperty("lst_symptom")]
		private TreeView _lst_symptom;

		[AccessedThroughProperty("result_grid")]
		private AxMSFlexGrid _result_grid;

		[AccessedThroughProperty("Materia")]
		private Button _Materia;

		internal virtual ToolStrip ToolStrip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStrip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStrip1 = value;
			}
		}

		internal virtual ToolStripButton Tool_Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Tool_Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Tool_Button1 != null)
				{
					this._Tool_Button1.Click -= new EventHandler(this.Tool_Button1_Click);
				}
				this._Tool_Button1 = value;
				if (this._Tool_Button1 != null)
				{
					this._Tool_Button1.Click += new EventHandler(this.Tool_Button1_Click);
				}
			}
		}

		internal virtual ToolStripButton ToolStripButton2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripButton2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripButton2 != null)
				{
					this._ToolStripButton2.Click -= new EventHandler(this.ToolStripButton2_Click);
				}
				this._ToolStripButton2 = value;
				if (this._ToolStripButton2 != null)
				{
					this._ToolStripButton2.Click += new EventHandler(this.ToolStripButton2_Click);
				}
			}
		}

		internal virtual ToolStripButton ToolStripButton3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripButton3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripButton3 != null)
				{
					this._ToolStripButton3.Click -= new EventHandler(this.ToolStripButton3_Click);
				}
				this._ToolStripButton3 = value;
				if (this._ToolStripButton3 != null)
				{
					this._ToolStripButton3.Click += new EventHandler(this.ToolStripButton3_Click);
				}
			}
		}

		internal virtual Button Button3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button3 != null)
				{
					this._Button3.Click -= new EventHandler(this.Button3_Click);
				}
				this._Button3 = value;
				if (this._Button3 != null)
				{
					this._Button3.Click += new EventHandler(this.Button3_Click);
				}
			}
		}

		internal virtual ToolStripButton ToolStripButton1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripButton1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripButton1 != null)
				{
					this._ToolStripButton1.Click -= new EventHandler(this.ToolStripButton1_Click);
				}
				this._ToolStripButton1 = value;
				if (this._ToolStripButton1 != null)
				{
					this._ToolStripButton1.Click += new EventHandler(this.ToolStripButton1_Click);
				}
			}
		}

		internal virtual FontDialog FontDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._FontDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._FontDialog1 = value;
			}
		}

		internal virtual Button Button4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button4 != null)
				{
					this._Button4.Click -= new EventHandler(this.Button4_Click);
				}
				this._Button4 = value;
				if (this._Button4 != null)
				{
					this._Button4.Click += new EventHandler(this.Button4_Click);
				}
			}
		}

		internal virtual ToolStripButton ToolStripButton4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripButton4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripButton4 != null)
				{
					this._ToolStripButton4.Click -= new EventHandler(this.ToolStripButton4_Click);
				}
				this._ToolStripButton4 = value;
				if (this._ToolStripButton4 != null)
				{
					this._ToolStripButton4.Click += new EventHandler(this.ToolStripButton4_Click);
				}
			}
		}

		internal virtual ToolStripButton ToolStripButton5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripButton5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripButton5 != null)
				{
					this._ToolStripButton5.Click -= new EventHandler(this.ToolStripButton5_Click);
				}
				this._ToolStripButton5 = value;
				if (this._ToolStripButton5 != null)
				{
					this._ToolStripButton5.Click += new EventHandler(this.ToolStripButton5_Click);
				}
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual Label Label7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label7 = value;
			}
		}

		internal virtual ToolTip ToolTip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip1 = value;
			}
		}

		internal virtual ToolStripDropDownButton ToolStripDropDownButton1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripDropDownButton1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ToolStripDropDownButton1 != null)
				{
					this._ToolStripDropDownButton1.Click -= new EventHandler(this.ToolStripDropDownButton1_Click);
				}
				this._ToolStripDropDownButton1 = value;
				if (this._ToolStripDropDownButton1 != null)
				{
					this._ToolStripDropDownButton1.Click += new EventHandler(this.ToolStripDropDownButton1_Click);
				}
			}
		}

		internal virtual ToolStripMenuItem load_book1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._load_book1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._load_book1 != null)
				{
					this._load_book1.Click -= new EventHandler(this.load_book1_Click);
				}
				this._load_book1 = value;
				if (this._load_book1 != null)
				{
					this._load_book1.Click += new EventHandler(this.load_book1_Click);
				}
			}
		}

		internal virtual ToolStripMenuItem load_book2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._load_book2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._load_book2 != null)
				{
					this._load_book2.Click -= new EventHandler(this.load_book2_Click);
				}
				this._load_book2 = value;
				if (this._load_book2 != null)
				{
					this._load_book2.Click += new EventHandler(this.load_book2_Click);
				}
			}
		}

		internal virtual Button Button5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button5 != null)
				{
					this._Button5.Click -= new EventHandler(this.Button5_Click);
				}
				this._Button5 = value;
				if (this._Button5 != null)
				{
					this._Button5.Click += new EventHandler(this.Button5_Click);
				}
			}
		}

		internal virtual ImageList ImageList1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ImageList1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ImageList1 = value;
			}
		}

		internal virtual ListBox ListBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ListBox1 != null)
				{
					this._ListBox1.Click -= new EventHandler(this.ListBox1_Click);
				}
				this._ListBox1 = value;
				if (this._ListBox1 != null)
				{
					this._ListBox1.Click += new EventHandler(this.ListBox1_Click);
				}
			}
		}

		internal virtual Label Label8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label8 = value;
			}
		}

		internal virtual Button Button6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button6 != null)
				{
					this._Button6.Click -= new EventHandler(this.Button6_Click);
				}
				this._Button6 = value;
				if (this._Button6 != null)
				{
					this._Button6.Click += new EventHandler(this.Button6_Click);
				}
			}
		}

		internal virtual Label Label9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label9 = value;
			}
		}

		internal virtual Label Label10
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label10;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label10 = value;
			}
		}

		internal virtual GroupBox repertory
		{
			[DebuggerNonUserCode]
			get
			{
				return this._repertory;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._repertory = value;
			}
		}

		internal virtual GroupBox chapter
		{
			[DebuggerNonUserCode]
			get
			{
				return this._chapter;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._chapter = value;
			}
		}

		internal virtual ListBox lst_chapter
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_chapter;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_chapter != null)
				{
					this._lst_chapter.SelectedIndexChanged -= new EventHandler(this.lst_chapter_SelectedIndexChanged);
				}
				this._lst_chapter = value;
				if (this._lst_chapter != null)
				{
					this._lst_chapter.SelectedIndexChanged += new EventHandler(this.lst_chapter_SelectedIndexChanged);
				}
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual ListBox lst_search_symptoms
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_search_symptoms;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_search_symptoms != null)
				{
					this._lst_search_symptoms.SelectedIndexChanged -= new EventHandler(this.lst_search_symptoms_SelectedIndexChanged);
				}
				this._lst_search_symptoms = value;
				if (this._lst_search_symptoms != null)
				{
					this._lst_search_symptoms.SelectedIndexChanged += new EventHandler(this.lst_search_symptoms_SelectedIndexChanged);
				}
			}
		}

		internal virtual TextBox search3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search3 = value;
			}
		}

		internal virtual TextBox search2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search2 = value;
			}
		}

		internal virtual TextBox search11
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search11;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._search11 != null)
				{
					this._search11.TextChanged -= new EventHandler(this.search11_TextChanged);
				}
				this._search11 = value;
				if (this._search11 != null)
				{
					this._search11.TextChanged += new EventHandler(this.search11_TextChanged);
				}
			}
		}

		internal virtual Label Label11
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label11;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label11 = value;
			}
		}

		internal virtual Label Label12
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label12;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label12 = value;
			}
		}

		internal virtual Timer Timer1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Timer1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Timer1 != null)
				{
					this._Timer1.Tick -= new EventHandler(this.Timer1_Tick);
				}
				this._Timer1 = value;
				if (this._Timer1 != null)
				{
					this._Timer1.Tick += new EventHandler(this.Timer1_Tick);
				}
			}
		}

		internal virtual ToolStripMenuItem BoggarToolStripMenuItem
		{
			[DebuggerNonUserCode]
			get
			{
				return this._BoggarToolStripMenuItem;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._BoggarToolStripMenuItem != null)
				{
					this._BoggarToolStripMenuItem.Click -= new EventHandler(this.BoggarToolStripMenuItem_Click);
				}
				this._BoggarToolStripMenuItem = value;
				if (this._BoggarToolStripMenuItem != null)
				{
					this._BoggarToolStripMenuItem.Click += new EventHandler(this.BoggarToolStripMenuItem_Click);
				}
			}
		}

		internal virtual ToolStripMenuItem ToolStripMenuItem1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolStripMenuItem1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolStripMenuItem1 = value;
			}
		}

		internal virtual Label Label6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label6 = value;
			}
		}

		internal virtual ListBox lst_symptoms
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_symptoms;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_symptoms != null)
				{
					this._lst_symptoms.SelectedIndexChanged -= new EventHandler(this.lst_symptoms_SelectedIndexChanged);
				}
				this._lst_symptoms = value;
				if (this._lst_symptoms != null)
				{
					this._lst_symptoms.SelectedIndexChanged += new EventHandler(this.lst_symptoms_SelectedIndexChanged);
				}
			}
		}

		internal virtual Label Label13
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label13;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label13 = value;
			}
		}

		internal virtual RadioButton R2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._R2 != null)
				{
					this._R2.CheckedChanged -= new EventHandler(this.R2_CheckedChanged);
					this._R2.Click -= new EventHandler(this.R2_Click);
				}
				this._R2 = value;
				if (this._R2 != null)
				{
					this._R2.CheckedChanged += new EventHandler(this.R2_CheckedChanged);
					this._R2.Click += new EventHandler(this.R2_Click);
				}
			}
		}

		internal virtual RadioButton R1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._R1 != null)
				{
					this._R1.CheckedChanged -= new EventHandler(this.R1_CheckedChanged);
					this._R1.Click -= new EventHandler(this.R1_Click);
				}
				this._R1 = value;
				if (this._R1 != null)
				{
					this._R1.CheckedChanged += new EventHandler(this.R1_CheckedChanged);
					this._R1.Click += new EventHandler(this.R1_Click);
				}
			}
		}

		internal virtual ListBox lst_symptoms1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_symptoms1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_symptoms1 != null)
				{
					this._lst_symptoms1.SelectedIndexChanged -= new EventHandler(this.lst_symptoms1_SelectedIndexChanged);
				}
				this._lst_symptoms1 = value;
				if (this._lst_symptoms1 != null)
				{
					this._lst_symptoms1.SelectedIndexChanged += new EventHandler(this.lst_symptoms1_SelectedIndexChanged);
				}
			}
		}

		internal virtual ListBox lst_search_symptoms1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_search_symptoms1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_search_symptoms1 != null)
				{
					this._lst_search_symptoms1.SelectedIndexChanged -= new EventHandler(this.lst_search_symptoms1_SelectedIndexChanged);
				}
				this._lst_search_symptoms1 = value;
				if (this._lst_search_symptoms1 != null)
				{
					this._lst_search_symptoms1.SelectedIndexChanged += new EventHandler(this.lst_search_symptoms1_SelectedIndexChanged);
				}
			}
		}

		internal virtual TextBox search3urdu
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search3urdu;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search3urdu = value;
			}
		}

		internal virtual TextBox search2urdu
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search2urdu;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search2urdu = value;
			}
		}

		internal virtual TextBox search11urdu
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search11urdu;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search11urdu = value;
			}
		}

		internal virtual ListBox ListBox2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ListBox2 != null)
				{
					this._ListBox2.SelectedIndexChanged -= new EventHandler(this.ListBox2_SelectedIndexChanged);
				}
				this._ListBox2 = value;
				if (this._ListBox2 != null)
				{
					this._ListBox2.SelectedIndexChanged += new EventHandler(this.ListBox2_SelectedIndexChanged);
				}
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual PictureBox Picture1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Picture1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Picture1 = value;
			}
		}

		internal virtual Button Button9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button9 != null)
				{
					this._Button9.Click -= new EventHandler(this.Button9_Click);
				}
				this._Button9 = value;
				if (this._Button9 != null)
				{
					this._Button9.Click += new EventHandler(this.Button9_Click);
				}
			}
		}

		internal virtual Button Button8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button8 != null)
				{
					this._Button8.Click -= new EventHandler(this.Button8_Click);
				}
				this._Button8 = value;
				if (this._Button8 != null)
				{
					this._Button8.Click += new EventHandler(this.Button8_Click);
				}
			}
		}

		internal virtual CheckBox CheckBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._CheckBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._CheckBox1 != null)
				{
					this._CheckBox1.CheckedChanged -= new EventHandler(this.CheckBox1_CheckedChanged);
				}
				this._CheckBox1 = value;
				if (this._CheckBox1 != null)
				{
					this._CheckBox1.CheckedChanged += new EventHandler(this.CheckBox1_CheckedChanged);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual Button cmd_rep
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cmd_rep;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cmd_rep != null)
				{
					this._cmd_rep.Click -= new EventHandler(this.cmd_rep_Click);
				}
				this._cmd_rep = value;
				if (this._cmd_rep != null)
				{
					this._cmd_rep.Click += new EventHandler(this.cmd_rep_Click);
				}
			}
		}

		internal virtual Button cmd_chapter
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cmd_chapter;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cmd_chapter != null)
				{
					this._cmd_chapter.Click -= new EventHandler(this.cmd_chapter_Click);
				}
				this._cmd_chapter = value;
				if (this._cmd_chapter != null)
				{
					this._cmd_chapter.Click += new EventHandler(this.cmd_chapter_Click);
				}
			}
		}

		internal virtual Button cmd_exit
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cmd_exit;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cmd_exit != null)
				{
					this._cmd_exit.Click -= new EventHandler(this.cmd_exit_Click);
				}
				this._cmd_exit = value;
				if (this._cmd_exit != null)
				{
					this._cmd_exit.Click += new EventHandler(this.cmd_exit_Click);
				}
			}
		}

		internal virtual GroupBox GroupBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._GroupBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._GroupBox1 = value;
			}
		}

		internal virtual Button cmd_result
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cmd_result;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cmd_result != null)
				{
					this._cmd_result.Click -= new EventHandler(this.cmd_result_Click);
				}
				this._cmd_result = value;
				if (this._cmd_result != null)
				{
					this._cmd_result.Click += new EventHandler(this.cmd_result_Click);
				}
			}
		}

		internal virtual TreeView lst_symptom
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_symptom;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_symptom != null)
				{
					this._lst_symptom.AfterSelect -= new TreeViewEventHandler(this.lst_symptom_AfterSelect);
				}
				this._lst_symptom = value;
				if (this._lst_symptom != null)
				{
					this._lst_symptom.AfterSelect += new TreeViewEventHandler(this.lst_symptom_AfterSelect);
				}
			}
		}

		internal virtual AxMSFlexGrid result_grid
		{
			[DebuggerNonUserCode]
			get
			{
				return this._result_grid;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._result_grid != null)
				{
					this._result_grid.MouseMoveEvent -= new DMSFlexGridEvents_MouseMoveEventHandler(this.result_grid_MouseMoveEvent);
					this._result_grid.ClickEvent -= new EventHandler(this.result_grid_ClickEvent);
				}
				this._result_grid = value;
				if (this._result_grid != null)
				{
					this._result_grid.MouseMoveEvent += new DMSFlexGridEvents_MouseMoveEventHandler(this.result_grid_MouseMoveEvent);
					this._result_grid.ClickEvent += new EventHandler(this.result_grid_ClickEvent);
				}
			}
		}

		internal virtual Button Materia
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Materia;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Materia != null)
				{
					this._Materia.Click -= new EventHandler(this.Materia_Click);
				}
				this._Materia = value;
				if (this._Materia != null)
				{
					this._Materia.Click += new EventHandler(this.Materia_Click);
				}
			}
		}

		public void Connect_Database()
		{
			this.open = 1;
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.DBPath + ";";
			this.con.Open("", "", "", -1);
			_Connection arg_57_0 = this.con;
			string arg_57_1 = "select *  from Categories";
			object value = Missing.Value;
			this.rst = arg_57_0.Execute(arg_57_1, out value, -1);
			_Connection arg_75_0 = this.con;
			string arg_75_1 = "delete * from cal_result";
			value = Missing.Value;
			arg_75_0.Execute(arg_75_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				this.lst_chapter.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["CatName"].Value));
				this.rst.MoveNext();
			}
		}

		public frm_Main()
		{
			base.Load += new EventHandler(this.frm_Main_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.rst1 = new RecordsetClass();
			this.rst2 = new RecordsetClass();
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_Main));
			this.cmd_rep = new Button();
			this.cmd_chapter = new Button();
			this.cmd_exit = new Button();
			this.GroupBox1 = new GroupBox();
			this.ListBox2 = new ListBox();
			this.ListBox1 = new ListBox();
			this.lst_symptom = new TreeView();
			this.ImageList1 = new ImageList(this.components);
			this.result_grid = new AxMSFlexGrid();
			this.cmd_result = new Button();
			this.Materia = new Button();
			this.Button2 = new Button();
			this.ToolStrip1 = new ToolStrip();
			this.Tool_Button1 = new ToolStripButton();
			this.ToolStripButton2 = new ToolStripButton();
			this.ToolStripButton4 = new ToolStripButton();
			this.ToolStripButton3 = new ToolStripButton();
			this.ToolStripButton1 = new ToolStripButton();
			this.ToolStripButton5 = new ToolStripButton();
			this.ToolStripDropDownButton1 = new ToolStripDropDownButton();
			this.load_book1 = new ToolStripMenuItem();
			this.load_book2 = new ToolStripMenuItem();
			this.BoggarToolStripMenuItem = new ToolStripMenuItem();
			this.Button3 = new Button();
			this.FontDialog1 = new FontDialog();
			this.Button4 = new Button();
			this.Label2 = new Label();
			this.Label5 = new Label();
			this.Label7 = new Label();
			this.ToolTip1 = new ToolTip(this.components);
			this.Picture1 = new PictureBox();
			this.Label8 = new Label();
			this.Button6 = new Button();
			this.search3urdu = new TextBox();
			this.search2urdu = new TextBox();
			this.search11urdu = new TextBox();
			this.search3 = new TextBox();
			this.search2 = new TextBox();
			this.search11 = new TextBox();
			this.Button9 = new Button();
			this.Button8 = new Button();
			this.Label9 = new Label();
			this.lst_search_symptoms1 = new ListBox();
			this.Label3 = new Label();
			this.lst_symptoms1 = new ListBox();
			this.R2 = new RadioButton();
			this.R1 = new RadioButton();
			this.lst_chapter = new ListBox();
			this.CheckBox1 = new CheckBox();
			this.Button5 = new Button();
			this.Label10 = new Label();
			this.repertory = new GroupBox();
			this.Label13 = new Label();
			this.Button1 = new Button();
			this.lst_search_symptoms = new ListBox();
			this.chapter = new GroupBox();
			this.lst_symptoms = new ListBox();
			this.Label11 = new Label();
			this.Label12 = new Label();
			this.Timer1 = new Timer(this.components);
			this.Label6 = new Label();
			this.GroupBox1.SuspendLayout();
			((ISupportInitialize)this.result_grid).BeginInit();
			this.ToolStrip1.SuspendLayout();
			((ISupportInitialize)this.Picture1).BeginInit();
			this.repertory.SuspendLayout();
			this.chapter.SuspendLayout();
			this.SuspendLayout();
			this.cmd_rep.BackColor = Color.Tan;
			this.cmd_rep.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_352_0 = this.cmd_rep;
			Point location = new Point(4, 28);
			arg_352_0.Location = location;
			this.cmd_rep.Name = "cmd_rep";
			Control arg_379_0 = this.cmd_rep;
			Size size = new Size(75, 60);
			arg_379_0.Size = size;
			this.cmd_rep.TabIndex = 1;
			this.cmd_rep.Text = "Words Search";
			this.ToolTip1.SetToolTip(this.cmd_rep, "Search Repertory By Three Different Search Words");
			this.cmd_rep.UseVisualStyleBackColor = false;
			this.cmd_chapter.BackColor = Color.Tan;
			this.cmd_chapter.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmd_chapter.ImageAlign = ContentAlignment.TopCenter;
			Control arg_407_0 = this.cmd_chapter;
			location = new Point(80, 28);
			arg_407_0.Location = location;
			this.cmd_chapter.Name = "cmd_chapter";
			Control arg_42E_0 = this.cmd_chapter;
			size = new Size(75, 60);
			arg_42E_0.Size = size;
			this.cmd_chapter.TabIndex = 4;
			this.cmd_chapter.Text = "Chapter Search";
			this.ToolTip1.SetToolTip(this.cmd_chapter, "Search Repertory In Order OF Chapters");
			this.cmd_chapter.UseVisualStyleBackColor = false;
			this.cmd_exit.BackColor = Color.Brown;
			this.cmd_exit.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmd_exit.ForeColor = Color.FromArgb(255, 255, 192);
			this.cmd_exit.ImageAlign = ContentAlignment.MiddleLeft;
			Control arg_4DF_0 = this.cmd_exit;
			location = new Point(661, 28);
			arg_4DF_0.Location = location;
			this.cmd_exit.Name = "cmd_exit";
			Control arg_506_0 = this.cmd_exit;
			size = new Size(45, 60);
			arg_506_0.Size = size;
			this.cmd_exit.TabIndex = 6;
			this.cmd_exit.Text = "Exit";
			this.ToolTip1.SetToolTip(this.cmd_exit, "Exit to close the program");
			this.cmd_exit.UseVisualStyleBackColor = false;
			this.GroupBox1.Controls.Add(this.ListBox2);
			this.GroupBox1.Controls.Add(this.ListBox1);
			this.GroupBox1.Controls.Add(this.lst_symptom);
			Control arg_5A0_0 = this.GroupBox1;
			location = new Point(12, 440);
			arg_5A0_0.Location = location;
			this.GroupBox1.Name = "GroupBox1";
			Control arg_5CD_0 = this.GroupBox1;
			size = new Size(696, 251);
			arg_5CD_0.Size = size;
			this.GroupBox1.TabIndex = 9;
			this.GroupBox1.TabStop = false;
			this.ListBox2.BackColor = Color.WhiteSmoke;
			this.ListBox2.BorderStyle = BorderStyle.FixedSingle;
			this.ListBox2.Font = new Font("Nafees Nastaleeq", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.ListBox2.ForeColor = Color.FromArgb(128, 64, 0);
			this.ListBox2.FormattingEnabled = true;
			this.ListBox2.HorizontalScrollbar = true;
			this.ListBox2.ItemHeight = 31;
			Control arg_673_0 = this.ListBox2;
			location = new Point(10, 83);
			arg_673_0.Location = location;
			this.ListBox2.Name = "ListBox2";
			this.ListBox2.RightToLeft = RightToLeft.Yes;
			Control arg_6AC_0 = this.ListBox2;
			size = new Size(680, 157);
			arg_6AC_0.Size = size;
			this.ListBox2.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.ListBox2, "آپ کی طرف سے منتخب کردہ علامات ، اگر اس فہرست سے اخراج چاھتے ہیں تو علامت کو کلک کریں۔");
			this.ListBox1.ForeColor = Color.FromArgb(192, 0, 0);
			this.ListBox1.FormattingEnabled = true;
			this.ListBox1.ItemHeight = 18;
			Control arg_715_0 = this.ListBox1;
			location = new Point(10, 20);
			arg_715_0.Location = location;
			this.ListBox1.Name = "ListBox1";
			Control arg_73F_0 = this.ListBox1;
			size = new Size(680, 58);
			arg_73F_0.Size = size;
			this.ListBox1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.ListBox1, "Symptoms in english as selected by you ، To remove from this list click on a symptom then Press Result Button");
			this.lst_symptom.FullRowSelect = true;
			this.lst_symptom.ImageIndex = 0;
			this.lst_symptom.ImageList = this.ImageList1;
			this.lst_symptom.ImeMode = ImeMode.On;
			Control arg_7B0_0 = this.lst_symptom;
			location = new Point(129, 19);
			arg_7B0_0.Location = location;
			this.lst_symptom.Name = "lst_symptom";
			this.lst_symptom.SelectedImageIndex = 0;
			this.lst_symptom.ShowNodeToolTips = true;
			Control arg_7EF_0 = this.lst_symptom;
			size = new Size(56, 19);
			arg_7EF_0.Size = size;
			this.lst_symptom.TabIndex = 0;
			this.ImageList1.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("ImageList1.ImageStream");
			this.ImageList1.TransparentColor = Color.Transparent;
			this.ImageList1.Images.SetKeyName(0, "file8.ico");
			this.ImageList1.Images.SetKeyName(1, "REPORT.ICO");
			Control arg_86F_0 = this.result_grid;
			location = new Point(708, 459);
			arg_86F_0.Location = location;
			this.result_grid.Name = "result_grid";
			this.result_grid.OcxState = (AxHost.State)componentResourceManager.GetObject("result_grid.OcxState");
			Control arg_8B7_0 = this.result_grid;
			size = new Size(299, 231);
			arg_8B7_0.Size = size;
			this.result_grid.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.result_grid, "ادویات کے نام ، گریڈز کا ٹوٹل، اور فی صد حاصل");
			this.cmd_result.BackColor = Color.Lavender;
			this.cmd_result.Font = new Font("Trebuchet MS", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmd_result.ForeColor = Color.FromArgb(192, 0, 0);
			this.cmd_result.Image = (Image)componentResourceManager.GetObject("cmd_result.Image");
			this.cmd_result.ImageAlign = ContentAlignment.MiddleLeft;
			Control arg_963_0 = this.cmd_result;
			location = new Point(711, 431);
			arg_963_0.Location = location;
			this.cmd_result.Name = "cmd_result";
			Control arg_98A_0 = this.cmd_result;
			size = new Size(75, 31);
			arg_98A_0.Size = size;
			this.cmd_result.TabIndex = 11;
			this.cmd_result.Text = "Result";
			this.cmd_result.TextAlign = ContentAlignment.MiddleRight;
			this.cmd_result.UseVisualStyleBackColor = false;
			this.Materia.BackColor = Color.Tan;
			this.Materia.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Materia.ImageAlign = ContentAlignment.TopCenter;
			Control arg_A13_0 = this.Materia;
			location = new Point(232, 28);
			arg_A13_0.Location = location;
			this.Materia.Name = "Materia";
			Control arg_A3A_0 = this.Materia;
			size = new Size(75, 60);
			arg_A3A_0.Size = size;
			this.Materia.TabIndex = 12;
			this.Materia.Text = "Reverse Materia";
			this.ToolTip1.SetToolTip(this.Materia, "Show Reversed Repertory Symptoms For Remedies");
			this.Materia.UseVisualStyleBackColor = false;
			this.Button2.BackColor = Color.Tan;
			this.Button2.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button2.ImageAlign = ContentAlignment.TopCenter;
			Control arg_ACC_0 = this.Button2;
			location = new Point(308, 28);
			arg_ACC_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_AF3_0 = this.Button2;
			size = new Size(75, 60);
			arg_AF3_0.Size = size;
			this.Button2.TabIndex = 13;
			this.Button2.Text = "Remedies Family";
			this.ToolTip1.SetToolTip(this.Button2, "Show Remedies-Families Grouping");
			this.Button2.UseVisualStyleBackColor = false;
			this.ToolStrip1.Items.AddRange(new ToolStripItem[]
			{
				this.Tool_Button1,
				this.ToolStripButton2,
				this.ToolStripButton4,
				this.ToolStripButton3,
				this.ToolStripButton1,
				this.ToolStripButton5,
				this.ToolStripDropDownButton1
			});
			Control arg_B9E_0 = this.ToolStrip1;
			location = new Point(0, 0);
			arg_B9E_0.Location = location;
			this.ToolStrip1.Name = "ToolStrip1";
			Control arg_BC8_0 = this.ToolStrip1;
			size = new Size(1028, 25);
			arg_BC8_0.Size = size;
			this.ToolStrip1.TabIndex = 14;
			this.ToolStrip1.Text = "ToolStrip1";
			this.Tool_Button1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.Tool_Button1.Image = (Image)componentResourceManager.GetObject("Tool_Button1.Image");
			this.Tool_Button1.ImageTransparentColor = Color.Magenta;
			this.Tool_Button1.Name = "Tool_Button1";
			ToolStripItem arg_C43_0 = this.Tool_Button1;
			size = new Size(23, 22);
			arg_C43_0.Size = size;
			this.Tool_Button1.Text = "ToolStripButton1";
			this.Tool_Button1.ToolTipText = "New Patient";
			this.ToolStripButton2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripButton2.Image = (Image)componentResourceManager.GetObject("ToolStripButton2.Image");
			this.ToolStripButton2.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton2.Name = "ToolStripButton2";
			ToolStripItem arg_CC1_0 = this.ToolStripButton2;
			size = new Size(23, 22);
			arg_CC1_0.Size = size;
			this.ToolStripButton2.Text = "ToolStripButton2";
			this.ToolStripButton2.ToolTipText = "open previous patient";
			this.ToolStripButton4.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripButton4.Image = (Image)componentResourceManager.GetObject("ToolStripButton4.Image");
			this.ToolStripButton4.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton4.Name = "ToolStripButton4";
			ToolStripItem arg_D3F_0 = this.ToolStripButton4;
			size = new Size(23, 22);
			arg_D3F_0.Size = size;
			this.ToolStripButton4.Text = "ToolStripButton4";
			this.ToolStripButton4.ToolTipText = "Doctor Properties";
			this.ToolStripButton3.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripButton3.Enabled = false;
			this.ToolStripButton3.Image = (Image)componentResourceManager.GetObject("ToolStripButton3.Image");
			this.ToolStripButton3.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton3.Name = "ToolStripButton3";
			ToolStripItem arg_DC9_0 = this.ToolStripButton3;
			size = new Size(23, 22);
			arg_DC9_0.Size = size;
			this.ToolStripButton3.Text = "ToolStripButton3";
			this.ToolStripButton3.ToolTipText = "Save patient";
			this.ToolStripButton3.Visible = false;
			this.ToolStripButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripButton1.Enabled = false;
			this.ToolStripButton1.Image = (Image)componentResourceManager.GetObject("ToolStripButton1.Image");
			this.ToolStripButton1.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton1.Name = "ToolStripButton1";
			ToolStripItem arg_E5F_0 = this.ToolStripButton1;
			size = new Size(23, 22);
			arg_E5F_0.Size = size;
			this.ToolStripButton1.Text = "Change Font Properties";
			this.ToolStripButton1.Visible = false;
			this.ToolStripButton5.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripButton5.Image = (Image)componentResourceManager.GetObject("ToolStripButton5.Image");
			this.ToolStripButton5.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton5.Name = "ToolStripButton5";
			ToolStripItem arg_ED9_0 = this.ToolStripButton5;
			size = new Size(23, 22);
			arg_ED9_0.Size = size;
			this.ToolStripButton5.Text = "ToolStripButton5";
			this.ToolStripButton5.ToolTipText = "Change Password";
			this.ToolStripDropDownButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.ToolStripDropDownButton1.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.load_book1,
				this.load_book2,
				this.BoggarToolStripMenuItem
			});
			this.ToolStripDropDownButton1.Image = (Image)componentResourceManager.GetObject("ToolStripDropDownButton1.Image");
			this.ToolStripDropDownButton1.ImageTransparentColor = Color.Magenta;
			this.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1";
			ToolStripItem arg_F8A_0 = this.ToolStripDropDownButton1;
			size = new Size(29, 22);
			arg_F8A_0.Size = size;
			this.ToolStripDropDownButton1.Text = "ToolStripDropDownButton1";
			this.ToolStripDropDownButton1.ToolTipText = "Select Reporty Book";
			this.load_book1.Name = "load_book1";
			ToolStripItem arg_FD4_0 = this.load_book1;
			size = new Size(191, 22);
			arg_FD4_0.Size = size;
			this.load_book1.Text = "Boenninghausen Loaded";
			this.load_book2.Name = "load_book2";
			ToolStripItem arg_100E_0 = this.load_book2;
			size = new Size(191, 22);
			arg_100E_0.Size = size;
			this.load_book2.Text = "Kent Repertory";
			this.BoggarToolStripMenuItem.Name = "BoggarToolStripMenuItem";
			ToolStripItem arg_1048_0 = this.BoggarToolStripMenuItem;
			size = new Size(191, 22);
			arg_1048_0.Size = size;
			this.BoggarToolStripMenuItem.Text = "Riaz Big Repertory";
			this.Button3.BackColor = Color.Tan;
			this.Button3.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button3.ImageAlign = ContentAlignment.TopCenter;
			Control arg_10AB_0 = this.Button3;
			location = new Point(383, 28);
			arg_10AB_0.Location = location;
			this.Button3.Name = "Button3";
			Control arg_10D2_0 = this.Button3;
			size = new Size(78, 60);
			arg_10D2_0.Size = size;
			this.Button3.TabIndex = 15;
			this.Button3.Text = "New Patient";
			this.ToolTip1.SetToolTip(this.Button3, "Add New Patient Detail");
			this.Button3.UseVisualStyleBackColor = false;
			this.Button4.BackColor = Color.Tan;
			this.Button4.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button4.ImageAlign = ContentAlignment.TopCenter;
			Control arg_1164_0 = this.Button4;
			location = new Point(156, 28);
			arg_1164_0.Location = location;
			this.Button4.Name = "Button4";
			Control arg_118B_0 = this.Button4;
			size = new Size(75, 60);
			arg_118B_0.Size = size;
			this.Button4.TabIndex = 16;
			this.Button4.Text = "Symptoms Analysis";
			this.ToolTip1.SetToolTip(this.Button4, "Symptoms Analysis , This button should not be pressed before you select symptoms using \"Words Search\" or \"Chapter Search\",otherwise program will generate errors in running.");
			this.Button4.UseVisualStyleBackColor = false;
			this.Label2.AutoSize = true;
			this.Label2.BackColor = Color.Linen;
			this.Label2.Font = new Font("Trebuchet MS", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = Color.FromArgb(192, 64, 0);
			Control arg_1235_0 = this.Label2;
			location = new Point(598, 113);
			arg_1235_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_125B_0 = this.Label2;
			size = new Size(0, 20);
			arg_125B_0.Size = size;
			this.Label2.TabIndex = 18;
			this.Label5.AutoSize = true;
			Control arg_128E_0 = this.Label5;
			location = new Point(874, 25);
			arg_128E_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_12B5_0 = this.Label5;
			size = new Size(43, 18);
			arg_12B5_0.Size = size;
			this.Label5.TabIndex = 19;
			this.Label5.Text = "Label5";
			this.Label5.Visible = false;
			this.Label7.AutoSize = true;
			Control arg_1304_0 = this.Label7;
			location = new Point(784, 25);
			arg_1304_0.Location = location;
			this.Label7.Name = "Label7";
			Control arg_132B_0 = this.Label7;
			size = new Size(43, 18);
			arg_132B_0.Size = size;
			this.Label7.TabIndex = 21;
			this.Label7.Text = "Label7";
			this.Label7.Visible = false;
			this.ToolTip1.AutoPopDelay = 5000;
			this.ToolTip1.BackColor = Color.White;
			this.ToolTip1.ForeColor = Color.FromArgb(192, 0, 192);
			this.ToolTip1.InitialDelay = 0;
			this.ToolTip1.IsBalloon = true;
			this.ToolTip1.ReshowDelay = 0;
			this.ToolTip1.ToolTipTitle = "Pak Homeopathic Repertory 2008 Ver 1.0   :::: HELP";
			this.Picture1.BackColor = Color.AntiqueWhite;
			this.Picture1.ErrorImage = null;
			Control arg_13F9_0 = this.Picture1;
			location = new Point(891, 28);
			arg_13F9_0.Location = location;
			this.Picture1.Name = "Picture1";
			Control arg_1420_0 = this.Picture1;
			size = new Size(116, 100);
			arg_1420_0.Size = size;
			this.Picture1.TabIndex = 32;
			this.Picture1.TabStop = false;
			this.ToolTip1.SetToolTip(this.Picture1, "Image size authorized width:100 Height:100");
			this.Label8.AutoSize = true;
			this.Label8.Font = new Font("Trebuchet MS", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Label8.ForeColor = Color.Red;
			Control arg_14A5_0 = this.Label8;
			location = new Point(933, 437);
			arg_14A5_0.Location = location;
			this.Label8.Name = "Label8";
			Control arg_14CC_0 = this.Label8;
			size = new Size(52, 20);
			arg_14CC_0.Size = size;
			this.Label8.TabIndex = 23;
			this.Label8.Text = "Label8";
			this.ToolTip1.SetToolTip(this.Label8, "علامات کے مطابق حاصل ھونے والے نتیجہ میں ادویات کی کل تعداد");
			this.Button6.BackColor = Color.Tan;
			this.Button6.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_1546_0 = this.Button6;
			location = new Point(524, 28);
			arg_1546_0.Location = location;
			this.Button6.Name = "Button6";
			Control arg_156D_0 = this.Button6;
			size = new Size(68, 60);
			arg_156D_0.Size = size;
			this.Button6.TabIndex = 24;
			this.Button6.Text = "Excel Report";
			this.ToolTip1.SetToolTip(this.Button6, "Export Results To Excel Sheet");
			this.Button6.UseVisualStyleBackColor = false;
			this.search3urdu.Font = new Font("Nafees Nastaleeq", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			Control arg_15E0_0 = this.search3urdu;
			location = new Point(13, 30);
			arg_15E0_0.Location = location;
			this.search3urdu.Name = "search3urdu";
			this.search3urdu.RightToLeft = RightToLeft.Yes;
			Control arg_1613_0 = this.search3urdu;
			size = new Size(100, 44);
			arg_1613_0.Size = size;
			this.search3urdu.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.search3urdu, "تیسرا اردو لفظ");
			this.search2urdu.Font = new Font("Nafees Nastaleeq", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			Control arg_1669_0 = this.search2urdu;
			location = new Point(117, 30);
			arg_1669_0.Location = location;
			this.search2urdu.Name = "search2urdu";
			this.search2urdu.RightToLeft = RightToLeft.Yes;
			Control arg_169C_0 = this.search2urdu;
			size = new Size(101, 44);
			arg_169C_0.Size = size;
			this.search2urdu.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.search2urdu, "دوسرا اردو لفظ");
			this.search11urdu.Font = new Font("Nafees Nastaleeq", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			Control arg_16F5_0 = this.search11urdu;
			location = new Point(222, 29);
			arg_16F5_0.Location = location;
			this.search11urdu.Name = "search11urdu";
			this.search11urdu.RightToLeft = RightToLeft.Yes;
			Control arg_1728_0 = this.search11urdu;
			size = new Size(100, 44);
			arg_1728_0.Size = size;
			this.search11urdu.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.search11urdu, "اردو  لفظ");
			Control arg_1763_0 = this.search3;
			location = new Point(222, 7);
			arg_1763_0.Location = location;
			this.search3.Name = "search3";
			Control arg_178A_0 = this.search3;
			size = new Size(100, 21);
			arg_178A_0.Size = size;
			this.search3.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.search3, "Third Search Word Type Here");
			Control arg_17C2_0 = this.search2;
			location = new Point(118, 8);
			arg_17C2_0.Location = location;
			this.search2.Name = "search2";
			Control arg_17E9_0 = this.search2;
			size = new Size(99, 21);
			arg_17E9_0.Size = size;
			this.search2.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.search2, "Second Search Word Type Here");
			this.search11.AutoCompleteSource = AutoCompleteSource.CustomSource;
			Control arg_182E_0 = this.search11;
			location = new Point(13, 8);
			arg_182E_0.Location = location;
			this.search11.Name = "search11";
			Control arg_1855_0 = this.search11;
			size = new Size(102, 21);
			arg_1855_0.Size = size;
			this.search11.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.search11, "First Search Word Type Here");
			this.Button9.BackColor = Color.Tan;
			this.Button9.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_18BE_0 = this.Button9;
			location = new Point(457, 28);
			arg_18BE_0.Location = location;
			this.Button9.Name = "Button9";
			Control arg_18E5_0 = this.Button9;
			size = new Size(67, 60);
			arg_18E5_0.Size = size;
			this.Button9.TabIndex = 35;
			this.Button9.Text = "Old Patients";
			this.ToolTip1.SetToolTip(this.Button9, "Already Existing Patients ");
			this.Button9.UseVisualStyleBackColor = false;
			this.Button8.BackColor = Color.Tan;
			this.Button8.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_196B_0 = this.Button8;
			location = new Point(591, 28);
			arg_196B_0.Location = location;
			this.Button8.Name = "Button8";
			Control arg_1992_0 = this.Button8;
			size = new Size(69, 60);
			arg_1992_0.Size = size;
			this.Button8.TabIndex = 34;
			this.Button8.Text = "Doctor Detail";
			this.ToolTip1.SetToolTip(this.Button8, "Add Doctor Detail");
			this.Button8.UseVisualStyleBackColor = false;
			this.Label9.AutoSize = true;
			this.Label9.Font = new Font("Trebuchet MS", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label9.ForeColor = Color.Maroon;
			Control arg_1A24_0 = this.Label9;
			location = new Point(330, 104);
			arg_1A24_0.Location = location;
			this.Label9.Name = "Label9";
			Control arg_1A4E_0 = this.Label9;
			size = new Size(174, 18);
			arg_1A4E_0.Size = size;
			this.Label9.TabIndex = 25;
			this.Label9.Text = "Boenninghausen Repertory";
			this.ToolTip1.SetToolTip(this.Label9, "Name of current loaded Homeopathic Repertory");
			this.lst_search_symptoms1.BackColor = Color.GhostWhite;
			this.lst_search_symptoms1.Font = new Font("Nafees Nastaleeq", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lst_search_symptoms1.FormattingEnabled = true;
			this.lst_search_symptoms1.HorizontalScrollbar = true;
			this.lst_search_symptoms1.ItemHeight = 31;
			Control arg_1AEA_0 = this.lst_search_symptoms1;
			location = new Point(12, 75);
			arg_1AEA_0.Location = location;
			this.lst_search_symptoms1.Name = "lst_search_symptoms1";
			this.lst_search_symptoms1.RightToLeft = RightToLeft.Yes;
			Control arg_1B23_0 = this.lst_search_symptoms1;
			size = new Size(977, 221);
			arg_1B23_0.Size = size;
			this.lst_search_symptoms1.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.lst_search_symptoms1, "یہ علامات آپ کی جانب سے ٹایپ کردہ الفاظ کو تلاش کرنے کے نتیجہ میں ریپرٹری سے ملی ھیں۔");
			this.lst_search_symptoms1.Visible = false;
			this.Label3.BackColor = Color.GhostWhite;
			this.Label3.BorderStyle = BorderStyle.FixedSingle;
			this.Label3.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label3.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_1BBC_0 = this.Label3;
			location = new Point(754, 13);
			arg_1BBC_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_1BE6_0 = this.Label3;
			size = new Size(218, 23);
			arg_1BE6_0.Size = size;
			this.Label3.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.Label3, "Total symptoms found in currently loaded repertory against your search.");
			this.lst_symptoms1.BackColor = SystemColors.Info;
			this.lst_symptoms1.Font = new Font("Nafees Nastaleeq", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lst_symptoms1.FormattingEnabled = true;
			this.lst_symptoms1.ItemHeight = 31;
			Control arg_1C69_0 = this.lst_symptoms1;
			location = new Point(178, 39);
			arg_1C69_0.Location = location;
			this.lst_symptoms1.Name = "lst_symptoms1";
			this.lst_symptoms1.RightToLeft = RightToLeft.Yes;
			Control arg_1CA2_0 = this.lst_symptoms1;
			size = new Size(797, 252);
			arg_1CA2_0.Size = size;
			this.lst_symptoms1.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.lst_symptoms1, "اردو میں حاصل شدہ علامات جو کہ سامنے سے کسی بھی عنوان کو کلک کرنے کے نتیجہ میں ملی ھیں۔");
			this.lst_symptoms1.Visible = false;
			this.R2.AutoSize = true;
			Control arg_1CF3_0 = this.R2;
			location = new Point(95, 103);
			arg_1CF3_0.Location = location;
			this.R2.Name = "R2";
			Control arg_1D1A_0 = this.R2;
			size = new Size(53, 22);
			arg_1D1A_0.Size = size;
			this.R2.TabIndex = 31;
			this.R2.TabStop = true;
			this.R2.Text = "Urdu";
			this.ToolTip1.SetToolTip(this.R2, "اردو میں علامات دیکھنے کے لیے پہلے یہاں کلک کریں۔");
			this.R2.UseVisualStyleBackColor = true;
			this.R1.AutoSize = true;
			this.R1.Checked = true;
			Control arg_1D94_0 = this.R1;
			location = new Point(26, 103);
			arg_1D94_0.Location = location;
			this.R1.Name = "R1";
			Control arg_1DBB_0 = this.R1;
			size = new Size(63, 22);
			arg_1DBB_0.Size = size;
			this.R1.TabIndex = 30;
			this.R1.TabStop = true;
			this.R1.Text = "English";
			this.ToolTip1.SetToolTip(this.R1, "To view symptoms in English ,please click here .");
			this.R1.UseVisualStyleBackColor = true;
			this.lst_chapter.ItemHeight = 18;
			Control arg_1E29_0 = this.lst_chapter;
			location = new Point(6, 19);
			arg_1E29_0.Location = location;
			this.lst_chapter.Name = "lst_chapter";
			Control arg_1E56_0 = this.lst_chapter;
			size = new Size(153, 274);
			arg_1E56_0.Size = size;
			this.lst_chapter.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lst_chapter, "Chapters from selected repertory");
			this.CheckBox1.AutoSize = true;
			this.CheckBox1.BackColor = Color.PeachPuff;
			this.CheckBox1.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CheckBox1.ForeColor = Color.FromArgb(192, 64, 0);
			Control arg_1EE3_0 = this.CheckBox1;
			location = new Point(156, 107);
			arg_1EE3_0.Location = location;
			this.CheckBox1.Name = "CheckBox1";
			Control arg_1F0A_0 = this.CheckBox1;
			size = new Size(98, 22);
			arg_1F0A_0.Size = size;
			this.CheckBox1.TabIndex = 36;
			this.CheckBox1.Text = "Disable Help";
			this.ToolTip1.SetToolTip(this.CheckBox1, "Click here to disable help text to avoid distrubing you.");
			this.CheckBox1.UseVisualStyleBackColor = false;
			this.Button5.BackColor = Color.Tan;
			this.Button5.Enabled = false;
			this.Button5.Font = new Font("Trebuchet MS", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_1F9C_0 = this.Button5;
			location = new Point(707, 25);
			arg_1F9C_0.Location = location;
			this.Button5.Name = "Button5";
			Control arg_1FC3_0 = this.Button5;
			size = new Size(49, 60);
			arg_1FC3_0.Size = size;
			this.Button5.TabIndex = 22;
			this.Button5.Text = "Report";
			this.Button5.UseVisualStyleBackColor = false;
			this.Button5.Visible = false;
			this.Label10.AutoSize = true;
			Control arg_201E_0 = this.Label10;
			location = new Point(915, 25);
			arg_201E_0.Location = location;
			this.Label10.Name = "Label10";
			Control arg_2045_0 = this.Label10;
			size = new Size(49, 18);
			arg_2045_0.Size = size;
			this.Label10.TabIndex = 26;
			this.Label10.Text = "Label10";
			this.Label10.Visible = false;
			this.repertory.BackColor = Color.Thistle;
			this.repertory.Controls.Add(this.search3urdu);
			this.repertory.Controls.Add(this.search2urdu);
			this.repertory.Controls.Add(this.search11urdu);
			this.repertory.Controls.Add(this.Label13);
			this.repertory.Controls.Add(this.Button1);
			this.repertory.Controls.Add(this.search3);
			this.repertory.Controls.Add(this.search2);
			this.repertory.Controls.Add(this.search11);
			this.repertory.Controls.Add(this.lst_search_symptoms1);
			this.repertory.Controls.Add(this.lst_search_symptoms);
			Control arg_2174_0 = this.repertory;
			location = new Point(12, 134);
			arg_2174_0.Location = location;
			this.repertory.Name = "repertory";
			Control arg_21A1_0 = this.repertory;
			size = new Size(995, 300);
			arg_21A1_0.Size = size;
			this.repertory.TabIndex = 7;
			this.repertory.TabStop = false;
			this.repertory.Visible = false;
			this.Label13.AutoSize = true;
			Control arg_21EB_0 = this.Label13;
			location = new Point(396, 19);
			arg_21EB_0.Location = location;
			this.Label13.Name = "Label13";
			Control arg_2211_0 = this.Label13;
			size = new Size(0, 18);
			arg_2211_0.Size = size;
			this.Label13.TabIndex = 8;
			this.Button1.BackColor = Color.Transparent;
			this.Button1.Font = new Font("Trebuchet MS", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button1.Image = (Image)componentResourceManager.GetObject("Button1.Image");
			Control arg_227F_0 = this.Button1;
			location = new Point(337, 37);
			arg_227F_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_22A6_0 = this.Button1;
			size = new Size(65, 36);
			arg_22A6_0.Size = size;
			this.Button1.TabIndex = 3;
			this.Button1.Text = "search  :::  تلاش کریں";
			this.Button1.UseVisualStyleBackColor = false;
			this.lst_search_symptoms.ItemHeight = 18;
			Control arg_22F2_0 = this.lst_search_symptoms;
			location = new Point(12, 75);
			arg_22F2_0.Location = location;
			this.lst_search_symptoms.Name = "lst_search_symptoms";
			Control arg_231F_0 = this.lst_search_symptoms;
			size = new Size(960, 220);
			arg_231F_0.Size = size;
			this.lst_search_symptoms.TabIndex = 4;
			this.chapter.BackColor = Color.LightSteelBlue;
			this.chapter.Controls.Add(this.lst_symptoms1);
			this.chapter.Controls.Add(this.lst_symptoms);
			this.chapter.Controls.Add(this.lst_chapter);
			this.chapter.Controls.Add(this.Label3);
			Control arg_23AD_0 = this.chapter;
			location = new Point(12, 134);
			arg_23AD_0.Location = location;
			this.chapter.Name = "chapter";
			Control arg_23DA_0 = this.chapter;
			size = new Size(995, 300);
			arg_23DA_0.Size = size;
			this.chapter.TabIndex = 0;
			this.chapter.TabStop = false;
			this.lst_symptoms.ItemHeight = 18;
			Control arg_2419_0 = this.lst_symptoms;
			location = new Point(176, 39);
			arg_2419_0.Location = location;
			this.lst_symptoms.Name = "lst_symptoms";
			Control arg_2446_0 = this.lst_symptoms;
			size = new Size(799, 256);
			arg_2446_0.Size = size;
			this.lst_symptoms.TabIndex = 1;
			this.Label11.AutoSize = true;
			this.Label11.Font = new Font("Trebuchet MS", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label11.ForeColor = Color.DarkRed;
			Control arg_24A5_0 = this.Label11;
			location = new Point(726, 48);
			arg_24A5_0.Location = location;
			this.Label11.Name = "Label11";
			Control arg_24CC_0 = this.Label11;
			size = new Size(57, 18);
			arg_24CC_0.Size = size;
			this.Label11.TabIndex = 27;
			this.Label11.Text = "Label11";
			this.Label12.AutoSize = true;
			this.Label12.Font = new Font("Trebuchet MS", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label12.ForeColor = Color.DarkRed;
			Control arg_253C_0 = this.Label12;
			location = new Point(726, 28);
			arg_253C_0.Location = location;
			this.Label12.Name = "Label12";
			Control arg_2563_0 = this.Label12;
			size = new Size(57, 18);
			arg_2563_0.Size = size;
			this.Label12.TabIndex = 28;
			this.Label12.Text = "Label12";
			this.Timer1.Interval = 1000;
			this.Label6.AutoSize = true;
			Control arg_25B6_0 = this.Label6;
			location = new Point(833, 25);
			arg_25B6_0.Location = location;
			this.Label6.Name = "Label6";
			Control arg_25DD_0 = this.Label6;
			size = new Size(43, 18);
			arg_25DD_0.Size = size;
			this.Label6.TabIndex = 29;
			this.Label6.Text = "Label6";
			this.Label6.Visible = false;
			size = new Size(6, 14);
			this.AutoScaleBaseSize = size;
			this.BackColor = Color.AntiqueWhite;
			size = new Size(1028, 746);
			this.ClientSize = size;
			this.Controls.Add(this.CheckBox1);
			this.Controls.Add(this.Button9);
			this.Controls.Add(this.Button8);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Picture1);
			this.Controls.Add(this.R2);
			this.Controls.Add(this.R1);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label10);
			this.Controls.Add(this.Label9);
			this.Controls.Add(this.Button6);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Button5);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Button4);
			this.Controls.Add(this.Button3);
			this.Controls.Add(this.ToolStrip1);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Materia);
			this.Controls.Add(this.cmd_result);
			this.Controls.Add(this.result_grid);
			this.Controls.Add(this.GroupBox1);
			this.Controls.Add(this.cmd_exit);
			this.Controls.Add(this.cmd_chapter);
			this.Controls.Add(this.cmd_rep);
			this.Controls.Add(this.Label11);
			this.Controls.Add(this.Label12);
			this.Controls.Add(this.repertory);
			this.Controls.Add(this.chapter);
			this.Font = new Font("Trebuchet MS", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_Main";
			this.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "PHR 2008 ::: Pak Homeopathic Repertory Ver 1.0 Boenninghausen Edition English :: Urdu   Build April-2008";
			this.GroupBox1.ResumeLayout(false);
			((ISupportInitialize)this.result_grid).EndInit();
			this.ToolStrip1.ResumeLayout(false);
			this.ToolStrip1.PerformLayout();
			((ISupportInitialize)this.Picture1).EndInit();
			this.repertory.ResumeLayout(false);
			this.repertory.PerformLayout();
			this.chapter.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void frm_Main_Load(object sender, EventArgs e)
		{
			/*try
			{
				FileStream fileStream = new FileStream("" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\xyz.txt", FileMode.Open, FileAccess.Read);
				fileStream.Close();
			}
			catch (Exception arg_15_0)
			{
				ProjectData.SetProjectError(arg_15_0);
				Interaction.MsgBox("Error Code: 0001-Pak-2008: Repertory will close now,Kindly obtain valid file from Homeopathic Dr. Mumtaz Ali Riaz  drmumtaz@urduhomeopath.com  Mobile Phone: 0321-7701904", MsgBoxStyle.OkOnly, null);
				this.Close();
				ProjectData.ClearProjectError();
			}
			try
			{
				FileStream fileStream2 = new FileStream("" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\def.txt", FileMode.Open, FileAccess.Read);
				fileStream2.Close();
			}
			catch (Exception arg_4B_0)
			{
				ProjectData.SetProjectError(arg_4B_0);
				Interaction.MsgBox("Error Code: 0002-Pak-2008: Repertory will close now,Kindly obtain valid file from Homeopathic Dr. Mumtaz Ali Riaz  drmumtaz@urduhomeopath.com  Mobile Phone: 0321-7701904", MsgBoxStyle.OkOnly, null);
				this.Close();
				ProjectData.ClearProjectError();
			}*/
			TreeNode treeNode = new TreeNode();
			TreeNode treeNode2 = new TreeNode();
			TreeNode treeNode3 = new TreeNode();
			this.Label8.Text = "";
			this.chapter.Visible = false;
			this.repertory.Visible = false;
			this.GroupBox1.Visible = false;
			this.Timer1.Enabled = true;
			this.search11urdu.Visible = false;
			this.search2urdu.Visible = false;
			this.search3urdu.Visible = false;
			this.result_grid.set_ColWidth(0, 2200);
			this.result_grid.set_ColWidth(1, 930);
			this.result_grid.set_ColWidth(2, 910);
			this.result_grid.set_ColWidth(3, 0);
			this.DBPath = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.Connect_Database();
			_Connection arg_146_0 = this.con;
			string arg_146_1 = "delete * from cal_result";
			object value = Missing.Value;
			arg_146_0.Execute(arg_146_1, out value, -1);
			_Connection arg_162_0 = this.con;
			string arg_162_1 = "select pic from doctor where id=1";
			value = Missing.Value;
			this.rst = arg_162_0.Execute(arg_162_1, out value, -1);
		}

		public void open_patient()
		{
			this.lst_symptom.Visible = true;
			this.GroupBox1.Visible = true;
			this.ListBox1.Items.Clear();
			_Connection arg_3C_0 = this.con;
			string arg_3C_1 = "delete * from cal_result";
			object value = Missing.Value;
			arg_3C_0.Execute(arg_3C_1, out value, -1);
			_Connection arg_57_0 = this.con;
			string arg_57_1 = "select * from doctor";
			value = Missing.Value;
			this.rst = arg_57_0.Execute(arg_57_1, out value, -1);
			if (this.rst.EOF & !this.rst.BOF)
			{
				int num = Conversions.ToInteger(this.rst.Fields["d_name"].Value);
				Interaction.MsgBox("Sorry due to error cannot continue", MsgBoxStyle.OkOnly, null);
				if (num == 4)
				{
					string text = Conversions.ToString(this.rst.Fields["phone"].Value);
					string text2 = Conversions.ToString(this.rst.Fields["r_address"].Value);
					_Connection arg_14B_0 = this.con;
					string arg_14B_1 = string.Concat(new string[]
					{
						"select * from Patient_Symptoms where Patient_id=",
						Conversions.ToString(num),
						" and date1='",
						text,
						"' and  time1='",
						text2,
						"'"
					});
					value = Missing.Value;
					this.rst = arg_14B_0.Execute(arg_14B_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_19D_0 = this.con;
						string arg_19D_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.SymName='", this.rst.Fields["symname"].Value), "'"));
						value = Missing.Value;
						this.rst1 = arg_19D_0.Execute(arg_19D_1, out value, -1);
						while (!this.rst1.EOF & !this.rst1.BOF)
						{
							_Connection arg_292_0 = this.con;
							string arg_292_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst1.Fields["RemName"].Value), "',"), this.rst1.Fields["Weight"].Value), ","), this.rst1.Fields["remid"].Value), ","), this.rst1.Fields["symid"].Value), ",'"), this.rst1.Fields["symname"].Value), "')"));
							value = Missing.Value;
							arg_292_0.Execute(arg_292_1, out value, -1);
							this.rst1.MoveNext();
						}
						this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["symname"].Value));
						this.rst.MoveNext();
					}
					_Connection arg_336_0 = this.con;
					string arg_336_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_336_0.Execute(arg_336_1, out value, -1);
				}
			}
		}

		private void lst_chapter_SelectedIndexChanged(object sender, EventArgs e)
		{
			int num = 0;
			Cursor.Current = Cursors.WaitCursor;
			this.lst_symptoms.Items.Clear();
			this.lst_symptoms1.Items.Clear();
			_Connection arg_60_0 = this.con;
			string arg_60_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select Catid from categories where catname ='", this.lst_chapter.SelectedItem), "'"));
			object value = Missing.Value;
			this.rst = arg_60_0.Execute(arg_60_1, out value, -1);
			short value2 = Conversions.ToShort(this.rst.Fields["catid"].Value);
			_Connection arg_AF_0 = this.con;
			string arg_AF_1 = "SELECT * FROM Symptoms INNER JOIN SymCategory ON Symptoms.[SymId] = SymCategory.[SymId] where SymCategory.CatID = " + Conversions.ToString((int)value2) + " ";
			value = Missing.Value;
			this.rst = arg_AF_0.Execute(arg_AF_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					this.lst_symptoms.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["SymName"].Value));
					this.lst_symptoms1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["Symurdu"].Value));
					this.rst.MoveNext();
					num++;
				}
				Cursor.Current = Cursors.Arrow;
				this.Label3.Text = "Total Symptoms: " + Conversions.ToString(num);
			}
		}

		public void sym_load()
		{
			byte b = 0;
			int arg_16_0 = 0;
			checked
			{
				int num = this.ListBox1.Items.Count - 1;
				for (int i = arg_16_0; i <= num; i++)
				{
					if (Operators.ConditionalCompareObjectEqual(this.ListBox1.Items[i], this.lst_symptoms.SelectedItem, false))
					{
						b = 1;
					}
				}
				if (b == 0)
				{
					_Connection arg_92_0 = this.con;
					string arg_92_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symurdu,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.Symname='", this.lst_symptoms.Items[this.lst_symptoms.SelectedIndex]), "'"));
					object value = Missing.Value;
					this.rst = arg_92_0.Execute(arg_92_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_187_0 = this.con;
						string arg_187_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst.Fields["RemName"].Value), "',"), this.rst.Fields["Weight"].Value), ","), this.rst.Fields["remid"].Value), ","), this.rst.Fields["symid"].Value), ",'"), this.rst.Fields["symurdu"].Value), "')"));
						value = Missing.Value;
						arg_187_0.Execute(arg_187_1, out value, -1);
						this.rst.MoveNext();
					}
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_symptoms.SelectedItem));
					_Connection arg_1EF_0 = this.con;
					string arg_1EF_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_1EF_0.Execute(arg_1EF_1, out value, -1);
				}
			}
		}

		private void cmd_chapter_Click(object sender, EventArgs e)
		{
			this.chapter.Visible = true;
			this.repertory.Visible = false;
			this.GroupBox1.Visible = true;
		}

		private void cmd_tree_Click(object sender, EventArgs e)
		{
			this.chapter.Visible = false;
			this.repertory.Visible = false;
		}

		private void cmd_exit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			int num = 0;
			Cursor.Current = Cursors.WaitCursor;
			this.lst_search_symptoms.Items.Clear();
			this.lst_search_symptoms1.Items.Clear();
			checked
			{
				if (this.R1.Checked)
				{
					if (Operators.CompareString(this.search11.Text, "", false) == 0 & Operators.CompareString(this.search2.Text, "", false) == 0 & Operators.CompareString(this.search3.Text, "", false) == 0)
					{
						return;
					}
					if (Operators.CompareString(this.search11.Text, "", false) != 0 & Operators.CompareString(this.search2.Text, "", false) != 0 & Operators.CompareString(this.search3.Text, "", false) != 0)
					{
						_Connection arg_14B_0 = this.con;
						string arg_14B_1 = string.Concat(new string[]
						{
							"select Symname,symurdu from Symptoms where SymName like '%",
							this.search11.Text,
							"%' and SymName like '%",
							this.search2.Text,
							"%' and SymName like '%",
							this.search3.Text,
							"%' "
						});
						object value = Missing.Value;
						this.rst = arg_14B_0.Execute(arg_14B_1, out value, -1);
					}
					else if (Operators.CompareString(this.search11.Text, "", false) != 0 & Operators.CompareString(this.search2.Text, "", false) != 0)
					{
						_Connection arg_1E6_0 = this.con;
						string arg_1E6_1 = string.Concat(new string[]
						{
							"select SymName,symurdu  from Symptoms where SymName like '%",
							this.search11.Text,
							"%' and SymName like '%",
							this.search2.Text,
							"%' "
						});
						object value = Missing.Value;
						this.rst = arg_1E6_0.Execute(arg_1E6_1, out value, -1);
					}
					else
					{
						_Connection arg_21F_0 = this.con;
						string arg_21F_1 = "select SymName,symurdu from Symptoms where SymName like '%" + this.search11.Text + "%'";
						object value = Missing.Value;
						this.rst = arg_21F_0.Execute(arg_21F_1, out value, -1);
					}
					while (!this.rst.EOF & !this.rst.BOF)
					{
						this.lst_search_symptoms.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["SymName"].Value));
						this.lst_search_symptoms1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["Symurdu"].Value));
						num++;
						this.rst.MoveNext();
					}
				}
				if (this.R2.Checked)
				{
					if (Operators.CompareString(this.search11urdu.Text, "", false) == 0 & Operators.CompareString(this.search2urdu.Text, "", false) == 0 & Operators.CompareString(this.search3urdu.Text, "", false) == 0)
					{
						return;
					}
					if (Operators.CompareString(this.search11urdu.Text, "", false) != 0 & Operators.CompareString(this.search2urdu.Text, "", false) != 0 & Operators.CompareString(this.search3urdu.Text, "", false) != 0)
					{
						_Connection arg_3DB_0 = this.con;
						string arg_3DB_1 = string.Concat(new string[]
						{
							"select Symname,symurdu from Symptoms where Symurdu like '%",
							this.search11urdu.Text,
							"%' and Symurdu like '%",
							this.search2urdu.Text,
							"%' and Symurdu like '%",
							this.search3urdu.Text,
							"%' "
						});
						object value = Missing.Value;
						this.rst = arg_3DB_0.Execute(arg_3DB_1, out value, -1);
					}
					else if (Operators.CompareString(this.search11urdu.Text, "", false) != 0 & Operators.CompareString(this.search2urdu.Text, "", false) != 0)
					{
						_Connection arg_476_0 = this.con;
						string arg_476_1 = string.Concat(new string[]
						{
							"select SymName,symurdu  from Symptoms where Symurdu like '%",
							this.search11urdu.Text,
							"%' and Symurdu like '%",
							this.search2urdu.Text,
							"%' "
						});
						object value = Missing.Value;
						this.rst = arg_476_0.Execute(arg_476_1, out value, -1);
					}
					else
					{
						_Connection arg_4AF_0 = this.con;
						string arg_4AF_1 = "select SymName,symurdu from Symptoms where Symurdu like '%" + this.search11urdu.Text + "%'";
						object value = Missing.Value;
						this.rst = arg_4AF_0.Execute(arg_4AF_1, out value, -1);
					}
					while (!this.rst.EOF & !this.rst.BOF)
					{
						this.lst_search_symptoms.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["SymName"].Value));
						this.lst_search_symptoms1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["Symurdu"].Value));
						num++;
						this.rst.MoveNext();
					}
				}
				Cursor.Current = Cursors.Arrow;
				this.Label13.Text = "Total Symptoms: " + Conversions.ToString(num);
			}
		}

		private void cmd_rep_Click(object sender, EventArgs e)
		{
			this.repertory.Visible = true;
			this.chapter.Visible = false;
			this.GroupBox1.Visible = true;
		}

		private void lst_search_symptoms_SelectedIndexChanged(object sender, EventArgs e)
		{
			byte b = 0;
			int arg_16_0 = 0;
			checked
			{
				int num = this.ListBox1.Items.Count - 1;
				for (int i = arg_16_0; i <= num; i++)
				{
					if (Operators.ConditionalCompareObjectEqual(this.ListBox1.Items[i], this.lst_search_symptoms.SelectedItem, false))
					{
						b = 1;
					}
				}
				if (b == 0)
				{
					_Connection arg_82_0 = this.con;
					string arg_82_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.SymName='", this.lst_search_symptoms.SelectedItem), "'"));
					object value = Missing.Value;
					this.rst = arg_82_0.Execute(arg_82_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_177_0 = this.con;
						string arg_177_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst.Fields["RemName"].Value), "',"), this.rst.Fields["Weight"].Value), ","), this.rst.Fields["remid"].Value), ","), this.rst.Fields["symid"].Value), ",'"), this.rst.Fields["symname"].Value), "')"));
						value = Missing.Value;
						arg_177_0.Execute(arg_177_1, out value, -1);
						this.rst.MoveNext();
					}
					_Connection arg_1BE_0 = this.con;
					string arg_1BE_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_1BE_0.Execute(arg_1BE_1, out value, -1);
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_search_symptoms.SelectedItem));
					this.ListBox2.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_search_symptoms1.Items[this.lst_search_symptoms.SelectedIndex]));
				}
			}
		}

		private void lst_symptoms_SelectedIndexChanged(object sender, EventArgs e)
		{
			byte b = 0;
			int arg_16_0 = 0;
			checked
			{
				int num = this.ListBox1.Items.Count - 1;
				for (int i = arg_16_0; i <= num; i++)
				{
					if (Operators.ConditionalCompareObjectEqual(this.ListBox1.Items[i], this.lst_symptoms.SelectedItem, false))
					{
						b = 1;
					}
				}
				if (b == 0)
				{
					_Connection arg_82_0 = this.con;
					string arg_82_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symurdu,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.Symname='", this.lst_symptoms.SelectedItem), "'"));
					object value = Missing.Value;
					this.rst = arg_82_0.Execute(arg_82_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_177_0 = this.con;
						string arg_177_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst.Fields["RemName"].Value), "',"), this.rst.Fields["Weight"].Value), ","), this.rst.Fields["remid"].Value), ","), this.rst.Fields["symid"].Value), ",'"), this.rst.Fields["symname"].Value), "')"));
						value = Missing.Value;
						arg_177_0.Execute(arg_177_1, out value, -1);
						this.rst.MoveNext();
					}
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_symptoms.SelectedItem));
					this.ListBox2.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_symptoms1.Items[this.lst_symptoms.SelectedIndex]));
					_Connection arg_210_0 = this.con;
					string arg_210_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_210_0.Execute(arg_210_1, out value, -1);
				}
			}
		}

		private void cmd_result_Click(object sender, EventArgs e)
		{
			this.result_grid.Clear();
			Recordset recordset = new RecordsetClass();
			double num = 2.0;
			Cursor.Current = Cursors.WaitCursor;
			this.result_grid.Row = 0;
			this.result_grid.Col = 0;
			this.result_grid.Text = "Remedy";
			this.result_grid.Col = 1;
			this.result_grid.Text = "Grade";
			this.result_grid.Col = 2;
			this.result_grid.Text = "Percent";
			_Connection arg_9B_0 = this.con;
			string arg_9B_1 = "select rem,remid from cal_result where parity=0";
			object value = Missing.Value;
			this.rst = arg_9B_0.Execute(arg_9B_1, out value, -1);
			int num2;
			while (!this.rst.EOF & !this.rst.BOF)
			{
				checked
				{
					this.result_grid.Rows = (int)Math.Round(num);
					this.result_grid.Row = (int)Math.Round(unchecked(num - 1.0));
					this.result_grid.Col = 0;
					this.result_grid.Text = Conversions.ToString(this.rst.Fields["rem"].Value);
					num2 = 0;
					_Connection arg_153_0 = this.con;
					string arg_153_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select weight from cal_result where remid=", this.rst.Fields["remid"].Value), ""));
					value = Missing.Value;
					recordset = arg_153_0.Execute(arg_153_1, out value, -1);
					while (!recordset.EOF & !recordset.BOF)
					{
						num2 = Conversions.ToInteger(Operators.AddObject(num2, recordset.Fields["weight"].Value));
						_Connection arg_1C4_0 = this.con;
						string arg_1C4_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("update cal_result set parity=1 where remid=", this.rst.Fields["remid"].Value), ""));
						value = Missing.Value;
						arg_1C4_0.Execute(arg_1C4_1, out value, -1);
						recordset.MoveNext();
					}
					this.result_grid.Row = (int)Math.Round(unchecked(num - 1.0));
					this.result_grid.Col = 1;
					this.result_grid.Text = Conversions.ToString(num2);
					this.result_grid.Col = 3;
					this.result_grid.Text = Conversions.ToString(this.rst.Fields["remid"].Value);
					this.rst.MoveNext();
				}
				num += 1.0;
			}
			this.result_grid.Col = 1;
			this.result_grid.Sort = 2;
			this.result_grid.Row = 1;
			this.result_grid.Col = 1;
			num2 = checked((int)Math.Round(Conversion.Val(this.result_grid.Text)));
			this.Label8.Text = Conversions.ToString(num - 2.0);
			if (num > 2.0)
			{
				double arg_31A_0 = 1.0;
				double num3 = (double)(checked(this.result_grid.Rows - 1));
				for (num = arg_31A_0; num <= num3; num += 1.0)
				{
					checked
					{
						this.result_grid.Row = (int)Math.Round(num);
						this.result_grid.Col = 1;
						int num4 = (int)Math.Round(unchecked(Conversions.ToDouble(this.result_grid.Text) * 100.0) / (double)num2);
						this.result_grid.Col = 2;
						this.result_grid.Text = Conversions.ToString(num4) + "%";
						if (num4 == 100)
						{
							this.result_grid.Col = 0;
							this.result_grid.CellBackColor = Color.LightSkyBlue;
							this.result_grid.Col = 1;
							this.result_grid.CellBackColor = Color.LightSkyBlue;
							this.result_grid.Col = 2;
							this.result_grid.CellBackColor = Color.LightSkyBlue;
						}
					}
				}
			}
			Cursor.Current = Cursors.Arrow;
		}

		private void Materia_Click(object sender, EventArgs e)
		{
			frm_Materia frm_Materia = new frm_Materia();
			frm_Materia.Show();
		}

		private void lst_symptom_AfterSelect(object sender, TreeViewEventArgs e)
		{
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("NOTICE: Families-Remedies grouping tree is based on various sources in history", MsgBoxStyle.OkOnly, null);
			frm_Family frm_Family = new frm_Family();
			frm_Family.Show();
		}

		private void Tool_Button1_Click(object sender, EventArgs e)
		{
			_Connection arg_14_0 = this.con;
			string arg_14_1 = "delete * from cal_result";
			object value = Missing.Value;
			arg_14_0.Execute(arg_14_1, out value, -1);
			frm_Patient frm_Patient = new frm_Patient();
			frm_Patient.Show();
			this.result_grid.Clear();
			this.lst_symptom.Nodes.Clear();
		}

		private void Button3_Click(object sender, EventArgs e)
		{
			frm_Patient frm_Patient = new frm_Patient();
			frm_Patient.Show();
		}

		private void ToolStripButton1_Click(object sender, EventArgs e)
		{
			int num;
			int num2;
            try
            {
                FontDialog fontDialog = this.FontDialog1;
                if (fontDialog.ShowDialog() == DialogResult.OK && this.FontDialog1.Font.Size <= 14f)
                {
                    ProjectData.ClearProjectError();
                    num = 2;
                }
            //IL_2E:
                //goto IL_73;
                //num2 = -1;
            //@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num);
            //IL_46:
                //goto IL_68;
            }
            catch (Exception ex) { }
			/*object arg_48_0;
			endfilter(arg_48_0 is Exception & num != 0 & num2 == 0);
			IL_68:
			throw ProjectData.CreateProjectError(-2146828237);
			IL_73:*/
			//if (num2 != 0)
			{
				ProjectData.ClearProjectError();
			}
		}

		private void ToolStripButton3_Click(object sender, EventArgs e)
		{
			string timeString = DateAndTime.TimeString;
			checked
			{
				if (Operators.CompareString(this.Label6.Text, "", false) != 0)
				{
					int arg_36_0 = 0;
					int num = this.ListBox1.Items.Count - 1;
					for (int i = arg_36_0; i <= num; i++)
					{
						_Connection arg_76_0 = this.con;
						string arg_76_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select * from symptoms where symname='", this.ListBox1.Items[i]), "'"));
						object value = Missing.Value;
						this.rst = arg_76_0.Execute(arg_76_1, out value, -1);
						_Connection arg_120_0 = this.con;
						string arg_120_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into Patient_Symptoms values(" + this.Label6.Text + ",'" + DateAndTime.DateString + "','", this.ListBox1.Items[i]), "',"), this.rst.Fields["symid"].Value), ",'"), timeString), "')"));
						value = Missing.Value;
						arg_120_0.Execute(arg_120_1, out value, -1);
					}
				}
			}
		}

		private void Button4_Click(object sender, EventArgs e)
		{
			frm_report1 frm_report = new frm_report1();
			frm_report.Show();
		}

		private void ToolStripButton4_Click(object sender, EventArgs e)
		{
			frm_doctor frm_doctor = new frm_doctor();
			frm_doctor.Show();
		}

		private void result_grid_ClickEvent(object sender, EventArgs e)
		{
			if (this.result_grid.Row == 1)
			{
				if (this.result_grid.Col == 0)
				{
					this.result_grid.Col = 0;
					this.result_grid.Sort = 1;
				}
				else
				{
					this.result_grid.Col = 1;
					this.result_grid.Sort = 2;
				}
			}
		}

		private void ToolStripButton5_Click(object sender, EventArgs e)
		{
			frm_adduser frm_adduser = new frm_adduser();
			frm_adduser.Show();
		}

		private void ToolStripButton2_Click(object sender, EventArgs e)
		{
			this.lst_symptom.Nodes.Clear();
			frm_patientopen frm_patientopen = new frm_patientopen();
			frm_patientopen.Show();
		}

		private void result_grid_MouseMoveEvent(object sender, DMSFlexGridEvents_MouseMoveEvent e)
		{
			AxMSFlexGrid result_grid = this.result_grid;
			this.result_grid.Col = 0;
			this.result_grid.Row = result_grid.MouseRow;
			this.ToolTip1.SetToolTip(this.result_grid, result_grid.Text);
		}

		private void Button5_Click(object sender, EventArgs e)
		{
			frm_patientreport frm_patientreport = new frm_patientreport();
			frm_patientreport.Show();
		}

		private void ListBox1_Click(object sender, EventArgs e)
		{
			_Connection arg_33_0 = this.con;
			string arg_33_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("delete from cal_result where symname ='", this.ListBox1.SelectedItem), "'"));
			object value = Missing.Value;
			arg_33_0.Execute(arg_33_1, out value, -1);
			this.ListBox2.Items.Remove(RuntimeHelpers.GetObjectValue(this.ListBox2.Items[this.ListBox1.SelectedIndex]));
			this.ListBox1.Items.Remove(RuntimeHelpers.GetObjectValue(this.ListBox1.SelectedItem));
			_Connection arg_9D_0 = this.con;
			string arg_9D_1 = "update cal_result set parity=0";
			value = Missing.Value;
			arg_9D_0.Execute(arg_9D_1, out value, -1);
		}

		private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
		{
			int num;
			int num2;
            try
            {
                ProjectData.ClearProjectError();
                num = 2;
                _Connection arg_4A_0 = this.con;
                string arg_4A_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("delete from cal_result where symname ='", this.ListBox1.Items[this.ListBox2.SelectedIndex]), "'"));
                object value = Missing.Value;
                arg_4A_0.Execute(arg_4A_1, out value, -1);
                this.ListBox1.Items.Remove(RuntimeHelpers.GetObjectValue(this.ListBox1.Items[this.ListBox2.SelectedIndex]));
                this.ListBox2.Items.Remove(RuntimeHelpers.GetObjectValue(this.ListBox2.SelectedItem));
                _Connection arg_B4_0 = this.con;
                string arg_B4_1 = "update cal_result set parity=0";
                value = Missing.Value;
                arg_B4_0.Execute(arg_B4_1, out value, -1);
            IL_BA:
                goto IL_FD;
                num2 = -1;
            //@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num);
            IL_D0:
                goto IL_F2;
            }
            catch (Exception ex) { }
			object arg_D2_0;
			//endfilter(arg_D2_0 is Exception & num != 0 & num2 == 0);
			IL_F2:
			//throw ProjectData.CreateProjectError(-2146828237);
			IL_FD:
			//if (num2 != 0)
			{
				ProjectData.ClearProjectError();
			}
		}

		private void load_book1_Click(object sender, EventArgs e)
		{
			if (this.open == 1)
			{
				this.con.Close();
				this.open = 0;
			}
			this.lst_chapter.Items.Clear();
			this.lst_symptoms.Items.Clear();
			this.DBPath = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.Connect_Database();
			this.Label9.Visible = true;
			this.Label9.Text = "Boenninghausen Loaded";
		}

		private void load_book2_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("This is just Boenninghausen Version,To Run Kent Repertory,Please Buy It", MsgBoxStyle.OkOnly, null);
		}

		private void Timer1_Tick(object sender, EventArgs e)
		{
			this.Label11.Text = DateAndTime.DateString;
			this.Label12.Text = DateAndTime.TimeString;
		}

		private void ToolStripDropDownButton1_Click(object sender, EventArgs e)
		{
		}

		private void Button6_Click(object sender, EventArgs e)
		{
			int num;
			int num2;
            try
            {
                ProjectData.ClearProjectError();
                num = 2;
                object objectValue = RuntimeHelpers.GetObjectValue(Interaction.CreateObject("Excel.Application", ""));
                object objectValue2 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(NewLateBinding.LateGet(objectValue, null, "Workbooks", new object[0], null, null, null), null, "Add", new object[0], null, null, null));
                object objectValue3 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(objectValue2, null, "Worksheets", new object[]
				{
					1
				}, null, null, null));
                NewLateBinding.LateCall(objectValue2, null, "SaveAs", new object[]
				{
					AppDomain.CurrentDomain.BaseDirectory + "Report.xls"
				}, null, null, null, true);
                NewLateBinding.LateCall(objectValue, null, "Quit", new object[0], null, null, null, true);
            IL_BF:
                goto IL_106;
                num2 = -1;
            //@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num);
            IL_D7:
                goto IL_FB;
            }
            catch (Exception ex) { }
			object arg_D9_0;
			//endfilter(arg_D9_0 is Exception & num != 0 & num2 == 0);
			IL_FB:
			//throw ProjectData.CreateProjectError(-2146828237);
			IL_106:
			//if (num2 != 0)
			{
				ProjectData.ClearProjectError();
			}
		}

		private void BoggarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("Sorry this book is not available in this edition", MsgBoxStyle.OkOnly, null);
		}

		private void CompleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("Sorry this book is not available in this edition", MsgBoxStyle.OkOnly, null);
		}

		private void Book5ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("Sorry this book is not available in this edition", MsgBoxStyle.OkOnly, null);
		}

		private void lst_symptoms1_SelectedIndexChanged(object sender, EventArgs e)
		{
			byte b = 0;
			int arg_16_0 = 0;
			checked
			{
				int num = this.ListBox1.Items.Count - 1;
				for (int i = arg_16_0; i <= num; i++)
				{
					if (Operators.ConditionalCompareObjectEqual(this.ListBox1.Items[i], this.lst_symptoms.Items[this.lst_symptoms1.SelectedIndex], false))
					{
						b = 1;
					}
				}
				if (b == 0)
				{
					_Connection arg_A2_0 = this.con;
					string arg_A2_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symurdu,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.Symname='", this.lst_symptoms.Items[this.lst_symptoms1.SelectedIndex]), "'"));
					object value = Missing.Value;
					this.rst = arg_A2_0.Execute(arg_A2_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_197_0 = this.con;
						string arg_197_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst.Fields["RemName"].Value), "',"), this.rst.Fields["Weight"].Value), ","), this.rst.Fields["remid"].Value), ","), this.rst.Fields["symid"].Value), ",'"), this.rst.Fields["symname"].Value), "')"));
						value = Missing.Value;
						arg_197_0.Execute(arg_197_1, out value, -1);
						this.rst.MoveNext();
					}
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_symptoms.Items[this.lst_symptoms1.SelectedIndex]));
					this.ListBox2.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_symptoms1.SelectedItem));
					_Connection arg_230_0 = this.con;
					string arg_230_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_230_0.Execute(arg_230_1, out value, -1);
				}
			}
		}

		private void R1_Click(object sender, EventArgs e)
		{
			if (this.R1.Checked)
			{
				this.lst_symptoms.Visible = true;
				this.lst_symptoms1.Visible = false;
				this.lst_search_symptoms.Visible = true;
				this.lst_search_symptoms1.Visible = false;
				this.search11.Visible = true;
				this.search2.Visible = true;
				this.search3.Visible = true;
				this.search11urdu.Visible = false;
				this.search2urdu.Visible = false;
				this.search3urdu.Visible = false;
			}
		}

		private void R2_Click(object sender, EventArgs e)
		{
			if (this.R2.Checked)
			{
				this.lst_symptoms1.Visible = true;
				this.lst_symptoms.Visible = false;
				this.lst_search_symptoms1.Visible = true;
				this.lst_search_symptoms.Visible = false;
				this.search11urdu.Visible = true;
				this.search2urdu.Visible = true;
				this.search3urdu.Visible = true;
				this.search11.Visible = false;
				this.search2.Visible = false;
				this.search3.Visible = false;
			}
		}

		private void lst_search_symptoms1_SelectedIndexChanged(object sender, EventArgs e)
		{
			byte b = 0;
			int arg_16_0 = 0;
			checked
			{
				int num = this.ListBox1.Items.Count - 1;
				for (int i = arg_16_0; i <= num; i++)
				{
					if (Operators.ConditionalCompareObjectEqual(this.ListBox1.Items[i], this.lst_search_symptoms1.SelectedItem, false))
					{
						b = 1;
					}
				}
				if (b == 0)
				{
					_Connection arg_92_0 = this.con;
					string arg_92_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("SELECT Symptoms.SymName,Symptoms.Symid, RemSymptoms.RemId, Remedies.RemName, RemSymptoms.Weight FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Symptoms.SymName='", this.lst_search_symptoms.Items[this.lst_search_symptoms1.SelectedIndex]), "'"));
					object value = Missing.Value;
					this.rst = arg_92_0.Execute(arg_92_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						_Connection arg_187_0 = this.con;
						string arg_187_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("insert into cal_result(rem,weight,remid,symid,symname) values('", this.rst.Fields["RemName"].Value), "',"), this.rst.Fields["Weight"].Value), ","), this.rst.Fields["remid"].Value), ","), this.rst.Fields["symid"].Value), ",'"), this.rst.Fields["symname"].Value), "')"));
						value = Missing.Value;
						arg_187_0.Execute(arg_187_1, out value, -1);
						this.rst.MoveNext();
					}
					_Connection arg_1CE_0 = this.con;
					string arg_1CE_1 = "update cal_result set parity=0";
					value = Missing.Value;
					arg_1CE_0.Execute(arg_1CE_1, out value, -1);
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_search_symptoms.Items[this.lst_search_symptoms1.SelectedIndex]));
					this.ListBox2.Items.Add(RuntimeHelpers.GetObjectValue(this.lst_search_symptoms1.SelectedItem));
				}
			}
		}

		private void R1_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void R2_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void Button8_Click(object sender, EventArgs e)
		{
			frm_doctor frm_doctor = new frm_doctor();
			frm_doctor.Show();
		}

		private void Button9_Click(object sender, EventArgs e)
		{
			frm_patientopen frm_patientopen = new frm_patientopen();
			frm_patientopen.Show();
		}

		private void search11_TextChanged(object sender, EventArgs e)
		{
		}

		private void CheckBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (this.CheckBox1.Checked)
			{
				this.ToolTip1.Active = false;
			}
			else
			{
				this.ToolTip1.Active = true;
			}
		}
	}
}
