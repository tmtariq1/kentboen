using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class Form1 : Form
	{
		private IContainer components;

		[DebuggerNonUserCode]
		public Form1()
		{
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			this.AutoScaleMode = AutoScaleMode.Font;
			this.Text = "Form1";
		}
	}
}
