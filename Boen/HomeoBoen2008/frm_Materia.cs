using ADODB;
using AxMSFlexGridLib;
using HomeoBoen2008.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	public class frm_Materia : Form
	{
		private IContainer components;

		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[AccessedThroughProperty("lst_remedies")]
		private ListBox _lst_remedies;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("MSFlexGrid2")]
		private AxMSFlexGrid _MSFlexGrid2;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("Button4")]
		private Button _Button4;

		[AccessedThroughProperty("Label7")]
		private Label _Label7;

		[AccessedThroughProperty("Label6")]
		private Label _Label6;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("Label4")]
		private Label _Label4;

		[AccessedThroughProperty("Label8")]
		private Label _Label8;

		[AccessedThroughProperty("search1")]
		private TextBox _search1;

		[AccessedThroughProperty("Button5")]
		private Button _Button5;

		[AccessedThroughProperty("Label9")]
		private Label _Label9;

		[AccessedThroughProperty("SaveFileDialog1")]
		private SaveFileDialog _SaveFileDialog1;

		[AccessedThroughProperty("Back")]
		private Button _Back;

		[AccessedThroughProperty("ListBox1")]
		private ListBox _ListBox1;

		[AccessedThroughProperty("R1")]
		private RadioButton _R1;

		[AccessedThroughProperty("R2")]
		private RadioButton _R2;

		[AccessedThroughProperty("R3")]
		private RadioButton _R3;

		[AccessedThroughProperty("Label11")]
		private Label _Label11;

		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		[AccessedThroughProperty("ToolTip2")]
		private ToolTip _ToolTip2;

		[AccessedThroughProperty("Label12")]
		private Label _Label12;

		[AccessedThroughProperty("CheckBox1")]
		private CheckBox _CheckBox1;

		[AccessedThroughProperty("ToolTip3")]
		private ToolTip _ToolTip3;

		[AccessedThroughProperty("MSFlexGrid1")]
		private AxMSFlexGrid _MSFlexGrid1;

		private Connection con;

		private Recordset rst;

		private Recordset rst1;

		internal virtual GroupBox GroupBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._GroupBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._GroupBox1 = value;
			}
		}

		internal virtual ListBox lst_remedies
		{
			[DebuggerNonUserCode]
			get
			{
				return this._lst_remedies;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._lst_remedies != null)
				{
					this._lst_remedies.SelectedIndexChanged -= new EventHandler(this.lst_remedies_SelectedIndexChanged);
				}
				this._lst_remedies = value;
				if (this._lst_remedies != null)
				{
					this._lst_remedies.SelectedIndexChanged += new EventHandler(this.lst_remedies_SelectedIndexChanged);
				}
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual AxMSFlexGrid MSFlexGrid2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._MSFlexGrid2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._MSFlexGrid2 = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual Button Button4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button4 != null)
				{
					this._Button4.Click -= new EventHandler(this.Button4_Click);
				}
				this._Button4 = value;
				if (this._Button4 != null)
				{
					this._Button4.Click += new EventHandler(this.Button4_Click);
				}
			}
		}

		internal virtual Label Label7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label7 = value;
			}
		}

		internal virtual Label Label6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label6 = value;
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual Label Label4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label4 = value;
			}
		}

		internal virtual Label Label8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label8 = value;
			}
		}

		internal virtual TextBox search1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._search1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._search1 = value;
			}
		}

		internal virtual Button Button5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button5 != null)
				{
					this._Button5.Click -= new EventHandler(this.Button5_Click);
				}
				this._Button5 = value;
				if (this._Button5 != null)
				{
					this._Button5.Click += new EventHandler(this.Button5_Click);
				}
			}
		}

		internal virtual Label Label9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label9 = value;
			}
		}

		internal virtual SaveFileDialog SaveFileDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._SaveFileDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._SaveFileDialog1 = value;
			}
		}

		internal virtual Button Back
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Back;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Back != null)
				{
					this._Back.Click -= new EventHandler(this.Back_Click);
				}
				this._Back = value;
				if (this._Back != null)
				{
					this._Back.Click += new EventHandler(this.Back_Click);
				}
			}
		}

		internal virtual ListBox ListBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._ListBox1 != null)
				{
					this._ListBox1.SelectedIndexChanged -= new EventHandler(this.ListBox1_SelectedIndexChanged);
				}
				this._ListBox1 = value;
				if (this._ListBox1 != null)
				{
					this._ListBox1.SelectedIndexChanged += new EventHandler(this.ListBox1_SelectedIndexChanged);
				}
			}
		}

		internal virtual RadioButton R1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._R1 != null)
				{
					this._R1.CheckedChanged -= new EventHandler(this.R1_CheckedChanged);
					this._R1.Click -= new EventHandler(this.R1_Click);
				}
				this._R1 = value;
				if (this._R1 != null)
				{
					this._R1.CheckedChanged += new EventHandler(this.R1_CheckedChanged);
					this._R1.Click += new EventHandler(this.R1_Click);
				}
			}
		}

		internal virtual RadioButton R2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._R2 != null)
				{
					this._R2.CheckedChanged -= new EventHandler(this.R2_CheckedChanged);
					this._R2.Click -= new EventHandler(this.R2_Click);
				}
				this._R2 = value;
				if (this._R2 != null)
				{
					this._R2.CheckedChanged += new EventHandler(this.R2_CheckedChanged);
					this._R2.Click += new EventHandler(this.R2_Click);
				}
			}
		}

		internal virtual RadioButton R3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._R3 != null)
				{
					this._R3.CheckedChanged -= new EventHandler(this.R3_CheckedChanged);
					this._R3.Click -= new EventHandler(this.R3_Click);
				}
				this._R3 = value;
				if (this._R3 != null)
				{
					this._R3.CheckedChanged += new EventHandler(this.R3_CheckedChanged);
					this._R3.Click += new EventHandler(this.R3_Click);
				}
			}
		}

		internal virtual Label Label11
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label11;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label11 = value;
			}
		}

		internal virtual ToolTip ToolTip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip1 = value;
			}
		}

		internal virtual ToolTip ToolTip2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip2 = value;
			}
		}

		internal virtual Label Label12
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label12;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Label12 != null)
				{
					this._Label12.Click -= new EventHandler(this.Label12_Click);
				}
				this._Label12 = value;
				if (this._Label12 != null)
				{
					this._Label12.Click += new EventHandler(this.Label12_Click);
				}
			}
		}

		internal virtual CheckBox CheckBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._CheckBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._CheckBox1 != null)
				{
					this._CheckBox1.CheckedChanged -= new EventHandler(this.CheckBox1_CheckedChanged);
				}
				this._CheckBox1 = value;
				if (this._CheckBox1 != null)
				{
					this._CheckBox1.CheckedChanged += new EventHandler(this.CheckBox1_CheckedChanged);
				}
			}
		}

		internal virtual ToolTip ToolTip3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip3 = value;
			}
		}

		internal virtual AxMSFlexGrid MSFlexGrid1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._MSFlexGrid1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._MSFlexGrid1 = value;
			}
		}

		public frm_Materia()
		{
			base.Load += new EventHandler(this.frm_Materia_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.rst1 = new RecordsetClass();
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_Materia));
			this.GroupBox1 = new GroupBox();
			this.Label11 = new Label();
			this.ListBox1 = new ListBox();
			this.Label9 = new Label();
			this.Label7 = new Label();
			this.Label6 = new Label();
			this.Label5 = new Label();
			this.Label4 = new Label();
			this.Label3 = new Label();
			this.MSFlexGrid2 = new AxMSFlexGrid();
			this.Label2 = new Label();
			this.Label1 = new Label();
			this.MSFlexGrid1 = new AxMSFlexGrid();
			this.lst_remedies = new ListBox();
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.Button4 = new Button();
			this.Label8 = new Label();
			this.search1 = new TextBox();
			this.Button5 = new Button();
			this.SaveFileDialog1 = new SaveFileDialog();
			this.Back = new Button();
			this.R1 = new RadioButton();
			this.R2 = new RadioButton();
			this.R3 = new RadioButton();
			this.ToolTip1 = new ToolTip(this.components);
			this.CheckBox1 = new CheckBox();
			this.ToolTip2 = new ToolTip(this.components);
			this.Label12 = new Label();
			this.ToolTip3 = new ToolTip(this.components);
			this.GroupBox1.SuspendLayout();
			((ISupportInitialize)this.MSFlexGrid2).BeginInit();
			((ISupportInitialize)this.MSFlexGrid1).BeginInit();
			this.SuspendLayout();
			this.GroupBox1.BackColor = Color.PaleGoldenrod;
			this.GroupBox1.Controls.Add(this.Label11);
			this.GroupBox1.Controls.Add(this.ListBox1);
			this.GroupBox1.Controls.Add(this.Label9);
			this.GroupBox1.Controls.Add(this.Label7);
			this.GroupBox1.Controls.Add(this.Label6);
			this.GroupBox1.Controls.Add(this.Label5);
			this.GroupBox1.Controls.Add(this.Label4);
			this.GroupBox1.Controls.Add(this.Label3);
			this.GroupBox1.Controls.Add(this.MSFlexGrid2);
			this.GroupBox1.Controls.Add(this.Label2);
			this.GroupBox1.Controls.Add(this.Label1);
			this.GroupBox1.Controls.Add(this.MSFlexGrid1);
			this.GroupBox1.Controls.Add(this.lst_remedies);
			Control arg_2E1_0 = this.GroupBox1;
			Point location = new Point(12, 128);
			arg_2E1_0.Location = location;
			this.GroupBox1.Name = "GroupBox1";
			Control arg_30E_0 = this.GroupBox1;
			Size size = new Size(1013, 606);
			arg_30E_0.Size = size;
			this.GroupBox1.TabIndex = 0;
			this.GroupBox1.TabStop = false;
			this.Label11.AutoSize = true;
			this.Label11.BackColor = Color.White;
			this.Label11.BorderStyle = BorderStyle.FixedSingle;
			this.Label11.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label11.ForeColor = Color.FromArgb(255, 128, 128);
			Control arg_3A4_0 = this.Label11;
			location = new Point(363, 13);
			arg_3A4_0.Location = location;
			this.Label11.Name = "Label11";
			Control arg_3CB_0 = this.Label11;
			size = new Size(97, 15);
			arg_3CB_0.Size = size;
			this.Label11.TabIndex = 12;
			this.Label11.Text = "Remedy Family:";
			this.ListBox1.FormattingEnabled = true;
			this.ListBox1.HorizontalScrollbar = true;
			Control arg_41A_0 = this.ListBox1;
			location = new Point(196, 33);
			arg_41A_0.Location = location;
			this.ListBox1.Name = "ListBox1";
			Control arg_447_0 = this.ListBox1;
			size = new Size(811, 368);
			arg_447_0.Size = size;
			this.ListBox1.Sorted = true;
			this.ListBox1.TabIndex = 11;
			this.ToolTip2.SetToolTip(this.ListBox1, "یہ آپ کی جانب سے منتخب کردہ دوا کی کل علامات ریپرٹری میں سے حاصل کی گئ ہیں۔");
			this.ToolTip1.SetToolTip(this.ListBox1, "These are the symptoms of a remedy selected by you.");
			this.Label9.AutoSize = true;
			this.Label9.BackColor = Color.FromArgb(255, 255, 192);
			this.Label9.BorderStyle = BorderStyle.FixedSingle;
			this.Label9.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label9.ForeColor = Color.FromArgb(192, 64, 0);
			Control arg_512_0 = this.Label9;
			location = new Point(882, 12);
			arg_512_0.Location = location;
			this.Label9.Name = "Label9";
			Control arg_538_0 = this.Label9;
			size = new Size(2, 18);
			arg_538_0.Size = size;
			this.Label9.TabIndex = 10;
			this.Label7.AutoSize = true;
			this.Label7.BackColor = Color.White;
			this.Label7.BorderStyle = BorderStyle.FixedSingle;
			this.Label7.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label7.ForeColor = Color.Green;
			Control arg_5B7_0 = this.Label7;
			location = new Point(950, 409);
			arg_5B7_0.Location = location;
			this.Label7.Name = "Label7";
			Control arg_5DE_0 = this.Label7;
			size = new Size(85, 15);
			arg_5DE_0.Size = size;
			this.Label7.TabIndex = 9;
			this.Label7.Text = "Incompatible:";
			this.Label6.AutoSize = true;
			this.Label6.BackColor = Color.White;
			this.Label6.BorderStyle = BorderStyle.FixedSingle;
			this.Label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label6.ForeColor = Color.Blue;
			Control arg_66D_0 = this.Label6;
			location = new Point(866, 409);
			arg_66D_0.Location = location;
			this.Label6.Name = "Label6";
			Control arg_694_0 = this.Label6;
			size = new Size(71, 15);
			arg_694_0.Size = size;
			this.Label6.TabIndex = 8;
			this.Label6.Text = "Follows W:";
			this.Label5.AutoSize = true;
			this.Label5.BackColor = Color.White;
			this.Label5.BorderStyle = BorderStyle.FixedSingle;
			this.Label5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label5.ForeColor = Color.FromArgb(255, 192, 128);
			Control arg_731_0 = this.Label5;
			location = new Point(796, 409);
			arg_731_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_758_0 = this.Label5;
			size = new Size(50, 15);
			arg_758_0.Size = size;
			this.Label5.TabIndex = 7;
			this.Label5.Text = "Compli:";
			this.Label4.AutoSize = true;
			this.Label4.BackColor = Color.White;
			this.Label4.BorderStyle = BorderStyle.FixedSingle;
			this.Label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label4.ForeColor = Color.Red;
			Control arg_7E6_0 = this.Label4;
			location = new Point(709, 408);
			arg_7E6_0.Location = location;
			this.Label4.Name = "Label4";
			Control arg_80D_0 = this.Label4;
			size = new Size(35, 15);
			arg_80D_0.Size = size;
			this.Label4.TabIndex = 6;
			this.Label4.Text = "Anti:";
			this.Label3.AutoSize = true;
			this.Label3.BackColor = Color.Maroon;
			this.Label3.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label3.ForeColor = Color.FromArgb(192, 192, 0);
			Control arg_897_0 = this.Label3;
			location = new Point(470, 12);
			arg_897_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_8BD_0 = this.Label3;
			size = new Size(0, 15);
			arg_8BD_0.Size = size;
			this.Label3.TabIndex = 5;
			Control arg_8E6_0 = this.MSFlexGrid2;
			location = new Point(683, 427);
			arg_8E6_0.Location = location;
			this.MSFlexGrid2.Name = "MSFlexGrid2";
			this.MSFlexGrid2.OcxState = (AxHost.State)componentResourceManager.GetObject("MSFlexGrid2.OcxState");
			Control arg_92E_0 = this.MSFlexGrid2;
			size = new Size(324, 163);
			arg_92E_0.Size = size;
			this.MSFlexGrid2.TabIndex = 4;
			this.Label2.AutoSize = true;
			this.Label2.BackColor = Color.White;
			this.Label2.BorderStyle = BorderStyle.FixedSingle;
			this.Label2.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = Color.FromArgb(192, 0, 192);
			Control arg_9B4_0 = this.Label2;
			location = new Point(309, 12);
			arg_9B4_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_9DB_0 = this.Label2;
			size = new Size(18, 18);
			arg_9DB_0.Size = size;
			this.Label2.TabIndex = 3;
			this.Label2.Text = "0";
			this.Label1.AutoSize = true;
			this.Label1.BackColor = Color.FromArgb(0, 192, 192);
			this.Label1.BorderStyle = BorderStyle.FixedSingle;
			this.Label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_A61_0 = this.Label1;
			location = new Point(196, 15);
			arg_A61_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_A88_0 = this.Label1;
			size = new Size(102, 15);
			arg_A88_0.Size = size;
			this.Label1.TabIndex = 2;
			this.Label1.Text = "Total Symptoms:";
			Control arg_ABE_0 = this.MSFlexGrid1;
			location = new Point(194, 32);
			arg_ABE_0.Location = location;
			this.MSFlexGrid1.Name = "MSFlexGrid1";
			this.MSFlexGrid1.OcxState = (AxHost.State)componentResourceManager.GetObject("MSFlexGrid1.OcxState");
			Control arg_B06_0 = this.MSFlexGrid1;
			size = new Size(337, 456);
			arg_B06_0.Size = size;
			this.MSFlexGrid1.TabIndex = 1;
			this.MSFlexGrid1.Visible = false;
			this.lst_remedies.Font = new Font("Courier New", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.lst_remedies.ForeColor = Color.FromArgb(0, 0, 192);
			this.lst_remedies.HorizontalScrollbar = true;
			this.lst_remedies.ItemHeight = 15;
			Control arg_B81_0 = this.lst_remedies;
			location = new Point(8, 16);
			arg_B81_0.Location = location;
			this.lst_remedies.Name = "lst_remedies";
			Control arg_BAE_0 = this.lst_remedies;
			size = new Size(180, 574);
			arg_BAE_0.Size = size;
			this.lst_remedies.Sorted = true;
			this.lst_remedies.TabIndex = 0;
			this.ToolTip2.SetToolTip(this.lst_remedies, "یہ ھومیو پیتھک ادویات کے ناموں کی فہرست ھے کسی نام پر کلک کرکے علامات دیکھیں۔");
			this.ToolTip1.SetToolTip(this.lst_remedies, "These are the homepathic remedies names list,select one to view symptoms in english or urdu.");
			this.Button1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_C25_0 = this.Button1;
			location = new Point(12, 4);
			arg_C25_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_C4C_0 = this.Button1;
			size = new Size(81, 26);
			arg_C4C_0.Size = size;
			this.Button1.TabIndex = 1;
			this.Button1.Text = "Print";
			this.Button1.UseVisualStyleBackColor = true;
			this.Button2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_CA7_0 = this.Button2;
			location = new Point(93, 4);
			arg_CA7_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_CCE_0 = this.Button2;
			size = new Size(82, 26);
			arg_CCE_0.Size = size;
			this.Button2.TabIndex = 2;
			this.Button2.Text = "Save As ";
			this.ToolTip2.SetToolTip(this.Button2, "کسی دوا کی علامات کو انگریزی میں ورڈ فایئل کے طور پر محفوظ کریں۔");
			this.ToolTip1.SetToolTip(this.Button2, "Save symptoms of remedy as Word Document in English only, Urdu will give Errors.");
			this.Button2.UseVisualStyleBackColor = true;
			this.Button4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_D58_0 = this.Button4;
			location = new Point(175, 4);
			arg_D58_0.Location = location;
			this.Button4.Name = "Button4";
			Control arg_D7F_0 = this.Button4;
			size = new Size(101, 26);
			arg_D7F_0.Size = size;
			this.Button4.TabIndex = 4;
			this.Button4.Text = "Export to Excel";
			this.ToolTip2.SetToolTip(this.Button4, "اردو علامات کو ایکسل کی فائل میں محفوظ کریں۔");
			this.ToolTip1.SetToolTip(this.Button4, "Export symptoms of remedy into a excel file .");
			this.Button4.UseVisualStyleBackColor = true;
			this.Label8.AutoSize = true;
			this.Label8.BorderStyle = BorderStyle.FixedSingle;
			Control arg_E05_0 = this.Label8;
			location = new Point(442, 75);
			arg_E05_0.Location = location;
			this.Label8.Name = "Label8";
			Control arg_E2C_0 = this.Label8;
			size = new Size(72, 15);
			arg_E2C_0.Size = size;
			this.Label8.TabIndex = 5;
			this.Label8.Text = "Search Word";
			this.Label8.Visible = false;
			this.search1.Enabled = false;
			Control arg_E7A_0 = this.search1;
			location = new Point(440, 91);
			arg_E7A_0.Location = location;
			this.search1.Name = "search1";
			Control arg_EA1_0 = this.search1;
			size = new Size(123, 20);
			arg_EA1_0.Size = size;
			this.search1.TabIndex = 6;
			this.search1.Visible = false;
			this.Button5.BackColor = Color.Brown;
			this.Button5.Enabled = false;
			this.Button5.ForeColor = Color.Yellow;
			Control arg_EFF_0 = this.Button5;
			location = new Point(569, 91);
			arg_EFF_0.Location = location;
			this.Button5.Name = "Button5";
			Control arg_F26_0 = this.Button5;
			size = new Size(51, 22);
			arg_F26_0.Size = size;
			this.Button5.TabIndex = 5;
			this.Button5.Text = "Search";
			this.Button5.UseVisualStyleBackColor = false;
			this.Button5.Visible = false;
			this.Back.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Back.ForeColor = Color.Red;
			Control arg_FA0_0 = this.Back;
			location = new Point(278, 4);
			arg_FA0_0.Location = location;
			this.Back.Name = "Back";
			Control arg_FC7_0 = this.Back;
			size = new Size(44, 26);
			arg_FC7_0.Size = size;
			this.Back.TabIndex = 4;
			this.Back.Text = "Back";
			this.Back.UseVisualStyleBackColor = true;
			this.R1.AutoSize = true;
			this.R1.Checked = true;
			this.R1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_103B_0 = this.R1;
			location = new Point(12, 92);
			arg_103B_0.Location = location;
			this.R1.Name = "R1";
			Control arg_1062_0 = this.R1;
			size = new Size(66, 17);
			arg_1062_0.Size = size;
			this.R1.TabIndex = 7;
			this.R1.TabStop = true;
			this.R1.Text = "English";
			this.ToolTip1.SetToolTip(this.R1, "To view symptoms in English only please click here.");
			this.R1.UseVisualStyleBackColor = true;
			this.R2.AutoSize = true;
			this.R2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_10EC_0 = this.R2;
			location = new Point(83, 92);
			arg_10EC_0.Location = location;
			this.R2.Name = "R2";
			Control arg_1113_0 = this.R2;
			size = new Size(52, 17);
			arg_1113_0.Size = size;
			this.R2.TabIndex = 8;
			this.R2.Text = "Urdu";
			this.ToolTip2.SetToolTip(this.R2, "اردو میں علامات دیکھنے کے لیے یہاں کلک کریں");
			this.R2.UseVisualStyleBackColor = true;
			this.R3.AutoSize = true;
			this.R3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_1194_0 = this.R3;
			location = new Point(147, 92);
			arg_1194_0.Location = location;
			this.R3.Name = "R3";
			Control arg_11BB_0 = this.R3;
			size = new Size(117, 17);
			arg_11BB_0.Size = size;
			this.R3.TabIndex = 9;
			this.R3.TabStop = true;
			this.R3.Text = "Both Languages";
			this.ToolTip1.SetToolTip(this.R3, "English Urdu Languages");
			this.R3.UseVisualStyleBackColor = true;
			this.ToolTip1.AutoPopDelay = 20000;
			this.ToolTip1.BackColor = Color.Ivory;
			this.ToolTip1.ForeColor = Color.FromArgb(192, 64, 0);
			this.ToolTip1.InitialDelay = 100;
			this.ToolTip1.IsBalloon = true;
			this.ToolTip1.ReshowDelay = 0;
			this.ToolTip1.ShowAlways = true;
			this.ToolTip1.StripAmpersands = true;
			this.ToolTip1.ToolTipIcon = ToolTipIcon.Info;
			this.CheckBox1.AutoSize = true;
			this.CheckBox1.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CheckBox1.ForeColor = Color.FromArgb(0, 0, 192);
			Control arg_12E1_0 = this.CheckBox1;
			location = new Point(267, 93);
			arg_12E1_0.Location = location;
			this.CheckBox1.Name = "CheckBox1";
			Control arg_130B_0 = this.CheckBox1;
			size = new Size(152, 20);
			arg_130B_0.Size = size;
			this.CheckBox1.TabIndex = 12;
			this.CheckBox1.Text = "Disable Help Text";
			this.ToolTip1.SetToolTip(this.CheckBox1, "Check this box if you want to disable Help Text Baloons");
			this.CheckBox1.UseVisualStyleBackColor = true;
			this.ToolTip2.AutoPopDelay = 10000;
			this.ToolTip2.BackColor = Color.Ivory;
			this.ToolTip2.ForeColor = Color.FromArgb(192, 64, 0);
			this.ToolTip2.InitialDelay = 0;
			this.ToolTip2.IsBalloon = true;
			this.ToolTip2.ReshowDelay = 100;
			this.ToolTip2.ShowAlways = true;
			this.ToolTip2.StripAmpersands = true;
			this.ToolTip2.ToolTipIcon = ToolTipIcon.Info;
			this.Label12.AutoSize = true;
			this.Label12.BorderStyle = BorderStyle.FixedSingle;
			this.Label12.Font = new Font("Courier New", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Label12.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_142E_0 = this.Label12;
			location = new Point(17, 46);
			arg_142E_0.Location = location;
			this.Label12.Name = "Label12";
			Control arg_1454_0 = this.Label12;
			size = new Size(2, 20);
			arg_1454_0.Size = size;
			this.Label12.TabIndex = 11;
			this.ToolTip3.AutoPopDelay = 5000;
			this.ToolTip3.BackColor = Color.FromArgb(192, 255, 192);
			this.ToolTip3.ForeColor = Color.Green;
			this.ToolTip3.InitialDelay = 0;
			this.ToolTip3.IsBalloon = true;
			this.ToolTip3.ReshowDelay = 0;
			this.ToolTip3.ShowAlways = true;
			this.ToolTip3.StripAmpersands = true;
			this.ToolTip3.ToolTipIcon = ToolTipIcon.Info;
			this.ToolTip3.ToolTipTitle = "Pak Homeopathic Repertory 2008  Build April-2008";
			size = new Size(5, 13);
			this.AutoScaleBaseSize = size;
			this.BackColor = Color.Beige;
			size = new Size(1028, 746);
			this.ClientSize = size;
			this.Controls.Add(this.CheckBox1);
			this.Controls.Add(this.Label12);
			this.Controls.Add(this.R3);
			this.Controls.Add(this.R2);
			this.Controls.Add(this.R1);
			this.Controls.Add(this.Back);
			this.Controls.Add(this.Button5);
			this.Controls.Add(this.search1);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Button4);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.GroupBox1);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_Materia";
			this.Text = "P H R :: Pak Homeopathic Repertory 2008 Ver 1.0 :::   REVERSE HOMEOPATHIC MATERIA MEDICA GENERATED FROM \"BOENNINGHAUSEN\" Build Apr-2008";
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			((ISupportInitialize)this.MSFlexGrid2).EndInit();
			((ISupportInitialize)this.MSFlexGrid1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void frm_Materia_Load(object sender, EventArgs e)
		{
			this.ToolTip1.SetToolTip(this.lst_remedies, "These are remedies list , click on any remedy name to view symptoms ");
			this.ToolTip1.SetToolTip(this.ListBox1, "These are symptoms of your selected remedy from a specific homeopathic repertory");
			this.Label12.Text = MyProject.Forms.frm_Main.Label9.Text;
			this.Connect_Database();
			this.MSFlexGrid1.set_ColWidth(0, 2700);
			this.MSFlexGrid1.set_ColWidth(1, 700);
			this.MSFlexGrid2.set_ColWidth(0, 1200);
			this.MSFlexGrid2.set_ColWidth(1, 1200);
			this.MSFlexGrid2.set_ColWidth(2, 1200);
			this.MSFlexGrid2.set_ColWidth(3, 1200);
			this.Label2.Visible = false;
			this.Label4.Visible = false;
			this.Label5.Visible = false;
			this.Label6.Visible = false;
			this.Label7.Visible = false;
			this.Label9.Visible = false;
			_Connection arg_114_0 = this.con;
			string arg_114_1 = "select * from remedies";
			object value = Missing.Value;
			this.rst = arg_114_0.Execute(arg_114_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				this.lst_remedies.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
				this.rst.MoveNext();
			}
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void lst_remedies_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.Label2.Visible = false;
			this.Label4.Visible = false;
			this.Label5.Visible = false;
			this.Label6.Visible = false;
			this.Label7.Visible = false;
			this.Label9.Visible = false;
			this.MSFlexGrid1.Clear();
			this.MSFlexGrid2.Clear();
			this.MSFlexGrid1.Rows = 2;
			this.MSFlexGrid1.Cols = 2;
			this.MSFlexGrid1.Row = 0;
			this.MSFlexGrid1.Col = 0;
			this.MSFlexGrid1.Text = "Symptoms";
			this.MSFlexGrid1.Row = 0;
			this.MSFlexGrid1.Col = 1;
			this.MSFlexGrid1.Text = "Grades";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 0;
			this.MSFlexGrid2.Text = "AntiDote";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 1;
			this.MSFlexGrid2.Text = "Compliment";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 2;
			this.MSFlexGrid2.Text = "Follows Well";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 3;
			this.MSFlexGrid2.Text = "Incompatible";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid1.Col = 1;
			this.MSFlexGrid1.Sort = 1;
			int[] array = new int[501];
			Cursor.Current = Cursors.WaitCursor;
			_Connection arg_1D5_0 = this.con;
			string arg_1D5_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select RemId,Poly from Remedies where RemName='", this.lst_remedies.SelectedItem), "'"));
			object value = Missing.Value;
			this.rst = arg_1D5_0.Execute(arg_1D5_1, out value, -1);
			int remid = Conversions.ToInteger(this.rst.Fields["RemId"].Value);
			if (Operators.ConditionalCompareObjectEqual(this.rst.Fields["Poly"].Value, "1", false))
			{
				this.Label9.Text = "Polychrest";
			}
			else
			{
				this.Label9.Text = "Non Polychrest";
			}
			this.sym_load();
			this.cal_symptom(remid);
			Cursor.Current = Cursors.Arrow;
		}

		private void cal_symptom(int remid)
		{
			int[] array = new int[501];
			_Connection arg_31_0 = this.con;
			string arg_31_1 = "SELECT Remedies.RemName, RemFamilies.FamId, Families.FamilyName FROM Families INNER JOIN (Remedies INNER JOIN RemFamilies ON Remedies.RemId = RemFamilies.RemId) ON Families.FamId = RemFamilies.FamId where Remedies.RemId= " + Conversions.ToString(remid) + " ";
			object value = Missing.Value;
			this.rst = arg_31_0.Execute(arg_31_1, out value, -1);
			this.Label3.Text = Conversions.ToString(this.rst.Fields["FamilyName"].Value);
			int num = 0;
			_Connection arg_8D_0 = this.con;
			string arg_8D_1 = "SELECT * from  RemFilters_AntidotedBy where RemId= " + Conversions.ToString(remid) + " ";
			value = Missing.Value;
			this.rst = arg_8D_0.Execute(arg_8D_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_EF_0 = 0;
				int num2 = num - 1;
				for (int i = arg_EF_0; i <= num2; i++)
				{
					_Connection arg_11D_0 = this.con;
					string arg_11D_1 = "select * from remedies where RemId=" + Conversions.ToString(array[i]) + " ";
					value = Missing.Value;
					this.rst = arg_11D_0.Execute(arg_11D_1, out value, -1);
					this.MSFlexGrid2.Rows = i + 2;
					this.MSFlexGrid2.Col = 0;
					this.MSFlexGrid2.Row = i + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemShortcut"].Value);
				}
				this.Label4.Text = Conversions.ToString(num);
				num = 0;
				_Connection arg_1BE_0 = this.con;
				string arg_1BE_1 = "SELECT * from RemFilters_Complementary where RemId= " + Conversions.ToString(remid) + " ";
				value = Missing.Value;
				this.rst = arg_1BE_0.Execute(arg_1BE_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_220_0 = 0;
				int num3 = num - 1;
				for (int j = arg_220_0; j <= num3; j++)
				{
					_Connection arg_24E_0 = this.con;
					string arg_24E_1 = "select * from remedies where RemId=" + Conversions.ToString(array[j]) + " ";
					value = Missing.Value;
					this.rst = arg_24E_0.Execute(arg_24E_1, out value, -1);
					if (j + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = j + 2;
					}
					this.MSFlexGrid2.Col = 1;
					this.MSFlexGrid2.Row = j + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemShortcut"].Value);
				}
				this.Label5.Text = Conversions.ToString(num);
				num = 0;
				_Connection arg_2FF_0 = this.con;
				string arg_2FF_1 = "SELECT  * from RemFilters_FollowsWell where RemId= " + Conversions.ToString(remid) + " ";
				value = Missing.Value;
				this.rst = arg_2FF_0.Execute(arg_2FF_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_361_0 = 0;
				int num4 = num - 1;
				for (int k = arg_361_0; k <= num4; k++)
				{
					_Connection arg_391_0 = this.con;
					string arg_391_1 = "select * from remedies where RemId=" + Conversions.ToString(array[k]) + " ";
					value = Missing.Value;
					this.rst = arg_391_0.Execute(arg_391_1, out value, -1);
					if (k + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = k + 2;
					}
					this.MSFlexGrid2.Col = 2;
					this.MSFlexGrid2.Row = k + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemShortcut"].Value);
				}
				this.Label6.Text = Conversions.ToString(num);
				num = 0;
				_Connection arg_448_0 = this.con;
				string arg_448_1 = "SELECT * from RemFilters_Incompatible where RemId= " + Conversions.ToString(remid) + " ";
				value = Missing.Value;
				this.rst = arg_448_0.Execute(arg_448_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_4AA_0 = 0;
				int num5 = num - 1;
				for (int l = arg_4AA_0; l <= num5; l++)
				{
					_Connection arg_4DA_0 = this.con;
					string arg_4DA_1 = "select * from remedies where RemId=" + Conversions.ToString(array[l]) + " ";
					value = Missing.Value;
					this.rst = arg_4DA_0.Execute(arg_4DA_1, out value, -1);
					if (l + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = l + 2;
					}
					this.MSFlexGrid2.Col = 3;
					this.MSFlexGrid2.Row = l + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemShortcut"].Value);
				}
				this.Label7.Text = Conversions.ToString(num);
				this.Label2.Visible = true;
				this.Label4.Visible = true;
				this.Label5.Visible = true;
				this.Label6.Visible = true;
				this.Label7.Visible = true;
				this.Label9.Visible = true;
			}
		}

		private void Button5_Click(object sender, EventArgs e)
		{
			this.MSFlexGrid1.Clear();
			this.MSFlexGrid2.Clear();
			this.MSFlexGrid2.Cols = 4;
			this.MSFlexGrid1.Rows = 2;
			this.MSFlexGrid1.Cols = 2;
			this.MSFlexGrid1.Row = 0;
			this.MSFlexGrid1.Col = 0;
			this.MSFlexGrid1.Text = "Symptoms";
			this.MSFlexGrid1.Row = 0;
			this.MSFlexGrid1.Col = 1;
			this.MSFlexGrid1.Text = "Grades";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 0;
			this.MSFlexGrid2.Text = "AntiDote";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 1;
			this.MSFlexGrid2.Text = "Complementary";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 2;
			this.MSFlexGrid2.Text = "Follow Well";
			this.MSFlexGrid2.Row = 0;
			this.MSFlexGrid2.Col = 3;
			this.MSFlexGrid2.Text = "Incompatible";
			int[] array = new int[501];
			int num = 1;
			Cursor.Current = Cursors.WaitCursor;
			_Connection arg_176_0 = this.con;
			string arg_176_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select RemId,Poly from Remedies where RemName ='", this.lst_remedies.SelectedItem), "' "));
			object value = Missing.Value;
			this.rst = arg_176_0.Execute(arg_176_1, out value, -1);
			int value2 = Conversions.ToInteger(this.rst.Fields["RemId"].Value);
			if (Operators.ConditionalCompareObjectEqual(this.rst.Fields["Poly"].Value, "1", false))
			{
				this.Label9.Text = "Polychrest";
			}
			else
			{
				this.Label9.Text = "Non Polychrest";
			}
			_Connection arg_23D_0 = this.con;
			string arg_23D_1 = string.Concat(new string[]
			{
				"SELECT Remedies.RemId, RemSymptoms.Weight, Symptoms.SymName FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Remedies.RemId=",
				Conversions.ToString(value2),
				" and symname like '%",
				this.search1.Text,
				"%'"
			});
			value = Missing.Value;
			this.rst = arg_23D_0.Execute(arg_23D_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					this.MSFlexGrid1.Rows = num + 1;
					this.MSFlexGrid1.Row = num;
					this.MSFlexGrid1.Col = 0;
					this.MSFlexGrid1.Text = Conversions.ToString(this.rst.Fields["SymName"].Value);
					this.MSFlexGrid1.Col = 1;
					this.MSFlexGrid1.Text = Conversions.ToString(this.rst.Fields["Weight"].Value);
					num++;
					this.rst.MoveNext();
				}
				Cursor.Current = Cursors.Arrow;
				this.Label2.Text = Conversions.ToString(num - 1);
				_Connection arg_348_0 = this.con;
				string arg_348_1 = "SELECT Remedies.RemName, RemFamilies.FamId, Families.FamilyName FROM Families INNER JOIN (Remedies INNER JOIN RemFamilies ON Remedies.RemId = RemFamilies.RemId) ON Families.FamId = RemFamilies.FamId where Remedies.RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_348_0.Execute(arg_348_1, out value, -1);
				this.Label3.Text = Conversions.ToString(this.rst.Fields["FamilyName"].Value);
				int num2 = 0;
				_Connection arg_3A4_0 = this.con;
				string arg_3A4_1 = "SELECT * from  RemFilters_AntidotedBy where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_3A4_0.Execute(arg_3A4_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num2] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num2++;
				}
				int arg_406_0 = 0;
				int num3 = num2 - 1;
				for (int i = arg_406_0; i <= num3; i++)
				{
					_Connection arg_436_0 = this.con;
					string arg_436_1 = "select * from remedies where RemId=" + Conversions.ToString(array[i]) + " ";
					value = Missing.Value;
					this.rst = arg_436_0.Execute(arg_436_1, out value, -1);
					this.MSFlexGrid2.Rows = i + 2;
					this.MSFlexGrid2.Col = 0;
					this.MSFlexGrid2.Row = i + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemName"].Value);
				}
				this.Label4.Text = Conversions.ToString(num2);
				num2 = 0;
				_Connection arg_4DC_0 = this.con;
				string arg_4DC_1 = "SELECT * from RemFilters_Complementary where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_4DC_0.Execute(arg_4DC_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num2] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num2++;
				}
				int arg_53E_0 = 0;
				int num4 = num2 - 1;
				for (int j = arg_53E_0; j <= num4; j++)
				{
					_Connection arg_56E_0 = this.con;
					string arg_56E_1 = "select * from remedies where RemId=" + Conversions.ToString(array[j]) + " ";
					value = Missing.Value;
					this.rst = arg_56E_0.Execute(arg_56E_1, out value, -1);
					if (j + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = j + 2;
					}
					this.MSFlexGrid2.Col = 1;
					this.MSFlexGrid2.Row = j + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemName"].Value);
				}
				this.Label5.Text = Conversions.ToString(num2);
				num2 = 0;
				_Connection arg_625_0 = this.con;
				string arg_625_1 = "SELECT  * from RemFilters_FollowsWell where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_625_0.Execute(arg_625_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num2] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num2++;
				}
				int arg_687_0 = 0;
				int num5 = num2 - 1;
				for (int k = arg_687_0; k <= num5; k++)
				{
					_Connection arg_6B7_0 = this.con;
					string arg_6B7_1 = "select * from remedies where RemId=" + Conversions.ToString(array[k]) + " ";
					value = Missing.Value;
					this.rst = arg_6B7_0.Execute(arg_6B7_1, out value, -1);
					if (k + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = k + 2;
					}
					this.MSFlexGrid2.Col = 2;
					this.MSFlexGrid2.Row = k + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemName"].Value);
				}
				this.Label6.Text = Conversions.ToString(num2);
				num2 = 0;
				_Connection arg_76E_0 = this.con;
				string arg_76E_1 = "SELECT * from RemFilters_Incompatible where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_76E_0.Execute(arg_76E_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num2] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num2++;
				}
				int arg_7D0_0 = 0;
				int num6 = num2 - 1;
				for (int l = arg_7D0_0; l <= num6; l++)
				{
					_Connection arg_800_0 = this.con;
					string arg_800_1 = "select * from remedies where RemId=" + Conversions.ToString(array[l]) + " ";
					value = Missing.Value;
					this.rst = arg_800_0.Execute(arg_800_1, out value, -1);
					if (l + 2 > this.MSFlexGrid2.Rows)
					{
						this.MSFlexGrid2.Rows = l + 2;
					}
					this.MSFlexGrid2.Col = 3;
					this.MSFlexGrid2.Row = l + 1;
					this.MSFlexGrid2.Text = Conversions.ToString(this.rst.Fields["RemName"].Value);
				}
				this.Label7.Text = Conversions.ToString(num2);
			}
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			FileStream stream = new FileStream("c:\\materia.doc", FileMode.Create, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream);
			int[] array = new int[501];
			streamWriter.WriteLine("Remedy:" + this.lst_remedies.Text);
			streamWriter.WriteLine("--------------------------------");
			streamWriter.WriteLine("                                ");
			streamWriter.WriteLine("Loaded Book:" + MyProject.Forms.frm_Main.Label9.Text);
			streamWriter.WriteLine("--------------------------------");
			streamWriter.WriteLine("                                ");
			streamWriter.WriteLine("Symptoms:-");
			streamWriter.WriteLine(this.Label2.Text);
			streamWriter.WriteLine("                                ");
			Cursor.Current = Cursors.WaitCursor;
			_Connection arg_FA_0 = this.con;
			string arg_FA_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select RemId,Poly from Remedies where RemName='", this.lst_remedies.SelectedItem), "'"));
			object value = Missing.Value;
			this.rst = arg_FA_0.Execute(arg_FA_1, out value, -1);
			int value2 = Conversions.ToInteger(this.rst.Fields["RemId"].Value);
			_Connection arg_14A_0 = this.con;
			string arg_14A_1 = "SELECT Remedies.RemId, RemSymptoms.Weight, Symptoms.SymName FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Remedies.RemId=" + Conversions.ToString(value2) + " ";
			value = Missing.Value;
			this.rst = arg_14A_0.Execute(arg_14A_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				streamWriter.WriteLine(Conversions.ToString(this.rst.Fields["SymName"].Value), RuntimeHelpers.GetObjectValue(this.rst.Fields["Weight"].Value));
				this.rst.MoveNext();
			}
			streamWriter.WriteLine("--------------------------------");
			streamWriter.WriteLine("          ");
			streamWriter.WriteLine("Remedy Family");
			streamWriter.WriteLine("          ");
			_Connection arg_21B_0 = this.con;
			string arg_21B_1 = "SELECT Remedies.RemName, RemFamilies.FamId, Families.FamilyName FROM Families INNER JOIN (Remedies INNER JOIN RemFamilies ON Remedies.RemId = RemFamilies.RemId) ON Families.FamId = RemFamilies.FamId where Remedies.RemId= " + Conversions.ToString(value2) + " ";
			value = Missing.Value;
			this.rst = arg_21B_0.Execute(arg_21B_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["FamilyName"].Value));
				this.rst.MoveNext();
			}
			int num = 0;
			streamWriter.WriteLine("--------------------------------");
			streamWriter.WriteLine("          ");
			streamWriter.WriteLine("Anti Doted remedies");
			streamWriter.WriteLine("          ");
			_Connection arg_2D0_0 = this.con;
			string arg_2D0_1 = "SELECT * from  RemFilters_AntidotedBy where RemId= " + Conversions.ToString(value2) + " ";
			value = Missing.Value;
			this.rst = arg_2D0_0.Execute(arg_2D0_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_336_0 = 0;
				int num2 = num - 1;
				for (int i = arg_336_0; i <= num2; i++)
				{
					_Connection arg_366_0 = this.con;
					string arg_366_1 = "select * from remedies where RemId=" + Conversions.ToString(array[i]) + " ";
					value = Missing.Value;
					this.rst = arg_366_0.Execute(arg_366_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
						this.rst.MoveNext();
					}
				}
				num = 0;
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("--------------------------------");
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("Complementary remedies");
				streamWriter.WriteLine("              ");
				_Connection arg_436_0 = this.con;
				string arg_436_1 = "SELECT * from RemFilters_Complementary where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_436_0.Execute(arg_436_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_49C_0 = 0;
				int num3 = num - 1;
				for (int j = arg_49C_0; j <= num3; j++)
				{
					_Connection arg_4CC_0 = this.con;
					string arg_4CC_1 = "select * from remedies where RemId=" + Conversions.ToString(array[j]) + " ";
					value = Missing.Value;
					this.rst = arg_4CC_0.Execute(arg_4CC_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
						this.rst.MoveNext();
					}
				}
				num = 0;
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("--------------------------------");
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("Follow Well remedies");
				streamWriter.WriteLine("              ");
				_Connection arg_59C_0 = this.con;
				string arg_59C_1 = "SELECT  * from RemFilters_FollowsWell where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_59C_0.Execute(arg_59C_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_602_0 = 0;
				int num4 = num - 1;
				for (int k = arg_602_0; k <= num4; k++)
				{
					_Connection arg_632_0 = this.con;
					string arg_632_1 = "select * from remedies where RemId=" + Conversions.ToString(array[k]) + " ";
					value = Missing.Value;
					this.rst = arg_632_0.Execute(arg_632_1, out value, -1);
					while (!this.rst.EOF & !this.rst.BOF)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
						this.rst.MoveNext();
					}
				}
				num = 0;
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("--------------------------------");
				streamWriter.WriteLine("              ");
				streamWriter.WriteLine("Incompatible remedies");
				streamWriter.WriteLine("              ");
				_Connection arg_702_0 = this.con;
				string arg_702_1 = "SELECT * from RemFilters_Incompatible where RemId= " + Conversions.ToString(value2) + " ";
				value = Missing.Value;
				this.rst = arg_702_0.Execute(arg_702_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					array[num] = Conversions.ToInteger(this.rst.Fields["TargetRemId"].Value);
					this.rst.MoveNext();
					num++;
				}
				int arg_768_0 = 0;
				int num5 = num - 1;
				for (int l = arg_768_0; l <= num5; l++)
				{
					_Connection arg_798_0 = this.con;
					string arg_798_1 = "select * from remedies where RemId=" + Conversions.ToString(array[l]) + " ";
					value = Missing.Value;
					this.rst = arg_798_0.Execute(arg_798_1, out value, -1);
					while (!this.rst.BOF & !this.rst.EOF)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
						this.rst.MoveNext();
					}
				}
				streamWriter.Close();
				Process.Start(new ProcessStartInfo
				{
					UseShellExecute = true,
					Verb = "print",
					WindowStyle = ProcessWindowStyle.Hidden,
					FileName = "c:\\materia.doc"
				});
				Cursor.Current = Cursors.Arrow;
			}
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			checked
			{
				if (saveFileDialog.ShowDialog() == DialogResult.OK)
				{
					FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
					StreamWriter streamWriter = new StreamWriter(stream);
					streamWriter.WriteLine("Remedy:" + this.lst_remedies.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Loaded Book:" + MyProject.Forms.frm_Main.Label9.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Symptoms:-");
					streamWriter.WriteLine(this.Label2.Text);
					streamWriter.WriteLine("                                ");
					Cursor.Current = Cursors.WaitCursor;
					int arg_E4_0 = 1;
					int num = this.ListBox1.Items.Count - 1;
					for (int i = arg_E4_0; i <= num; i++)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.ListBox1.Items[i - 1]));
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Poly Status:-");
					streamWriter.WriteLine(this.Label9.Text);
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Remedy Family");
					streamWriter.WriteLine(this.Label4.Text);
					streamWriter.WriteLine("                                ");
					int arg_198_0 = 1;
					int num2 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_198_0; i <= num2; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 0;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Anti Doted remedies");
					streamWriter.WriteLine(this.Label5.Text);
					streamWriter.WriteLine("                                ");
					int arg_243_0 = 1;
					int num3 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_243_0; i <= num3; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 1;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Complementary remedies");
					streamWriter.WriteLine(this.Label6.Text);
					streamWriter.WriteLine("                                ");
					int arg_2EE_0 = 1;
					int num4 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_2EE_0; i <= num4; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 2;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Incompatible remedies");
					streamWriter.WriteLine(this.Label7.Text);
					streamWriter.WriteLine("                                ");
					int arg_399_0 = 1;
					int num5 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_399_0; i <= num5; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 3;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.Close();
					Cursor.Current = Cursors.Arrow;
				}
			}
		}

		private void Back_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Button4_Click(object sender, EventArgs e)
		{
			checked
			{
				if (Operators.ConditionalCompareObjectNotEqual(this.lst_remedies.SelectedItem, "", false))
				{
					object objectValue = RuntimeHelpers.GetObjectValue(Interaction.CreateObject("Excel.Application", ""));
					object objectValue2 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(NewLateBinding.LateGet(objectValue, null, "Workbooks", new object[0], null, null, null), null, "Add", new object[0], null, null, null));
					object objectValue3 = RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(objectValue2, null, "Worksheets", new object[]
					{
						1
					}, null, null, null));
					this.Cursor = Cursors.WaitCursor;
					SaveFileDialog saveFileDialog = this.SaveFileDialog1;
					saveFileDialog.Filter = "Excel Files (*.xls)|*.xls|All files|*.*";
					if (saveFileDialog.ShowDialog() == DialogResult.OK)
					{
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"b2"
						}, null, null, null), null, "Value", new object[]
						{
							"Remedy Name:"
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"c2"
						}, null, null, null), null, "Value", new object[]
						{
							RuntimeHelpers.GetObjectValue(this.lst_remedies.SelectedItem)
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"b3"
						}, null, null, null), null, "Value", new object[]
						{
							"Total Symptoms:"
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"c3"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label2.Text
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"b4"
						}, null, null, null), null, "Value", new object[]
						{
							"Loaded Book:"
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"D3"
						}, null, null, null), null, "Value", new object[]
						{
							MyProject.Forms.frm_Main.Label9.Text
						}, null, null, false, true);
						int arg_281_0 = 1;
						int num = this.ListBox1.Items.Count - 1;
						for (int i = arg_281_0; i <= num; i++)
						{
							NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
							{
								"b" + Conversions.ToString(i + 5)
							}, null, null, null), null, "Value", new object[]
							{
								RuntimeHelpers.GetObjectValue(this.ListBox1.Items[i - 1])
							}, null, null, false, true);
						}
						int arg_301_0 = 0;
						int num2 = this.MSFlexGrid2.Rows - 1;
						for (int i = arg_301_0; i <= num2; i++)
						{
							this.MSFlexGrid2.Row = i;
							this.MSFlexGrid2.Col = 0;
							NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
							{
								"e" + Conversions.ToString(i + 5)
							}, null, null, null), null, "Value", new object[]
							{
								this.MSFlexGrid2.Text
							}, null, null, false, true);
							this.MSFlexGrid2.Col = 1;
							NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
							{
								"f" + Conversions.ToString(i + 5)
							}, null, null, null), null, "Value", new object[]
							{
								this.MSFlexGrid2.Text
							}, null, null, false, true);
							this.MSFlexGrid2.Col = 2;
							NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
							{
								"g" + Conversions.ToString(i + 5)
							}, null, null, null), null, "Value", new object[]
							{
								this.MSFlexGrid2.Text
							}, null, null, false, true);
							this.MSFlexGrid2.Col = 3;
							NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
							{
								"h" + Conversions.ToString(i + 5)
							}, null, null, null), null, "Value", new object[]
							{
								this.MSFlexGrid2.Text
							}, null, null, false, true);
						}
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"e1"
						}, null, null, null), null, "Value", new object[]
						{
							"Poly Status:"
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"g1"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label9.Text
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"e3"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label4.Text
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"f3"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label5.Text
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"g3"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label6.Text
						}, null, null, false, true);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(objectValue3, null, "Range", new object[]
						{
							"h3"
						}, null, null, null), null, "Value", new object[]
						{
							this.Label7.Text
						}, null, null, false, true);
						object arg_697_0 = objectValue2;
						Type arg_697_1 = null;
						string arg_697_2 = "SaveAs";
						object[] array = new object[1];
						object[] arg_67E_0 = array;
						int arg_67E_1 = 0;
						SaveFileDialog saveFileDialog2 = saveFileDialog;
						arg_67E_0[arg_67E_1] = saveFileDialog2.FileName;
						object[] array2 = array;
						object[] arg_697_3 = array2;
						string[] arg_697_4 = null;
						Type[] arg_697_5 = null;
						bool[] array3 = new bool[]
						{
							true
						};
						NewLateBinding.LateCall(arg_697_0, arg_697_1, arg_697_2, arg_697_3, arg_697_4, arg_697_5, array3, true);
						if (array3[0])
						{
							saveFileDialog2.FileName = (string)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(array2[0]), typeof(string));
						}
						this.Cursor = Cursors.Arrow;
					}
					NewLateBinding.LateCall(objectValue, null, "Quit", new object[0], null, null, null, true);
				}
				else
				{
					Interaction.MsgBox("Please First select Remedy", MsgBoxStyle.Information, "Aziz Urdu Homeopath Soft");
				}
			}
		}

		public void sym_load()
		{
			checked
			{
				if (Operators.ConditionalCompareObjectNotEqual(this.lst_remedies.SelectedItem, "", false))
				{
					_Connection arg_4F_0 = this.con;
					string arg_4F_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select RemId,Poly from Remedies where RemName='", this.lst_remedies.SelectedItem), "'"));
					object value = Missing.Value;
					this.rst = arg_4F_0.Execute(arg_4F_1, out value, -1);
					int value2 = Conversions.ToInteger(this.rst.Fields["RemId"].Value);
					this.ListBox1.Items.Clear();
					int num = 1;
					_Connection arg_B0_0 = this.con;
					string arg_B0_1 = "SELECT  RemSymptoms.Weight, Symptoms.SymName,Symptoms.Symurdu FROM Symptoms INNER JOIN (Remedies INNER JOIN RemSymptoms ON Remedies.[RemId] = RemSymptoms.[RemId]) ON Symptoms.[SymId] = RemSymptoms.[SymId] where Remedies.RemId=" + Conversions.ToString(value2) + " and Symptoms.symid";
					value = Missing.Value;
					this.rst = arg_B0_0.Execute(arg_B0_1, out value, -1);
					if (this.R1.Checked)
					{
						while (!this.rst.EOF & !this.rst.BOF)
						{
							string text = Conversions.ToString(this.rst.Fields["SymName"].Value);
							this.ListBox1.Items.Add(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("[", this.rst.Fields["Weight"].Value), "]  "), text));
							num++;
							this.rst.MoveNext();
						}
					}
					else if (this.R2.Checked)
					{
						while (!this.rst.EOF & !this.rst.BOF)
						{
							string text = Conversions.ToString(this.rst.Fields["Symurdu"].Value);
							this.ListBox1.Items.Add(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("[", this.rst.Fields["Weight"].Value), "]  "), text));
							num++;
							this.rst.MoveNext();
						}
					}
					else
					{
						while (!this.rst.EOF & !this.rst.BOF)
						{
							string text = Conversions.ToString(this.rst.Fields["Symname"].Value);
							this.ListBox1.Items.Add(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("[", this.rst.Fields["Weight"].Value), "]  "), text));
							text = Conversions.ToString(this.rst.Fields["Symurdu"].Value);
							this.ListBox1.Items.Add(Strings.Space(10) + text);
							num++;
							this.rst.MoveNext();
						}
					}
					if (num - 1 == 0)
					{
						this.ListBox1.Items.Add("No Record Found");
					}
					this.Label2.Text = Conversions.ToString(num - 1);
				}
			}
		}

		private void R1_Click(object sender, EventArgs e)
		{
			this.sym_load();
		}

		private void R2_Click(object sender, EventArgs e)
		{
			this.sym_load();
		}

		private void R3_Click(object sender, EventArgs e)
		{
			this.sym_load();
		}

		private void R1_CheckedChanged(object sender, EventArgs e)
		{
			if (this.R1.Enabled)
			{
				this.ListBox1.Font = new Font("Tahoma", 8f, FontStyle.Bold, GraphicsUnit.Point, 0);
				this.ListBox1.ForeColor = Color.BurlyWood;
			}
		}

		private void R2_CheckedChanged(object sender, EventArgs e)
		{
			if (this.R2.Enabled)
			{
				this.ListBox1.Font = new Font("Nafees Nastaleeq", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
				this.ListBox1.RightToLeft = RightToLeft.Yes;
				this.ListBox1.ForeColor = Color.ForestGreen;
			}
			else
			{
				this.ListBox1.RightToLeft = RightToLeft.No;
			}
		}

		private void R3_CheckedChanged(object sender, EventArgs e)
		{
			if (this.R3.Enabled)
			{
				this.ListBox1.Font = new Font("Nafees Nastaleeq", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
				this.ListBox1.RightToLeft = RightToLeft.No;
				this.ListBox1.ForeColor = Color.BlueViolet;
			}
		}

		private void ToolStripButton3_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			checked
			{
				if (saveFileDialog.ShowDialog() == DialogResult.OK)
				{
					FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
					StreamWriter streamWriter = new StreamWriter(stream);
					streamWriter.WriteLine("Remedy:" + this.lst_remedies.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Loaded Book:" + MyProject.Forms.frm_Main.Label9.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Symptoms:-");
					streamWriter.WriteLine(this.Label2.Text);
					streamWriter.WriteLine("                                ");
					Cursor.Current = Cursors.WaitCursor;
					int arg_E4_0 = 1;
					int num = this.ListBox1.Items.Count - 1;
					for (int i = arg_E4_0; i <= num; i++)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.ListBox1.Items[i - 1]));
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Poly Status:-");
					streamWriter.WriteLine(this.Label9.Text);
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Remedy Family");
					streamWriter.WriteLine(this.Label4.Text);
					streamWriter.WriteLine("                                ");
					int arg_198_0 = 1;
					int num2 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_198_0; i <= num2; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 0;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Anti Doted remedies");
					streamWriter.WriteLine(this.Label5.Text);
					streamWriter.WriteLine("                                ");
					int arg_243_0 = 1;
					int num3 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_243_0; i <= num3; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 1;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Complementary remedies");
					streamWriter.WriteLine(this.Label6.Text);
					streamWriter.WriteLine("                                ");
					int arg_2EE_0 = 1;
					int num4 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_2EE_0; i <= num4; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 2;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Incompatible remedies");
					streamWriter.WriteLine(this.Label7.Text);
					streamWriter.WriteLine("                                ");
					int arg_399_0 = 1;
					int num5 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_399_0; i <= num5; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 3;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.Close();
					Cursor.Current = Cursors.Arrow;
				}
			}
		}

		private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			checked
			{
				if (saveFileDialog.ShowDialog() == DialogResult.OK)
				{
					FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
					StreamWriter streamWriter = new StreamWriter(stream);
					streamWriter.WriteLine("Remedy:" + this.lst_remedies.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Loaded Book:" + MyProject.Forms.frm_Main.Label9.Text);
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Symptoms:-");
					streamWriter.WriteLine(this.Label2.Text);
					streamWriter.WriteLine("                                ");
					Cursor.Current = Cursors.WaitCursor;
					int arg_E4_0 = 1;
					int num = this.ListBox1.Items.Count - 1;
					for (int i = arg_E4_0; i <= num; i++)
					{
						streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.ListBox1.Items[i - 1]));
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Poly Status:-");
					streamWriter.WriteLine(this.Label9.Text);
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Remedy Family");
					streamWriter.WriteLine(this.Label4.Text);
					streamWriter.WriteLine("                                ");
					int arg_198_0 = 1;
					int num2 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_198_0; i <= num2; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 0;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Anti Doted remedies");
					streamWriter.WriteLine(this.Label5.Text);
					streamWriter.WriteLine("                                ");
					int arg_243_0 = 1;
					int num3 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_243_0; i <= num3; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 1;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Complementary remedies");
					streamWriter.WriteLine(this.Label6.Text);
					streamWriter.WriteLine("                                ");
					int arg_2EE_0 = 1;
					int num4 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_2EE_0; i <= num4; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 2;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.WriteLine("--------------------------------");
					streamWriter.WriteLine("                                ");
					streamWriter.WriteLine("Incompatible remedies");
					streamWriter.WriteLine(this.Label7.Text);
					streamWriter.WriteLine("                                ");
					int arg_399_0 = 1;
					int num5 = this.MSFlexGrid2.Rows - 1;
					for (int i = arg_399_0; i <= num5; i++)
					{
						this.MSFlexGrid2.Row = i;
						this.MSFlexGrid2.Col = 3;
						if (Operators.CompareString(this.MSFlexGrid2.Text, "", false) != 0)
						{
							streamWriter.WriteLine(Conversions.ToString(i) + ":   " + this.MSFlexGrid2.Text);
						}
					}
					streamWriter.Close();
					Cursor.Current = Cursors.Arrow;
				}
			}
		}

		private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.ToolTip1.SetToolTip(this.ListBox1, "یہاں پر تلاش کردہ علامات کی کل تعداد بتائی گئ ھے۔ . Total symptoms found in database against a remedy name");
		}

		private void Label12_Click(object sender, EventArgs e)
		{
		}

		private void CheckBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (this.CheckBox1.Checked)
			{
				this.ToolTip1.Active = false;
				this.ToolTip2.Active = false;
				this.ToolTip3.Active = false;
			}
			else
			{
				this.ToolTip1.Active = true;
				this.ToolTip2.Active = true;
				this.ToolTip3.Active = true;
			}
		}
	}
}
