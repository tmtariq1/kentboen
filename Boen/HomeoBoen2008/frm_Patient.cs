using ADODB;
using HomeoBoen2008.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class frm_Patient : Form
	{
		private IContainer components;

		[AccessedThroughProperty("p_name")]
		private TextBox _p_name;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Label4")]
		private Label _Label4;

		[AccessedThroughProperty("age")]
		private TextBox _age;

		[AccessedThroughProperty("address")]
		private TextBox _address;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("sex")]
		private ComboBox _sex;

		[AccessedThroughProperty("cell")]
		private TextBox _cell;

		[AccessedThroughProperty("Phone")]
		private TextBox _Phone;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("Label6")]
		private Label _Label6;

		[AccessedThroughProperty("Label7")]
		private Label _Label7;

		[AccessedThroughProperty("Label9")]
		private Label _Label9;

		[AccessedThroughProperty("email")]
		private TextBox _email;

		[AccessedThroughProperty("comment")]
		private TextBox _comment;

		[AccessedThroughProperty("Label8")]
		private Label _Label8;

		private Connection con;

		private Recordset rst;

		internal virtual TextBox p_name
		{
			[DebuggerNonUserCode]
			get
			{
				return this._p_name;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._p_name = value;
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual Label Label4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label4 = value;
			}
		}

		internal virtual TextBox age
		{
			[DebuggerNonUserCode]
			get
			{
				return this._age;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._age = value;
			}
		}

		internal virtual TextBox address
		{
			[DebuggerNonUserCode]
			get
			{
				return this._address;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._address = value;
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual ComboBox sex
		{
			[DebuggerNonUserCode]
			get
			{
				return this._sex;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._sex = value;
			}
		}

		internal virtual TextBox cell
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cell;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._cell = value;
			}
		}

		internal virtual TextBox Phone
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Phone;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Phone = value;
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual Label Label6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label6 = value;
			}
		}

		internal virtual Label Label7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label7 = value;
			}
		}

		internal virtual Label Label9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label9 = value;
			}
		}

		internal virtual TextBox email
		{
			[DebuggerNonUserCode]
			get
			{
				return this._email;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._email = value;
			}
		}

		internal virtual TextBox comment
		{
			[DebuggerNonUserCode]
			get
			{
				return this._comment;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._comment = value;
			}
		}

		internal virtual Label Label8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label8 = value;
			}
		}

		public frm_Patient()
		{
			base.Load += new EventHandler(this.frm_Patient_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_Patient));
			this.p_name = new TextBox();
			this.Label1 = new Label();
			this.Label2 = new Label();
			this.Label3 = new Label();
			this.Label4 = new Label();
			this.age = new TextBox();
			this.address = new TextBox();
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.sex = new ComboBox();
			this.cell = new TextBox();
			this.Phone = new TextBox();
			this.Label5 = new Label();
			this.Label6 = new Label();
			this.Label7 = new Label();
			this.Label9 = new Label();
			this.email = new TextBox();
			this.comment = new TextBox();
			this.Label8 = new Label();
			this.SuspendLayout();
			this.p_name.ForeColor = Color.Teal;
			Control arg_109_0 = this.p_name;
			Point location = new Point(99, 27);
			arg_109_0.Location = location;
			this.p_name.Name = "p_name";
			Control arg_133_0 = this.p_name;
			Size size = new Size(371, 20);
			arg_133_0.Size = size;
			this.p_name.TabIndex = 0;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_17F_0 = this.Label1;
			location = new Point(21, 31);
			arg_17F_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_1A6_0 = this.Label1;
			size = new Size(43, 13);
			arg_1A6_0.Size = size;
			this.Label1.TabIndex = 1;
			this.Label1.Text = "Name:";
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_205_0 = this.Label2;
			location = new Point(534, 31);
			arg_205_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_22C_0 = this.Label2;
			size = new Size(33, 13);
			arg_22C_0.Size = size;
			this.Label2.TabIndex = 2;
			this.Label2.Text = "Age:";
			this.Label3.AutoSize = true;
			this.Label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_28B_0 = this.Label3;
			location = new Point(532, 66);
			arg_28B_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_2B2_0 = this.Label3;
			size = new Size(32, 13);
			arg_2B2_0.Size = size;
			this.Label3.TabIndex = 3;
			this.Label3.Text = "Sex:";
			this.Label4.AutoSize = true;
			this.Label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_30E_0 = this.Label4;
			location = new Point(20, 66);
			arg_30E_0.Location = location;
			this.Label4.Name = "Label4";
			Control arg_335_0 = this.Label4;
			size = new Size(56, 13);
			arg_335_0.Size = size;
			this.Label4.TabIndex = 4;
			this.Label4.Text = "Address:";
			Control arg_36B_0 = this.age;
			location = new Point(585, 27);
			arg_36B_0.Location = location;
			this.age.Name = "age";
			Control arg_392_0 = this.age;
			size = new Size(100, 20);
			arg_392_0.Size = size;
			this.age.TabIndex = 1;
			this.address.ForeColor = Color.Navy;
			Control arg_3C5_0 = this.address;
			location = new Point(99, 62);
			arg_3C5_0.Location = location;
			this.address.Name = "address";
			Control arg_3EF_0 = this.address;
			size = new Size(371, 20);
			arg_3EF_0.Size = size;
			this.address.TabIndex = 2;
			this.Button1.BackgroundImage = (Image)componentResourceManager.GetObject("Button1.BackgroundImage");
			this.Button1.Image = (Image)componentResourceManager.GetObject("Button1.Image");
			Control arg_44B_0 = this.Button1;
			location = new Point(110, 378);
			arg_44B_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_472_0 = this.Button1;
			size = new Size(75, 60);
			arg_472_0.Size = size;
			this.Button1.TabIndex = 8;
			this.Button1.UseVisualStyleBackColor = true;
			this.Button2.BackgroundImage = (Image)componentResourceManager.GetObject("Button2.BackgroundImage");
			this.Button2.Image = (Image)componentResourceManager.GetObject("Button2.Image");
			Control arg_4DD_0 = this.Button2;
			location = new Point(191, 378);
			arg_4DD_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_504_0 = this.Button2;
			size = new Size(75, 60);
			arg_504_0.Size = size;
			this.Button2.TabIndex = 9;
			this.Button2.UseVisualStyleBackColor = true;
			this.sex.FormattingEnabled = true;
			this.sex.Items.AddRange(new object[]
			{
				"Male",
				"Female",
				"Child"
			});
			Control arg_573_0 = this.sex;
			location = new Point(585, 61);
			arg_573_0.Location = location;
			this.sex.Name = "sex";
			Control arg_59A_0 = this.sex;
			size = new Size(100, 21);
			arg_59A_0.Size = size;
			this.sex.TabIndex = 3;
			this.cell.ForeColor = Color.FromArgb(255, 192, 128);
			Control arg_5DC_0 = this.cell;
			location = new Point(99, 97);
			arg_5DC_0.Location = location;
			this.cell.Name = "cell";
			Control arg_603_0 = this.cell;
			size = new Size(112, 20);
			arg_603_0.Size = size;
			this.cell.TabIndex = 4;
			Control arg_629_0 = this.Phone;
			location = new Point(585, 97);
			arg_629_0.Location = location;
			this.Phone.Name = "Phone";
			Control arg_650_0 = this.Phone;
			size = new Size(100, 20);
			arg_650_0.Size = size;
			this.Phone.TabIndex = 5;
			this.Label5.AutoSize = true;
			this.Label5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_69C_0 = this.Label5;
			location = new Point(19, 101);
			arg_69C_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_6C3_0 = this.Label5;
			size = new Size(28, 13);
			arg_6C3_0.Size = size;
			this.Label5.TabIndex = 13;
			this.Label5.Text = "Cell";
			this.Label6.AutoSize = true;
			this.Label6.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_723_0 = this.Label6;
			location = new Point(532, 101);
			arg_723_0.Location = location;
			this.Label6.Name = "Label6";
			Control arg_74A_0 = this.Label6;
			size = new Size(43, 13);
			arg_74A_0.Size = size;
			this.Label6.TabIndex = 14;
			this.Label6.Text = "Phone";
			this.Label7.AutoSize = true;
			this.Label7.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_7AA_0 = this.Label7;
			location = new Point(20, 136);
			arg_7AA_0.Location = location;
			this.Label7.Name = "Label7";
			Control arg_7D1_0 = this.Label7;
			size = new Size(41, 13);
			arg_7D1_0.Size = size;
			this.Label7.TabIndex = 15;
			this.Label7.Text = "Email:";
			this.Label9.AutoSize = true;
			this.Label9.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_831_0 = this.Label9;
			location = new Point(19, 171);
			arg_831_0.Location = location;
			this.Label9.Name = "Label9";
			Control arg_858_0 = this.Label9;
			size = new Size(56, 15);
			arg_858_0.Size = size;
			this.Label9.TabIndex = 17;
			this.Label9.Text = "Notes ::";
			this.email.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_8A6_0 = this.email;
			location = new Point(99, 132);
			arg_8A6_0.Location = location;
			this.email.Name = "email";
			Control arg_8D0_0 = this.email;
			size = new Size(371, 20);
			arg_8D0_0.Size = size;
			this.email.TabIndex = 6;
			Control arg_8F6_0 = this.comment;
			location = new Point(99, 167);
			arg_8F6_0.Location = location;
			this.comment.Multiline = true;
			this.comment.Name = "comment";
			this.comment.ScrollBars = ScrollBars.Both;
			Control arg_93B_0 = this.comment;
			size = new Size(604, 205);
			arg_93B_0.Size = size;
			this.comment.TabIndex = 7;
			this.Label8.AutoSize = true;
			Control arg_970_0 = this.Label8;
			location = new Point(506, 132);
			arg_970_0.Location = location;
			this.Label8.Name = "Label8";
			Control arg_997_0 = this.Label8;
			size = new Size(39, 13);
			arg_997_0.Size = size;
			this.Label8.TabIndex = 18;
			this.Label8.Text = "Label8";
			this.Label8.Visible = false;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(715, 484);
			this.ClientSize = size;
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.comment);
			this.Controls.Add(this.email);
			this.Controls.Add(this.Label9);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Phone);
			this.Controls.Add(this.cell);
			this.Controls.Add(this.sex);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.address);
			this.Controls.Add(this.age);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.p_name);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "frm_Patient";
			this.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Patient Management System Basic :::  P H R :: Pak Homeopathic Repertory  2008 Ver 1.0 ";
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			if (Conversion.Val(this.Label8.Text) == 0.0)
			{
				_Connection arg_33_0 = this.con;
				string arg_33_1 = "select Persid from Person";
				object value = Missing.Value;
				this.rst = arg_33_0.Execute(arg_33_1, out value, -1);
				int value2 = 0;
				while (!this.rst.EOF & !this.rst.BOF)
				{
					value2 = checked((int)Math.Round(unchecked(Conversion.Val(RuntimeHelpers.GetObjectValue(this.rst.Fields["Persid"].Value)) + 1.0)));
					this.rst.MoveNext();
				}
				MyProject.Forms.frm_Main.Label6.Text = Conversions.ToString(value2);
				MyProject.Forms.frm_Main.Label7.Text = DateAndTime.DateString;
				if (Operators.CompareString(this.p_name.Text, "", false) != 0 & Operators.CompareString(this.age.Text, "", false) != 0 & Operators.CompareString(this.address.Text, "", false) != 0 & Operators.CompareString(this.cell.Text, "", false) != 0 & Operators.CompareString(this.Phone.Text, "", false) != 0 & Operators.CompareString(this.email.Text, "", false) != 0 & Operators.CompareString(this.comment.Text, "", false) != 0)
				{
					_Connection arg_2A2_0 = this.con;
					string arg_2A2_1 = string.Concat(new string[]
					{
						"insert into [Person](persID,Name1,City,Mobile,phoneHome,E_mail,DateBirth,comment,date1,sex) values(",
						Conversions.ToString(value2),
						",'",
						this.p_name.Text,
						"','",
						this.address.Text,
						"','",
						this.cell.Text,
						"','",
						this.Phone.Text,
						"' ,'",
						this.email.Text,
						"','",
						this.age.Text,
						"' , '",
						this.comment.Text,
						"','",
						DateAndTime.DateString,
						"','",
						this.sex.Text,
						"')"
					});
					value = Missing.Value;
					arg_2A2_0.Execute(arg_2A2_1, out value, -1);
					this.p_name.Text = "";
					this.age.Text = "";
					this.address.Text = "";
					this.cell.Text = "";
					this.Phone.Text = "";
					this.email.Text = "";
					this.comment.Text = "";
					MyProject.Forms.frm_Main.Label5.Text = this.p_name.Text;
					this.Close();
				}
				else
				{
					Interaction.MsgBox("Plz fill the require fields", MsgBoxStyle.Information, null);
				}
			}
			else
			{
				_Connection arg_454_0 = this.con;
				string arg_454_1 = string.Concat(new string[]
				{
					"update person set name1='",
					this.p_name.Text,
					"',city='",
					this.address.Text,
					"',mobile='",
					this.cell.Text,
					"',phoneHome='",
					this.Phone.Text,
					"',E_mail='",
					this.email.Text,
					"',DateBirth='",
					this.age.Text,
					"',comment='",
					this.comment.Text,
					"',sex='",
					this.sex.Text,
					"'  where persid=",
					Conversions.ToString(Conversion.Val(this.Label8.Text)),
					""
				});
				object value = Missing.Value;
				arg_454_0.Execute(arg_454_1, out value, -1);
				this.Close();
			}
		}

		public void Connect_Database()
		{
			string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void frm_Patient_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
			if (Conversion.Val(this.Label8.Text) != Conversions.ToDouble("0"))
			{
				_Connection arg_59_0 = this.con;
				string arg_59_1 = "select * from person where persid=" + Conversions.ToString(Conversion.Val(this.Label8.Text)) + "";
				object value = Missing.Value;
				this.rst = arg_59_0.Execute(arg_59_1, out value, -1);
				this.p_name.Text = Conversions.ToString(this.rst.Fields["name1"].Value);
				this.address.Text = Conversions.ToString(this.rst.Fields["city"].Value);
				this.cell.Text = Conversions.ToString(this.rst.Fields["mobile"].Value);
				this.email.Text = Conversions.ToString(this.rst.Fields["E_mail"].Value);
				this.comment.Text = Conversions.ToString(this.rst.Fields["Comment"].Value);
				this.age.Text = Conversions.ToString(this.rst.Fields["DateBirth"].Value);
				this.sex.Text = Conversions.ToString(this.rst.Fields["sex"].Value);
				this.Phone.Text = Conversions.ToString(this.rst.Fields["PhoneHome"].Value);
			}
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
