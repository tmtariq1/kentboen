using ADODB;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class frm_Login : Form
	{
		[AccessedThroughProperty("LogoPictureBox")]
		private PictureBox _LogoPictureBox;

		[AccessedThroughProperty("UsernameLabel")]
		private Label _UsernameLabel;

		[AccessedThroughProperty("PasswordLabel")]
		private Label _PasswordLabel;

		[AccessedThroughProperty("Username")]
		private TextBox _Username;

		[AccessedThroughProperty("Password")]
		private TextBox _Password;

		[AccessedThroughProperty("OK")]
		private Button _OK;

		[AccessedThroughProperty("Cancel")]
		private Button _Cancel;

		private IContainer components;

		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		private Connection con;

		private Recordset rst;

		internal virtual PictureBox LogoPictureBox
		{
			[DebuggerNonUserCode]
			get
			{
				return this._LogoPictureBox;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._LogoPictureBox = value;
			}
		}

		internal virtual Label UsernameLabel
		{
			[DebuggerNonUserCode]
			get
			{
				return this._UsernameLabel;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._UsernameLabel = value;
			}
		}

		internal virtual Label PasswordLabel
		{
			[DebuggerNonUserCode]
			get
			{
				return this._PasswordLabel;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._PasswordLabel = value;
			}
		}

		internal virtual TextBox Username
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Username;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Username = value;
			}
		}

		internal virtual TextBox Password
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Password;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Password = value;
			}
		}

		internal virtual Button OK
		{
			[DebuggerNonUserCode]
			get
			{
				return this._OK;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._OK != null)
				{
					this._OK.Click -= new EventHandler(this.OK_Click);
				}
				this._OK = value;
				if (this._OK != null)
				{
					this._OK.Click += new EventHandler(this.OK_Click);
				}
			}
		}

		internal virtual Button Cancel
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Cancel;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Cancel != null)
				{
					this._Cancel.Click -= new EventHandler(this.Cancel_Click);
				}
				this._Cancel = value;
				if (this._Cancel != null)
				{
					this._Cancel.Click += new EventHandler(this.Cancel_Click);
				}
			}
		}

		internal virtual ToolTip ToolTip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip1 = value;
			}
		}

		public frm_Login()
		{
			base.Load += new EventHandler(this.frm_Login_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_Login));
			this.LogoPictureBox = new PictureBox();
			this.UsernameLabel = new Label();
			this.PasswordLabel = new Label();
			this.Username = new TextBox();
			this.Password = new TextBox();
			this.OK = new Button();
			this.Cancel = new Button();
			this.ToolTip1 = new ToolTip(this.components);
			((ISupportInitialize)this.LogoPictureBox).BeginInit();
			this.SuspendLayout();
			this.LogoPictureBox.BorderStyle = BorderStyle.FixedSingle;
			this.LogoPictureBox.Image = (Image)componentResourceManager.GetObject("LogoPictureBox.Image");
			Control arg_C3_0 = this.LogoPictureBox;
			Point location = new Point(12, 12);
			arg_C3_0.Location = location;
			this.LogoPictureBox.Name = "LogoPictureBox";
			Control arg_ED_0 = this.LogoPictureBox;
			Size size = new Size(121, 171);
			arg_ED_0.Size = size;
			this.LogoPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
			this.LogoPictureBox.TabIndex = 0;
			this.LogoPictureBox.TabStop = false;
			this.ToolTip1.SetToolTip(this.LogoPictureBox, "Here User Photo will be displayed while you have attached your photo using doctor detail utility.");
			this.UsernameLabel.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_15E_0 = this.UsernameLabel;
			location = new Point(152, 19);
			arg_15E_0.Location = location;
			this.UsernameLabel.Name = "UsernameLabel";
			Control arg_185_0 = this.UsernameLabel;
			size = new Size(67, 23);
			arg_185_0.Size = size;
			this.UsernameLabel.TabIndex = 0;
			this.UsernameLabel.Text = "&User name";
			this.UsernameLabel.TextAlign = ContentAlignment.MiddleLeft;
			this.PasswordLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_1E5_0 = this.PasswordLabel;
			location = new Point(152, 68);
			arg_1E5_0.Location = location;
			this.PasswordLabel.Name = "PasswordLabel";
			Control arg_20C_0 = this.PasswordLabel;
			size = new Size(89, 23);
			arg_20C_0.Size = size;
			this.PasswordLabel.TabIndex = 2;
			this.PasswordLabel.Text = "&Password";
			this.PasswordLabel.TextAlign = ContentAlignment.MiddleLeft;
			Control arg_24F_0 = this.Username;
			location = new Point(156, 45);
			arg_24F_0.Location = location;
			this.Username.Name = "Username";
			Control arg_279_0 = this.Username;
			size = new Size(179, 20);
			arg_279_0.Size = size;
			this.Username.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.Username, "Please type here your login name which has been provided by us");
			Control arg_2B5_0 = this.Password;
			location = new Point(156, 94);
			arg_2B5_0.Location = location;
			this.Password.Name = "Password";
			this.Password.PasswordChar = '|';
			Control arg_2EC_0 = this.Password;
			size = new Size(179, 20);
			arg_2EC_0.Size = size;
			this.Password.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.Password, "Please type here your first password which has been provided to you by our comapny.");
			this.OK.BackColor = Color.Transparent;
			this.OK.BackgroundImage = (Image)componentResourceManager.GetObject("OK.BackgroundImage");
			this.OK.Image = (Image)componentResourceManager.GetObject("OK.Image");
			Control arg_371_0 = this.OK;
			location = new Point(154, 138);
			arg_371_0.Location = location;
			this.OK.Name = "OK";
			Control arg_398_0 = this.OK;
			size = new Size(75, 60);
			arg_398_0.Size = size;
			this.OK.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.OK, "Click on this button if you have typed your Login Name and Login Password ,");
			this.OK.UseVisualStyleBackColor = false;
			this.Cancel.BackColor = Color.Transparent;
			this.Cancel.BackgroundImage = (Image)componentResourceManager.GetObject("Cancel.BackgroundImage");
			this.Cancel.DialogResult = DialogResult.Cancel;
			this.Cancel.Image = (Image)componentResourceManager.GetObject("Cancel.Image");
			Control arg_435_0 = this.Cancel;
			location = new Point(229, 138);
			arg_435_0.Location = location;
			this.Cancel.Name = "Cancel";
			Control arg_45C_0 = this.Cancel;
			size = new Size(75, 60);
			arg_45C_0.Size = size;
			this.Cancel.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.Cancel, "If you want to cancel to Login to software ,cancel from here.");
			this.Cancel.UseVisualStyleBackColor = false;
			this.ToolTip1.AutoPopDelay = 5000;
			this.ToolTip1.BackColor = Color.AntiqueWhite;
			this.ToolTip1.ForeColor = Color.FromArgb(192, 0, 0);
			this.ToolTip1.InitialDelay = 0;
			this.ToolTip1.IsBalloon = true;
			this.ToolTip1.ReshowDelay = 100;
			this.ToolTip1.ToolTipIcon = ToolTipIcon.Info;
			this.ToolTip1.ToolTipTitle = "Login Help";
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			this.BackColor = Color.Khaki;
			size = new Size(454, 210);
			this.ClientSize = size;
			this.Controls.Add(this.Cancel);
			this.Controls.Add(this.OK);
			this.Controls.Add(this.Password);
			this.Controls.Add(this.Username);
			this.Controls.Add(this.PasswordLabel);
			this.Controls.Add(this.UsernameLabel);
			this.Controls.Add(this.LogoPictureBox);
			this.FormBorderStyle = FormBorderStyle.FixedDialog;
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frm_Login";
			this.SizeGripStyle = SizeGripStyle.Hide;
			this.StartPosition = FormStartPosition.CenterParent;
			this.Text = "LOGIN BOENNINGHAUSEN  HOMEOPATHIC REPERTORY";
			((ISupportInitialize)this.LogoPictureBox).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void OK_Click(object sender, EventArgs e)
		{
			_Connection arg_51_0 = this.con;
			string arg_51_1 = string.Concat(new string[]
			{
				"select * from pass where u_name='",
				this.Username.Text,
				"' and u_pass='",
				this.Password.Text,
				"' "
			});
			object value = Missing.Value;
			this.rst = arg_51_0.Execute(arg_51_1, out value, -1);
			if (!this.rst.EOF & !this.rst.BOF)
			{
				frm_Main frm_Main = new frm_Main();
				frm_Main.Show();
				this.Hide();
			}
			else
			{
				Interaction.MsgBox("UserName or Password is invalid", MsgBoxStyle.OkOnly, null);
				this.Username.Text = "";
				this.Password.Text = "";
			}
		}

		private void Cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		public void Connect_Database()
		{
            string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void frm_Login_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
			_Connection arg_1B_0 = this.con;
			string arg_1B_1 = "select pic from doctor where id=1";
			object value = Missing.Value;
			this.rst = arg_1B_0.Execute(arg_1B_1, out value, -1);
			this.LogoPictureBox.ImageLocation = Conversions.ToString(this.rst.Fields["pic"].Value);
		}
	}
}
