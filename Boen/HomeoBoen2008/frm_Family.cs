using ADODB;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class frm_Family : Form
	{
		private IContainer components;

		[AccessedThroughProperty("SaveFileDialog1")]
		private SaveFileDialog _SaveFileDialog1;

		[AccessedThroughProperty("Panel1")]
		private Panel _Panel1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Back")]
		private Button _Back;

		[AccessedThroughProperty("cmd_report")]
		private Button _cmd_report;

		[AccessedThroughProperty("PrintDialog1")]
		private PrintDialog _PrintDialog1;

		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		[AccessedThroughProperty("TreeView1")]
		private TreeView _TreeView1;

		[AccessedThroughProperty("ListBox1")]
		private ListBox _ListBox1;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		private Connection con;

		private Recordset rst;

		private Recordset rst1;

		private Recordset rst2;

		private Recordset rst3;

		internal virtual SaveFileDialog SaveFileDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._SaveFileDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._SaveFileDialog1 = value;
			}
		}

		internal virtual Panel Panel1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Panel1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Panel1 = value;
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Back
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Back;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Back != null)
				{
					this._Back.Click -= new EventHandler(this.Back_Click);
				}
				this._Back = value;
				if (this._Back != null)
				{
					this._Back.Click += new EventHandler(this.Back_Click);
				}
			}
		}

		internal virtual Button cmd_report
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cmd_report;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._cmd_report != null)
				{
					this._cmd_report.Click -= new EventHandler(this.cmd_report_Click);
				}
				this._cmd_report = value;
				if (this._cmd_report != null)
				{
					this._cmd_report.Click += new EventHandler(this.cmd_report_Click);
				}
			}
		}

		internal virtual PrintDialog PrintDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._PrintDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._PrintDialog1 = value;
			}
		}

		internal virtual ToolTip ToolTip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip1 = value;
			}
		}

		internal virtual TreeView TreeView1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TreeView1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._TreeView1 != null)
				{
					this._TreeView1.AfterSelect -= new TreeViewEventHandler(this.TreeView1_AfterSelect_1);
				}
				this._TreeView1 = value;
				if (this._TreeView1 != null)
				{
					this._TreeView1.AfterSelect += new TreeViewEventHandler(this.TreeView1_AfterSelect_1);
				}
			}
		}

		internal virtual ListBox ListBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ListBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ListBox1 = value;
			}
		}

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		public frm_Family()
		{
			base.Load += new EventHandler(this.Family_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.rst1 = new RecordsetClass();
			this.rst2 = new RecordsetClass();
			this.rst3 = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_Family));
			this.SaveFileDialog1 = new SaveFileDialog();
			this.Panel1 = new Panel();
			this.Button2 = new Button();
			this.Button1 = new Button();
			this.Back = new Button();
			this.cmd_report = new Button();
			this.PrintDialog1 = new PrintDialog();
			this.ToolTip1 = new ToolTip(this.components);
			this.TreeView1 = new TreeView();
			this.ListBox1 = new ListBox();
			this.Label1 = new Label();
			this.Label2 = new Label();
			this.Panel1.SuspendLayout();
			this.SuspendLayout();
			this.Panel1.Controls.Add(this.Button2);
			this.Panel1.Controls.Add(this.Button1);
			this.Panel1.Controls.Add(this.Back);
			this.Panel1.Controls.Add(this.cmd_report);
			Control arg_11F_0 = this.Panel1;
			Point location = new Point(12, 7);
			arg_11F_0.Location = location;
			this.Panel1.Name = "Panel1";
			Control arg_149_0 = this.Panel1;
			Size size = new Size(628, 66);
			arg_149_0.Size = size;
			this.Panel1.TabIndex = 7;
			this.Button2.BackgroundImage = (Image)componentResourceManager.GetObject("Button2.BackgroundImage");
			this.Button2.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button2.ForeColor = Color.Yellow;
			this.Button2.Image = (Image)componentResourceManager.GetObject("Button2.Image");
			Control arg_1D1_0 = this.Button2;
			location = new Point(161, 2);
			arg_1D1_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_1F8_0 = this.Button2;
			size = new Size(75, 60);
			arg_1F8_0.Size = size;
			this.Button2.TabIndex = 2;
			this.Button2.Text = "SAVE";
			this.ToolTip1.SetToolTip(this.Button2, "Save Remedies-Families in Word Doc file , English Only.");
			this.Button2.UseVisualStyleBackColor = true;
			this.Button1.BackgroundImage = (Image)componentResourceManager.GetObject("Button1.BackgroundImage");
			this.Button1.Enabled = false;
			this.Button1.Image = (Image)componentResourceManager.GetObject("Button1.Image");
			Control arg_28E_0 = this.Button1;
			location = new Point(86, 2);
			arg_28E_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_2B5_0 = this.Button1;
			size = new Size(75, 60);
			arg_2B5_0.Size = size;
			this.Button1.TabIndex = 1;
			this.Button1.UseVisualStyleBackColor = true;
			this.Button1.Visible = false;
			this.Back.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Back.ForeColor = Color.Lime;
			this.Back.Image = (Image)componentResourceManager.GetObject("Back.Image");
			Control arg_33A_0 = this.Back;
			location = new Point(536, 2);
			arg_33A_0.Location = location;
			this.Back.Name = "Back";
			Control arg_361_0 = this.Back;
			size = new Size(89, 60);
			arg_361_0.Size = size;
			this.Back.TabIndex = 3;
			this.Back.Text = "CLOSE";
			this.ToolTip1.SetToolTip(this.Back, "Close Remedies - Family Utility");
			this.Back.UseVisualStyleBackColor = true;
			this.cmd_report.BackgroundImage = (Image)componentResourceManager.GetObject("cmd_report.BackgroundImage");
			this.cmd_report.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmd_report.ForeColor = Color.Red;
			this.cmd_report.Image = (Image)componentResourceManager.GetObject("cmd_report.Image");
			Control arg_418_0 = this.cmd_report;
			location = new Point(11, 2);
			arg_418_0.Location = location;
			this.cmd_report.Name = "cmd_report";
			Control arg_43F_0 = this.cmd_report;
			size = new Size(75, 60);
			arg_43F_0.Size = size;
			this.cmd_report.TabIndex = 0;
			this.cmd_report.Text = "PRINT";
			this.ToolTip1.SetToolTip(this.cmd_report, "Take a print of Remedies - Families , Available in English Only");
			this.cmd_report.UseVisualStyleBackColor = true;
			this.PrintDialog1.UseEXDialog = true;
			this.ToolTip1.AutoPopDelay = 10000;
			this.ToolTip1.BackColor = Color.White;
			this.ToolTip1.ForeColor = Color.Navy;
			this.ToolTip1.InitialDelay = 0;
			this.ToolTip1.IsBalloon = true;
			this.ToolTip1.ReshowDelay = 0;
			this.TreeView1.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			Control arg_511_0 = this.TreeView1;
			location = new Point(12, 125);
			arg_511_0.Location = location;
			this.TreeView1.Name = "TreeView1";
			Control arg_53E_0 = this.TreeView1;
			size = new Size(350, 433);
			arg_53E_0.Size = size;
			this.TreeView1.TabIndex = 9;
			this.ListBox1.BorderStyle = BorderStyle.FixedSingle;
			this.ListBox1.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ListBox1.ForeColor = Color.FromArgb(0, 0, 192);
			this.ListBox1.FormattingEnabled = true;
			this.ListBox1.ItemHeight = 15;
			Control arg_5BE_0 = this.ListBox1;
			location = new Point(368, 125);
			arg_5BE_0.Location = location;
			this.ListBox1.Name = "ListBox1";
			Control arg_5EB_0 = this.ListBox1;
			size = new Size(430, 437);
			arg_5EB_0.Size = size;
			this.ListBox1.TabIndex = 10;
			this.Label1.AutoSize = true;
			this.Label1.BackColor = Color.White;
			this.Label1.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label1.ForeColor = Color.FromArgb(192, 0, 0);
			Control arg_662_0 = this.Label1;
			location = new Point(304, 107);
			arg_662_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_688_0 = this.Label1;
			size = new Size(0, 15);
			arg_688_0.Size = size;
			this.Label1.TabIndex = 11;
			this.Label2.AutoSize = true;
			this.Label2.BackColor = Color.White;
			this.Label2.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = Color.FromArgb(0, 192, 0);
			Control arg_6FF_0 = this.Label2;
			location = new Point(304, 80);
			arg_6FF_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_725_0 = this.Label2;
			size = new Size(0, 15);
			arg_725_0.Size = size;
			this.Label2.TabIndex = 12;
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(832, 570);
			this.ClientSize = size;
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.ListBox1);
			this.Controls.Add(this.TreeView1);
			this.Controls.Add(this.Panel1);
			this.FormBorderStyle = FormBorderStyle.FixedSingle;
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.Name = "frm_Family";
			this.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Remedies Families Tree (From Various  Sources)";
			this.Panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Family_Load(object sender, EventArgs e)
		{
			this.Label1.Text = "";
			this.Label2.Text = "";
			this.Connect_Database();
			_Connection arg_3C_0 = this.con;
			string arg_3C_1 = "select * from Families where FamParentId = -1 ";
			object value = Missing.Value;
			this.rst = arg_3C_0.Execute(arg_3C_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				object arg_A2_0 = this.TreeView1.Nodes;
				Type arg_A2_1 = null;
				string arg_A2_2 = "Add";
				object[] array = new object[1];
				object[] arg_8A_0 = array;
				int arg_8A_1 = 0;
				Field field = this.rst.Fields["FamilyName"];
				arg_8A_0[arg_8A_1] = RuntimeHelpers.GetObjectValue(field.Value);
				object[] array2 = array;
				object[] arg_A2_3 = array2;
				string[] arg_A2_4 = null;
				Type[] arg_A2_5 = null;
				bool[] array3 = new bool[]
				{
					true
				};
				object arg_BD_0 = NewLateBinding.LateGet(arg_A2_0, arg_A2_1, arg_A2_2, arg_A2_3, arg_A2_4, arg_A2_5, array3);
				if (array3[0])
				{
					field.Value = RuntimeHelpers.GetObjectValue(array2[0]);
				}
				TreeNode treeNode = (TreeNode)arg_BD_0;
				_Connection arg_107_0 = this.con;
				string arg_107_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select * from Families where FamParentId= ", this.rst.Fields["FamId"].Value), ""));
				value = Missing.Value;
				this.rst1 = arg_107_0.Execute(arg_107_1, out value, -1);
				while (!this.rst1.EOF & !this.rst1.BOF)
				{
					object arg_168_0 = treeNode.Nodes;
					Type arg_168_1 = null;
					string arg_168_2 = "Add";
					array2 = new object[1];
					object[] arg_150_0 = array2;
					int arg_150_1 = 0;
					field = this.rst1.Fields["FamilyName"];
					arg_150_0[arg_150_1] = RuntimeHelpers.GetObjectValue(field.Value);
					array = array2;
					object[] arg_168_3 = array;
					string[] arg_168_4 = null;
					Type[] arg_168_5 = null;
					array3 = new bool[]
					{
						true
					};
					object arg_183_0 = NewLateBinding.LateGet(arg_168_0, arg_168_1, arg_168_2, arg_168_3, arg_168_4, arg_168_5, array3);
					if (array3[0])
					{
						field.Value = RuntimeHelpers.GetObjectValue(array[0]);
					}
					TreeNode treeNode2 = (TreeNode)arg_183_0;
					_Connection arg_1CD_0 = this.con;
					string arg_1CD_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select * from Families where FamParentId= ", this.rst1.Fields["FamId"].Value), ""));
					value = Missing.Value;
					this.rst2 = arg_1CD_0.Execute(arg_1CD_1, out value, -1);
					while (!this.rst2.EOF & !this.rst2.BOF)
					{
						object arg_22E_0 = treeNode2.Nodes;
						Type arg_22E_1 = null;
						string arg_22E_2 = "Add";
						array2 = new object[1];
						object[] arg_216_0 = array2;
						int arg_216_1 = 0;
						field = this.rst2.Fields["FamilyName"];
						arg_216_0[arg_216_1] = RuntimeHelpers.GetObjectValue(field.Value);
						array = array2;
						object[] arg_22E_3 = array;
						string[] arg_22E_4 = null;
						Type[] arg_22E_5 = null;
						array3 = new bool[]
						{
							true
						};
						object arg_249_0 = NewLateBinding.LateGet(arg_22E_0, arg_22E_1, arg_22E_2, arg_22E_3, arg_22E_4, arg_22E_5, array3);
						if (array3[0])
						{
							field.Value = RuntimeHelpers.GetObjectValue(array[0]);
						}
						TreeNode treeNode3 = (TreeNode)arg_249_0;
						_Connection arg_293_0 = this.con;
						string arg_293_1 = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject("select * from Families where FamParentId= ", this.rst2.Fields["FamId"].Value), ""));
						value = Missing.Value;
						this.rst3 = arg_293_0.Execute(arg_293_1, out value, -1);
						while (!this.rst3.EOF & !this.rst3.BOF)
						{
							object arg_2F1_0 = treeNode3.Nodes;
							Type arg_2F1_1 = null;
							string arg_2F1_2 = "Add";
							array2 = new object[1];
							object[] arg_2D9_0 = array2;
							int arg_2D9_1 = 0;
							field = this.rst3.Fields["FamilyName"];
							arg_2D9_0[arg_2D9_1] = RuntimeHelpers.GetObjectValue(field.Value);
							array = array2;
							object[] arg_2F1_3 = array;
							string[] arg_2F1_4 = null;
							Type[] arg_2F1_5 = null;
							array3 = new bool[]
							{
								true
							};
							object arg_30C_0 = NewLateBinding.LateGet(arg_2F1_0, arg_2F1_1, arg_2F1_2, arg_2F1_3, arg_2F1_4, arg_2F1_5, array3);
							if (array3[0])
							{
								field.Value = RuntimeHelpers.GetObjectValue(array[0]);
							}
							TreeNode treeNode4 = (TreeNode)arg_30C_0;
							this.rst3.MoveNext();
						}
						this.rst2.MoveNext();
					}
					this.rst1.MoveNext();
				}
				this.rst.MoveNext();
			}
		}

		public void Connect_Database()
		{
            string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrk.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void create_report()
		{
			FileStream stream = new FileStream("c:\\family.txt" + this.TreeView1.SelectedNode.Text + ".doc", FileMode.Create, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream);
			streamWriter.WriteLine("Remedy Family:" + this.TreeView1.SelectedNode.Text);
			streamWriter.WriteLine("--------------------------------");
			streamWriter.WriteLine("Remedies:-");
			streamWriter.WriteLine("                                ");
			_Connection arg_9E_0 = this.con;
			string arg_9E_1 = "SELECT Families.FamilyName, Remedies.RemName, RemFamilies.RemId FROM Remedies INNER JOIN (Families INNER JOIN RemFamilies ON Families.[FamId] = RemFamilies.[FamId]) ON Remedies.[RemId] = RemFamilies.[RemId] where FamilyName='" + this.TreeView1.SelectedNode.Text + "'";
			object value = Missing.Value;
			this.rst = arg_9E_0.Execute(arg_9E_1, out value, -1);
			while (!this.rst.EOF & !this.rst.BOF)
			{
				streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
				this.rst.MoveNext();
			}
			streamWriter.Close();
		}

		private void cmd_report_Click(object sender, EventArgs e)
		{
			this.create_report();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = this.SaveFileDialog1;
			saveFileDialog.Filter = "Doc Files (*.doc)|*.doc|Text Files(*.txt)|*.txt |All files|*.*";
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
				StreamWriter streamWriter = new StreamWriter(stream);
				streamWriter.WriteLine("Remedy Family:" + this.TreeView1.SelectedNode.Text);
				streamWriter.WriteLine("                                ");
				streamWriter.WriteLine("--------------------------------");
				streamWriter.WriteLine("                                ");
				streamWriter.WriteLine(this.Label2.Text);
				streamWriter.WriteLine("                                ");
				streamWriter.WriteLine("--------------------------------");
				streamWriter.WriteLine("                                ");
				streamWriter.WriteLine("Remedies:-");
				streamWriter.WriteLine("                                ");
				_Connection arg_EB_0 = this.con;
				string arg_EB_1 = "SELECT Families.FamilyName, Remedies.RemName, RemFamilies.RemId FROM Remedies INNER JOIN (Families INNER JOIN RemFamilies ON Families.[FamId] = RemFamilies.[FamId]) ON Remedies.[RemId] = RemFamilies.[RemId] where FamilyName='" + this.TreeView1.SelectedNode.Text + "'";
				object value = Missing.Value;
				this.rst = arg_EB_0.Execute(arg_EB_1, out value, -1);
				while (!this.rst.EOF & !this.rst.BOF)
				{
					streamWriter.WriteLine(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
					this.rst.MoveNext();
				}
				streamWriter.Close();
			}
		}

		private void Back_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void TreeView1_AfterSelect_1(object sender, TreeViewEventArgs e)
		{
			int num = 0;
			this.ListBox1.Items.Clear();
			this.Label1.Text = this.TreeView1.SelectedNode.Text + " Remedies";
			_Connection arg_66_0 = this.con;
			string arg_66_1 = "SELECT Families.FamilyName, Remedies.RemName, RemFamilies.RemId FROM Remedies INNER JOIN (Families INNER JOIN RemFamilies ON Families.[FamId] = RemFamilies.[FamId]) ON Remedies.[RemId] = RemFamilies.[RemId] where FamilyName='" + this.TreeView1.SelectedNode.Text + "'";
			object value = Missing.Value;
			this.rst = arg_66_0.Execute(arg_66_1, out value, -1);
			checked
			{
				while (!this.rst.EOF & !this.rst.BOF)
				{
					this.ListBox1.Items.Add(RuntimeHelpers.GetObjectValue(this.rst.Fields["RemName"].Value));
					num++;
					this.rst.MoveNext();
				}
				this.Label2.Text = "Total Remedies: " + Conversions.ToString(num);
			}
		}
	}
}
