using ADODB;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HomeoBoen2008
{
	[DesignerGenerated]
	public class frm_doctor : Form
	{
		private IContainer components;

		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[AccessedThroughProperty("Label4")]
		private Label _Label4;

		[AccessedThroughProperty("Label5")]
		private Label _Label5;

		[AccessedThroughProperty("Label6")]
		private Label _Label6;

		[AccessedThroughProperty("Label7")]
		private Label _Label7;

		[AccessedThroughProperty("Label8")]
		private Label _Label8;

		[AccessedThroughProperty("Label9")]
		private Label _Label9;

		[AccessedThroughProperty("email")]
		private TextBox _email;

		[AccessedThroughProperty("phone")]
		private TextBox _phone;

		[AccessedThroughProperty("fax")]
		private TextBox _fax;

		[AccessedThroughProperty("c_name")]
		private TextBox _c_name;

		[AccessedThroughProperty("c_address")]
		private TextBox _c_address;

		[AccessedThroughProperty("degree")]
		private TextBox _degree;

		[AccessedThroughProperty("TextBox7")]
		private TextBox _TextBox7;

		[AccessedThroughProperty("R_address")]
		private TextBox _R_address;

		[AccessedThroughProperty("cell")]
		private TextBox _cell;

		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[AccessedThroughProperty("Browse")]
		private Button _Browse;

		[AccessedThroughProperty("PictureBox1")]
		private PictureBox _PictureBox1;

		[AccessedThroughProperty("name1")]
		private TextBox _name1;

		[AccessedThroughProperty("OpenFileDialog1")]
		private OpenFileDialog _OpenFileDialog1;

		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		private Connection con;

		private Recordset rst;

		internal virtual Label Label1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label1 = value;
			}
		}

		internal virtual Label Label2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label2 = value;
			}
		}

		internal virtual Label Label3
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label3;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label3 = value;
			}
		}

		internal virtual Label Label4
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label4;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label4 = value;
			}
		}

		internal virtual Label Label5
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label5;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label5 = value;
			}
		}

		internal virtual Label Label6
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label6;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label6 = value;
			}
		}

		internal virtual Label Label7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label7 = value;
			}
		}

		internal virtual Label Label8
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label8;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label8 = value;
			}
		}

		internal virtual Label Label9
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Label9;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._Label9 = value;
			}
		}

		internal virtual TextBox email
		{
			[DebuggerNonUserCode]
			get
			{
				return this._email;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._email = value;
			}
		}

		internal virtual TextBox phone
		{
			[DebuggerNonUserCode]
			get
			{
				return this._phone;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._phone = value;
			}
		}

		internal virtual TextBox fax
		{
			[DebuggerNonUserCode]
			get
			{
				return this._fax;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._fax = value;
			}
		}

		internal virtual TextBox c_name
		{
			[DebuggerNonUserCode]
			get
			{
				return this._c_name;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._c_name = value;
			}
		}

		internal virtual TextBox c_address
		{
			[DebuggerNonUserCode]
			get
			{
				return this._c_address;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._c_address = value;
			}
		}

		internal virtual TextBox degree
		{
			[DebuggerNonUserCode]
			get
			{
				return this._degree;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._degree = value;
			}
		}

		internal virtual TextBox TextBox7
		{
			[DebuggerNonUserCode]
			get
			{
				return this._TextBox7;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._TextBox7 = value;
			}
		}

		internal virtual TextBox R_address
		{
			[DebuggerNonUserCode]
			get
			{
				return this._R_address;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._R_address = value;
			}
		}

		internal virtual TextBox cell
		{
			[DebuggerNonUserCode]
			get
			{
				return this._cell;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._cell = value;
			}
		}

		internal virtual Button Button1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button1 != null)
				{
					this._Button1.Click -= new EventHandler(this.Button1_Click);
				}
				this._Button1 = value;
				if (this._Button1 != null)
				{
					this._Button1.Click += new EventHandler(this.Button1_Click);
				}
			}
		}

		internal virtual Button Button2
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Button2;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Button2 != null)
				{
					this._Button2.Click -= new EventHandler(this.Button2_Click);
				}
				this._Button2 = value;
				if (this._Button2 != null)
				{
					this._Button2.Click += new EventHandler(this.Button2_Click);
				}
			}
		}

		internal virtual Button Browse
		{
			[DebuggerNonUserCode]
			get
			{
				return this._Browse;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				if (this._Browse != null)
				{
					this._Browse.Click -= new EventHandler(this.Browse_Click);
				}
				this._Browse = value;
				if (this._Browse != null)
				{
					this._Browse.Click += new EventHandler(this.Browse_Click);
				}
			}
		}

		internal virtual PictureBox PictureBox1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._PictureBox1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._PictureBox1 = value;
			}
		}

		internal virtual TextBox name1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._name1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._name1 = value;
			}
		}

		internal virtual OpenFileDialog OpenFileDialog1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._OpenFileDialog1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._OpenFileDialog1 = value;
			}
		}

		internal virtual ToolTip ToolTip1
		{
			[DebuggerNonUserCode]
			get
			{
				return this._ToolTip1;
			}
			[DebuggerNonUserCode]
			[MethodImpl(MethodImplOptions.Synchronized)]
			set
			{
				this._ToolTip1 = value;
			}
		}

		public frm_doctor()
		{
			base.Load += new EventHandler(this.frm_doctor_Load);
			this.con = new ConnectionClass();
			this.rst = new RecordsetClass();
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frm_doctor));
			this.Label1 = new Label();
			this.Label2 = new Label();
			this.Label3 = new Label();
			this.Label4 = new Label();
			this.Label5 = new Label();
			this.Label6 = new Label();
			this.Label7 = new Label();
			this.Label8 = new Label();
			this.Label9 = new Label();
			this.email = new TextBox();
			this.phone = new TextBox();
			this.fax = new TextBox();
			this.c_name = new TextBox();
			this.c_address = new TextBox();
			this.degree = new TextBox();
			this.name1 = new TextBox();
			this.R_address = new TextBox();
			this.cell = new TextBox();
			this.Button1 = new Button();
			this.Button2 = new Button();
			this.Browse = new Button();
			this.PictureBox1 = new PictureBox();
			this.OpenFileDialog1 = new OpenFileDialog();
			this.ToolTip1 = new ToolTip(this.components);
			((ISupportInitialize)this.PictureBox1).BeginInit();
			this.SuspendLayout();
			this.Label1.AutoSize = true;
			Control arg_158_0 = this.Label1;
			Point location = new Point(20, 10);
			arg_158_0.Location = location;
			this.Label1.Name = "Label1";
			Control arg_17F_0 = this.Label1;
			Size size = new Size(38, 13);
			arg_17F_0.Size = size;
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Name:";
			this.Label2.AutoSize = true;
			Control arg_1BE_0 = this.Label2;
			location = new Point(18, 37);
			arg_1BE_0.Location = location;
			this.Label2.Name = "Label2";
			Control arg_1E5_0 = this.Label2;
			size = new Size(70, 13);
			arg_1E5_0.Size = size;
			this.Label2.TabIndex = 1;
			this.Label2.Text = "Res Address:";
			this.Label3.AutoSize = true;
			Control arg_224_0 = this.Label3;
			location = new Point(16, 64);
			arg_224_0.Location = location;
			this.Label3.Name = "Label3";
			Control arg_24B_0 = this.Label3;
			size = new Size(27, 13);
			arg_24B_0.Size = size;
			this.Label3.TabIndex = 2;
			this.Label3.Text = "Cell:";
			this.Label4.AutoSize = true;
			Control arg_28A_0 = this.Label4;
			location = new Point(16, 91);
			arg_28A_0.Location = location;
			this.Label4.Name = "Label4";
			Control arg_2B1_0 = this.Label4;
			size = new Size(66, 13);
			arg_2B1_0.Size = size;
			this.Label4.TabIndex = 3;
			this.Label4.Text = "Clinic Name:";
			this.Label5.AutoSize = true;
			Control arg_2F0_0 = this.Label5;
			location = new Point(17, 118);
			arg_2F0_0.Location = location;
			this.Label5.Name = "Label5";
			Control arg_317_0 = this.Label5;
			size = new Size(27, 13);
			arg_317_0.Size = size;
			this.Label5.TabIndex = 4;
			this.Label5.Text = "Fax:";
			this.Label6.AutoSize = true;
			Control arg_35C_0 = this.Label6;
			location = new Point(492, 140);
			arg_35C_0.Location = location;
			this.Label6.Name = "Label6";
			Control arg_383_0 = this.Label6;
			size = new Size(68, 13);
			arg_383_0.Size = size;
			this.Label6.TabIndex = 5;
			this.Label6.Text = "Qualification:";
			this.Label7.AutoSize = true;
			Control arg_3C8_0 = this.Label7;
			location = new Point(494, 174);
			arg_3C8_0.Location = location;
			this.Label7.Name = "Label7";
			Control arg_3EF_0 = this.Label7;
			size = new Size(41, 13);
			arg_3EF_0.Size = size;
			this.Label7.TabIndex = 6;
			this.Label7.Text = "Phone:";
			this.Label8.AutoSize = true;
			Control arg_431_0 = this.Label8;
			location = new Point(17, 144);
			arg_431_0.Location = location;
			this.Label8.Name = "Label8";
			Control arg_458_0 = this.Label8;
			size = new Size(35, 13);
			arg_458_0.Size = size;
			this.Label8.TabIndex = 7;
			this.Label8.Text = "Email:";
			this.Label9.AutoSize = true;
			Control arg_49A_0 = this.Label9;
			location = new Point(16, 171);
			arg_49A_0.Location = location;
			this.Label9.Name = "Label9";
			Control arg_4C1_0 = this.Label9;
			size = new Size(76, 13);
			arg_4C1_0.Size = size;
			this.Label9.TabIndex = 8;
			this.Label9.Text = "Clinic Address:";
			Control arg_4F7_0 = this.email;
			location = new Point(102, 140);
			arg_4F7_0.Location = location;
			this.email.Name = "email";
			Control arg_521_0 = this.email;
			size = new Size(359, 20);
			arg_521_0.Size = size;
			this.email.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.email, "Type here doctor Email account mainly used .");
			Control arg_560_0 = this.phone;
			location = new Point(566, 164);
			arg_560_0.Location = location;
			this.phone.Name = "phone";
			Control arg_587_0 = this.phone;
			size = new Size(112, 20);
			arg_587_0.Size = size;
			this.phone.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.phone, "Doctor Ptcl Phone ,");
			Control arg_5C0_0 = this.fax;
			location = new Point(102, 114);
			arg_5C0_0.Location = location;
			this.fax.Name = "fax";
			Control arg_5E7_0 = this.fax;
			size = new Size(100, 20);
			arg_5E7_0.Size = size;
			this.fax.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.fax, "Type here if any fax number");
			Control arg_620_0 = this.c_name;
			location = new Point(102, 87);
			arg_620_0.Location = location;
			this.c_name.Name = "c_name";
			Control arg_64A_0 = this.c_name;
			size = new Size(359, 20);
			arg_64A_0.Size = size;
			this.c_name.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.c_name, "Type here clinic name of doctor");
			Control arg_686_0 = this.c_address;
			location = new Point(102, 167);
			arg_686_0.Location = location;
			this.c_address.Name = "c_address";
			Control arg_6B0_0 = this.c_address;
			size = new Size(359, 20);
			arg_6B0_0.Size = size;
			this.c_address.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.c_address, "Type here Clinic Address of homeopathic doctor");
			Control arg_6EF_0 = this.degree;
			location = new Point(564, 137);
			arg_6EF_0.Location = location;
			this.degree.Name = "degree";
			Control arg_716_0 = this.degree;
			size = new Size(114, 20);
			arg_716_0.Size = size;
			this.degree.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.degree, "Type here doctor qulification like DHMS,etc");
			Control arg_74E_0 = this.name1;
			location = new Point(102, 6);
			arg_74E_0.Location = location;
			this.name1.Name = "name1";
			Control arg_778_0 = this.name1;
			size = new Size(359, 20);
			arg_778_0.Size = size;
			this.name1.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.name1, "Type here homeopathic doctor name");
			Control arg_7B1_0 = this.R_address;
			location = new Point(102, 33);
			arg_7B1_0.Location = location;
			this.R_address.Name = "R_address";
			Control arg_7DB_0 = this.R_address;
			size = new Size(359, 20);
			arg_7DB_0.Size = size;
			this.R_address.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.R_address, "Type here homeopathic doctor residence address");
			Control arg_814_0 = this.cell;
			location = new Point(102, 60);
			arg_814_0.Location = location;
			this.cell.Name = "cell";
			Control arg_83B_0 = this.cell;
			size = new Size(100, 20);
			arg_83B_0.Size = size;
			this.cell.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.cell, "Type here Mobile number of homeopathic doctor");
			this.Button1.BackColor = Color.Transparent;
			this.Button1.BackgroundImage = (Image)componentResourceManager.GetObject("Button1.BackgroundImage");
			this.Button1.Image = (Image)componentResourceManager.GetObject("Button1.Image");
			Control arg_8BD_0 = this.Button1;
			location = new Point(100, 199);
			arg_8BD_0.Location = location;
			this.Button1.Name = "Button1";
			Control arg_8E4_0 = this.Button1;
			size = new Size(75, 60);
			arg_8E4_0.Size = size;
			this.Button1.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.Button1, "Click this button to verify detail");
			this.Button1.UseVisualStyleBackColor = false;
			this.Button2.BackColor = Color.Transparent;
			this.Button2.BackgroundImage = (Image)componentResourceManager.GetObject("Button2.BackgroundImage");
			this.Button2.Image = (Image)componentResourceManager.GetObject("Button2.Image");
			Control arg_976_0 = this.Button2;
			location = new Point(175, 199);
			arg_976_0.Location = location;
			this.Button2.Name = "Button2";
			Control arg_99D_0 = this.Button2;
			size = new Size(75, 60);
			arg_99D_0.Size = size;
			this.Button2.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.Button2, "Click on this button to cancel ");
			this.Button2.UseVisualStyleBackColor = false;
			this.Browse.BackgroundImage = (Image)componentResourceManager.GetObject("Browse.BackgroundImage");
			this.Browse.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Browse.ForeColor = Color.FromArgb(255, 128, 0);
			this.Browse.Image = (Image)componentResourceManager.GetObject("Browse.Image");
			Control arg_A57_0 = this.Browse;
			location = new Point(582, 190);
			arg_A57_0.Location = location;
			this.Browse.Name = "Browse";
			Control arg_A7E_0 = this.Browse;
			size = new Size(75, 60);
			arg_A7E_0.Size = size;
			this.Browse.TabIndex = 9;
			this.Browse.Text = "Attach Photo";
			this.ToolTip1.SetToolTip(this.Browse, "Browse to attach your photo from your computer select photo size within 100X100 pixels only height and width.");
			this.Browse.UseVisualStyleBackColor = true;
			this.PictureBox1.Image = (Image)componentResourceManager.GetObject("PictureBox1.Image");
			this.PictureBox1.InitialImage = null;
			Control arg_AFD_0 = this.PictureBox1;
			location = new Point(564, 6);
			arg_AFD_0.Location = location;
			this.PictureBox1.Name = "PictureBox1";
			Control arg_B24_0 = this.PictureBox1;
			size = new Size(114, 125);
			arg_B24_0.Size = size;
			this.PictureBox1.TabIndex = 21;
			this.PictureBox1.TabStop = false;
			this.ToolTip1.SetToolTip(this.PictureBox1, "Doctor,s Photo attached ");
			this.OpenFileDialog1.FileName = "OpenFileDialog1";
			this.ToolTip1.AutoPopDelay = 10000;
			this.ToolTip1.BackColor = Color.FromArgb(255, 192, 255);
			this.ToolTip1.ForeColor = Color.Purple;
			this.ToolTip1.InitialDelay = 500;
			this.ToolTip1.IsBalloon = true;
			this.ToolTip1.ReshowDelay = 0;
			this.ToolTip1.ToolTipTitle = "How to add doctor details Help?";
			SizeF autoScaleDimensions = new SizeF(6f, 13f);
			this.AutoScaleDimensions = autoScaleDimensions;
			this.AutoScaleMode = AutoScaleMode.Font;
			size = new Size(708, 275);
			this.ClientSize = size;
			this.Controls.Add(this.PictureBox1);
			this.Controls.Add(this.Browse);
			this.Controls.Add(this.Button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.cell);
			this.Controls.Add(this.R_address);
			this.Controls.Add(this.name1);
			this.Controls.Add(this.degree);
			this.Controls.Add(this.c_address);
			this.Controls.Add(this.c_name);
			this.Controls.Add(this.fax);
			this.Controls.Add(this.phone);
			this.Controls.Add(this.email);
			this.Controls.Add(this.Label9);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label5);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			this.MinimizeBox = false;
			this.Name = "frm_doctor";
			this.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "Doctor Detail System :::  P H R  :::  Pak Homeopathic Repertory 2008 Ver 1.0";
			((ISupportInitialize)this.PictureBox1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			if (Operators.CompareString(this.name1.Text, "", false) != 0 & Operators.CompareString(this.R_address.Text, "", false) != 0 & Operators.CompareString(this.cell.Text, "", false) != 0 & Operators.CompareString(this.email.Text, "", false) != 0 & Operators.CompareString(this.fax.Text, "", false) != 0 & Operators.CompareString(this.c_name.Text, "", false) != 0 & Operators.CompareString(this.c_address.Text, "", false) != 0 & Operators.CompareString(this.degree.Text, "", false) != 0)
			{
				_Connection arg_1F9_0 = this.con;
				string arg_1F9_1 = string.Concat(new string[]
				{
					"update doctor set  [d_Name]='",
					this.name1.Text,
					"'  ,[r_address]='",
					this.R_address.Text,
					"' ,[phone]='",
					this.phone.Text,
					"' ,[email]='",
					this.email.Text,
					"' ,[c_name]='",
					this.c_name.Text,
					"' ,[cell]='",
					this.cell.Text,
					"' ,[fax]='",
					this.fax.Text,
					"' ,[c_address]='",
					this.c_address.Text,
					"' ,[degree]='",
					this.degree.Text,
					"',pic= '",
					this.PictureBox1.ImageLocation,
					"' where id =1"
				});
				object value = Missing.Value;
				arg_1F9_0.Execute(arg_1F9_1, out value, -1);
				this.name1.Text = "";
				this.R_address.Text = "";
				this.phone.Text = "";
				this.cell.Text = "";
				this.email.Text = "";
				this.fax.Text = "";
				this.c_name.Text = "";
				this.c_address.Text = "";
				this.degree.Text = "";
				this.Close();
			}
			else
			{
				Interaction.MsgBox("Plz fill required fields", MsgBoxStyle.Information, null);
			}
		}

		public void Connect_Database()
		{
            string str = "provider=microsoft.jet.oledb.4.0;data source=" + AppDomain.CurrentDomain.BaseDirectory + "\\db\\phrb.rep;Jet OLEDB:Database Password = پاکستان2008عزیز";
			this.con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";";
			this.con.Open("", "", "", -1);
		}

		private void frm_doctor_Load(object sender, EventArgs e)
		{
			this.Connect_Database();
			_Connection arg_1B_0 = this.con;
			string arg_1B_1 = "select * from doctor where id=1";
			object value = Missing.Value;
			this.rst = arg_1B_0.Execute(arg_1B_1, out value, -1);
			this.PictureBox1.ImageLocation = Conversions.ToString(this.rst.Fields["pic"].Value);
			this.name1.Text = Conversions.ToString(this.rst.Fields["d_name"].Value);
			this.R_address.Text = Conversions.ToString(this.rst.Fields["r_address"].Value);
			this.cell.Text = Conversions.ToString(this.rst.Fields["cell"].Value);
			this.c_name.Text = Conversions.ToString(this.rst.Fields["c_name"].Value);
			this.fax.Text = Conversions.ToString(this.rst.Fields["fax"].Value);
			this.email.Text = Conversions.ToString(this.rst.Fields["email"].Value);
			this.c_address.Text = Conversions.ToString(this.rst.Fields["c_address"].Value);
			this.degree.Text = Conversions.ToString(this.rst.Fields["degree"].Value);
			this.phone.Text = Conversions.ToString(this.rst.Fields["phone"].Value);
		}

		private void Browse_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = this.OpenFileDialog1;
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				this.PictureBox1.ImageLocation = openFileDialog.FileName;
				this.PictureBox1.Visible = true;
				_Connection arg_51_0 = this.con;
				string arg_51_1 = "update doctor set  pic= '" + openFileDialog.FileName + "' where id =1";
				object value = Missing.Value;
				arg_51_0.Execute(arg_51_1, out value, -1);
			}
		}
	}
}
