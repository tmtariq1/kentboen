using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Runtime.InteropServices;

namespace HomeoBoen2008
{
	[StandardModule]
	internal sealed class HDiskInfo
	{
		public struct DiskInfo
		{
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
			public string SerialNumber;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
			public string ModelNumber;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
			public string RevisionNo;

			public int BufferSize;

			public int Cylinders;

			public int Heads;

			public int Sectors;
		}

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern int GetIdeDiskInfo(short DriveNo, ref HDiskInfo.DiskInfo DiskInfo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern string GetSerialNumber(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern string GetModelNumber(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern string GetRevisionNo(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern short GetBufferSize(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern short GetCylinders(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern short GetHeads(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);

		[DllImport("GetDiskSerial.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
		public static extern short GetSectors(short DriveNo, [MarshalAs(UnmanagedType.VBByRefStr)] ref string sRegNumber);
	}
}
