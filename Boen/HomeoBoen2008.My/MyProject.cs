using Microsoft.VisualBasic;
using Microsoft.VisualBasic.ApplicationServices;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace HomeoBoen2008.My
{
	[StandardModule, HideModuleName, GeneratedCode("MyTemplate", "8.0.0.0")]
	internal sealed class MyProject
	{
		[MyGroupCollection("System.Windows.Forms.Form", "Create__Instance__", "Dispose__Instance__", "My.MyProject.Forms"), EditorBrowsable(EditorBrowsableState.Never)]
		internal sealed class MyForms
		{
			public Form1 m_Form1;

			public frm_adduser m_frm_adduser;

			public frm_doctor m_frm_doctor;

			public frm_Family m_frm_Family;

			public frm_Login m_frm_Login;

			public frm_Main m_frm_Main;

			public frm_Materia m_frm_Materia;

			public frm_Patient m_frm_Patient;

			public frm_patientopen m_frm_patientopen;

			public frm_patientreport m_frm_patientreport;

			public frm_registation m_frm_registation;

			public frm_report1 m_frm_report1;

			[ThreadStatic]
			private static Hashtable m_FormBeingCreated;

			public Form1 Form1
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_Form1 = MyProject.MyForms.Create__Instance__<Form1>(this.m_Form1);
					return this.m_Form1;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_Form1)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<Form1>(ref this.m_Form1);
				}
			}

			public frm_adduser frm_adduser
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_adduser = MyProject.MyForms.Create__Instance__<frm_adduser>(this.m_frm_adduser);
					return this.m_frm_adduser;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_adduser)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_adduser>(ref this.m_frm_adduser);
				}
			}

			public frm_doctor frm_doctor
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_doctor = MyProject.MyForms.Create__Instance__<frm_doctor>(this.m_frm_doctor);
					return this.m_frm_doctor;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_doctor)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_doctor>(ref this.m_frm_doctor);
				}
			}

			public frm_Family frm_Family
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_Family = MyProject.MyForms.Create__Instance__<frm_Family>(this.m_frm_Family);
					return this.m_frm_Family;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_Family)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_Family>(ref this.m_frm_Family);
				}
			}

			public frm_Login frm_Login
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_Login = MyProject.MyForms.Create__Instance__<frm_Login>(this.m_frm_Login);
					return this.m_frm_Login;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_Login)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_Login>(ref this.m_frm_Login);
				}
			}

			public frm_Main frm_Main
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_Main = MyProject.MyForms.Create__Instance__<frm_Main>(this.m_frm_Main);
					return this.m_frm_Main;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_Main)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_Main>(ref this.m_frm_Main);
				}
			}

			public frm_Materia frm_Materia
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_Materia = MyProject.MyForms.Create__Instance__<frm_Materia>(this.m_frm_Materia);
					return this.m_frm_Materia;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_Materia)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_Materia>(ref this.m_frm_Materia);
				}
			}

			public frm_Patient frm_Patient
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_Patient = MyProject.MyForms.Create__Instance__<frm_Patient>(this.m_frm_Patient);
					return this.m_frm_Patient;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_Patient)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_Patient>(ref this.m_frm_Patient);
				}
			}

			public frm_patientopen frm_patientopen
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_patientopen = MyProject.MyForms.Create__Instance__<frm_patientopen>(this.m_frm_patientopen);
					return this.m_frm_patientopen;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_patientopen)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_patientopen>(ref this.m_frm_patientopen);
				}
			}

			public frm_patientreport frm_patientreport
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_patientreport = MyProject.MyForms.Create__Instance__<frm_patientreport>(this.m_frm_patientreport);
					return this.m_frm_patientreport;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_patientreport)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_patientreport>(ref this.m_frm_patientreport);
				}
			}

			public frm_registation frm_registation
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_registation = MyProject.MyForms.Create__Instance__<frm_registation>(this.m_frm_registation);
					return this.m_frm_registation;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_registation)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_registation>(ref this.m_frm_registation);
				}
			}

			public frm_report1 frm_report1
			{
				[DebuggerNonUserCode]
				get
				{
					this.m_frm_report1 = MyProject.MyForms.Create__Instance__<frm_report1>(this.m_frm_report1);
					return this.m_frm_report1;
				}
				[DebuggerNonUserCode]
				set
				{
					if (value == this.m_frm_report1)
					{
						return;
					}
					if (value != null)
					{
						throw new ArgumentException("Property can only be set to Nothing");
					}
					this.Dispose__Instance__<frm_report1>(ref this.m_frm_report1);
				}
			}

			[DebuggerHidden]
			private static T Create__Instance__<T>(T Instance) where T : Form, new()
			{
				if (Instance == null || Instance.IsDisposed)
				{
					if (MyProject.MyForms.m_FormBeingCreated != null)
					{
						if (MyProject.MyForms.m_FormBeingCreated.ContainsKey(typeof(T)))
						{
							throw new InvalidOperationException(Utils.GetResourceString("WinForms_RecursiveFormCreate", new string[0]));
						}
					}
					else
					{
						MyProject.MyForms.m_FormBeingCreated = new Hashtable();
					}
					MyProject.MyForms.m_FormBeingCreated.Add(typeof(T), null);
					try
					{
						try
						{
							return Activator.CreateInstance<T>();
						} catch (Exception ex){}
						/*object arg_74_0;
						TargetInvocationException expr_79 = arg_74_0 as TargetInvocationException;
						int arg_96_0;
						if (expr_79 == null)
						{
							arg_96_0 = 0;
						}
						else
						{
							//TargetInvocationException ex = expr_79;
							//ProjectData.SetProjectError(expr_79);
							//arg_96_0 = (((ex.InnerException != null) > false) ? 1 : 0);
						}*/
						//endfilter(arg_96_0);
                    }
                    catch (Exception ex) { }
					finally
					{
						MyProject.MyForms.m_FormBeingCreated.Remove(typeof(T));
					}
					return Instance;
				}
				return Instance;
			}

			[DebuggerHidden]
			private void Dispose__Instance__<T>(ref T instance) where T : Form
			{
				instance.Dispose();
				instance = default(T);
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public MyForms()
			{
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override bool Equals(object o)
			{
				return base.Equals(RuntimeHelpers.GetObjectValue(o));
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			internal new Type GetType()
			{
				return typeof(MyProject.MyForms);
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override string ToString()
			{
				return base.ToString();
			}
		}

		[MyGroupCollection("System.Web.Services.Protocols.SoapHttpClientProtocol", "Create__Instance__", "Dispose__Instance__", ""), EditorBrowsable(EditorBrowsableState.Never)]
		internal sealed class MyWebServices
		{
			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public override bool Equals(object o)
			{
				return base.Equals(RuntimeHelpers.GetObjectValue(o));
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			internal new Type GetType()
			{
				return typeof(MyProject.MyWebServices);
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public override string ToString()
			{
				return base.ToString();
			}

			[DebuggerHidden]
			private static T Create__Instance__<T>(T instance) where T : new()
			{
				if (instance == null)
				{
					return Activator.CreateInstance<T>();
				}
				return instance;
			}

			[DebuggerHidden]
			private void Dispose__Instance__<T>(ref T instance)
			{
				instance = default(T);
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public MyWebServices()
			{
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), ComVisible(false)]
		internal sealed class ThreadSafeObjectProvider<T> where T : new()
		{
			[CompilerGenerated, ThreadStatic]
			private static T m_ThreadStaticValue;

			internal T GetInstance
			{
				[DebuggerHidden]
				get
				{
					if (MyProject.ThreadSafeObjectProvider<T>.m_ThreadStaticValue == null)
					{
						MyProject.ThreadSafeObjectProvider<T>.m_ThreadStaticValue = Activator.CreateInstance<T>();
					}
					return MyProject.ThreadSafeObjectProvider<T>.m_ThreadStaticValue;
				}
			}

			[EditorBrowsable(EditorBrowsableState.Never), DebuggerHidden]
			public ThreadSafeObjectProvider()
			{
			}
		}

		private static readonly MyProject.ThreadSafeObjectProvider<MyComputer> m_ComputerObjectProvider = new MyProject.ThreadSafeObjectProvider<MyComputer>();

		private static readonly MyProject.ThreadSafeObjectProvider<MyApplication> m_AppObjectProvider = new MyProject.ThreadSafeObjectProvider<MyApplication>();

		private static readonly MyProject.ThreadSafeObjectProvider<User> m_UserObjectProvider = new MyProject.ThreadSafeObjectProvider<User>();

		private static MyProject.ThreadSafeObjectProvider<MyProject.MyForms> m_MyFormsObjectProvider = new MyProject.ThreadSafeObjectProvider<MyProject.MyForms>();

		private static readonly MyProject.ThreadSafeObjectProvider<MyProject.MyWebServices> m_MyWebServicesObjectProvider = new MyProject.ThreadSafeObjectProvider<MyProject.MyWebServices>();

		[HelpKeyword("My.Computer")]
		internal static MyComputer Computer
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_ComputerObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.Application")]
		internal static MyApplication Application
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_AppObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.User")]
		internal static User User
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_UserObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.Forms")]
		internal static MyProject.MyForms Forms
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_MyFormsObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.WebServices")]
		internal static MyProject.MyWebServices WebServices
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_MyWebServicesObjectProvider.GetInstance;
			}
		}
	}
}
